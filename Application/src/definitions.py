# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 15:45:07 2020

@author: Magne Lauritzen

Various definitions, constants, custom exceptions, etc, for the PCTM application. 
"""
from .enums import SampleSource, InterlockSettings
import pint  # type: ignore

ureg = pint.UnitRegistry()


class PCTMException(Exception):
    """Base PCTM exception."""
    pass


class NotAPCTM(PCTMException):
    """Raised when one attempts to connect to a device that is not a PCTM"""
    pass


class PCTMLogException(PCTMException):
    pass

class PCTMInfluxException(PCTMException):
    """Raised when one fails to connect to an influx db"""
    pass

class LVPSUException(PCTMException):
    """Raised when failed to connect to lv psu"""
    pass


"""The dataset_definitions variable defines how datasets are saved in the HDF5
file which data is logged to. For each sampling source, a dataset is created.
The name of the dataset, its description, and its unit, are defined here."""
dataset_definitions = {
    SampleSource.NTC_1: {'name': "NTC 1",
                         'description': "Module temperature measured on NTC 1. This is "
                                        "the only NTC on Quad modules.",
                         'unit': ureg.degC},
    SampleSource.NTC_2: {'name': "NTC 2",
                         'description': "Module temperature measured on NTC 2. This is "
                                        "only valid on Triplet modules.",
                         'unit': ureg.degC},
    SampleSource.NTC_3: {'name': "NTC 3",
                         'description': "Module temperature measured on NTC 1.  This is "
                                        "only valid on Triplet modules.",
                         'unit': ureg.degC},
    SampleSource.GAS_FLOW: {'name': "GAS FLOW",
                            'description': "Purge gas flow entering the test enclosure.",
                            'unit': ureg.liter / ureg.minute},
    SampleSource.GAS_T_1: {'name': "ENV TEMP 1",
                           'description': "Temperature of the test enclosure atmosphere "
                                          "as measured by the 2nd humidity sensor.",
                           'unit': ureg.degC},
    SampleSource.GAS_T_2: {'name': "ENV TEMP 2",
                           'description': "Temperature of the test enclosure atmosphere "
                                          "as measured by the 2nd humidity sensor.",
                           'unit': ureg.degC},
    SampleSource.RH_1: {'name': "REL HUM 1",
                        'description': "Relative humidity of the test enclosure "
                                       "atmosphere as measured by the 1st humidity sensor.",
                        'unit': None},
    SampleSource.RH_2: {'name': "REL HUM 2",
                        'description': "Relative humidity of the test enclosure "
                                       "atmosphere as measured by the 2nd humidity sensor.",
                        'unit': None},
    SampleSource.DP_1: {'name': "DEW POINT 1",
                        'description': "Dew point of the test enclosure "
                                       "atmosphere as measured by the 1st humidity sensor.",
                        'unit': ureg.degC},
    SampleSource.DP_2: {'name': "DEW POINT 2",
                        'description': "Dew point of the test enclosure "
                                       "atmosphere as measured by the 2nd humidity sensor.",
                        'unit': ureg.degC},
    SampleSource.CP_T: {'name': "HEATSINK",
                        'description': "Temperature of the heatsink in the cooling "
                                       "system. When using the common Cooling Unit, this is the "
                                       "\"coldplate\".",
                        'unit': ureg.degC},
    SampleSource.VC_T: {'name': "VACUUMCHUCK",
                        'description': "Temperature of the vacuumchuck in the cooling "
                                       "system.",
                        'unit': ureg.degC},
    SampleSource.TEC_I: {'name': "PELTIER I",
                         'description': "Current flowing through the cooling system "
                                        "peltier elements.",
                         'unit': ureg.ampere},
    SampleSource.TEC_V: {'name': "PELTIER V",
                         'description': "Voltage across the cooling system "
                                        "peltier elements.",
                         'unit': ureg.volt},
    SampleSource.TEC_PID_SETPOINT: {'name': "TEC_SETPOINT",
                         'description': "Target temperature",
                         'unit': ureg.degC},
    SampleSource.PSU_PID_SETPOINT: {'name': "PSU_SETPOINT",
                         'description': "Target peltier current",
                         'unit': ureg.ampere},
    SampleSource.PSU_CONTROL: {'name': "PSU_CONTROL_VALUE",
                         'description': "The control value sent to the TEC PSU",
                         'unit': None},
    SampleSource.VACUUM_PRESSURE: {'name': "VACUUM_PRESSURE",
                                   'description': "The pressure differential between atmospheric pressure and the vacuum chuck pressure",
                                   'unit': ureg.kPa},
}

influxdb_key_name_definitions = {
    SampleSource.NTC_1: 'Module_temperature_1',
    SampleSource.NTC_2: 'Module_temperature_2',
    SampleSource.NTC_3: 'Module_temperature_3',
    SampleSource.GAS_FLOW: 'Gas_flow',
    SampleSource.GAS_T_1: 'Gas_temperature_1',
    SampleSource.GAS_T_2: 'Gas_temperature_2',
    SampleSource.RH_1: 'Relative_humidity_1',
    SampleSource.RH_2: 'Relative_humidity_2',
    SampleSource.DP_1: 'DewPoint_1',
    SampleSource.DP_2: 'DewPoint_2',
    SampleSource.CP_T: 'ColdPlate_temperature',
    SampleSource.VC_T: 'VacuumChuck_temperature',
    SampleSource.TEC_I: 'Peltier_current',
    SampleSource.TEC_V: 'Peltier_voltage',
    SampleSource.TEC_PID_SETPOINT: 'Temperature_setpoint',
    SampleSource.PSU_PID_SETPOINT: 'Peltier_setpoint',
    SampleSource.PSU_CONTROL: 'PSU_control_value',
    SampleSource.VACUUM_PRESSURE: 'Cold chuck vacuum pressure',
    InterlockSettings.ACTIVATED_HIGH: 'Interlock_high_limit',
    InterlockSettings.ACTIVATED_LOW: 'Interlock_low_limit',
    InterlockSettings.ACTIVATED_DEW: 'Interlock_dewpoint_limit',
    InterlockSettings.OVERHEAT: 'Interlock_overheat',
    InterlockSettings.ACTIVATED_ABSENT: 'Interlock_absent',
    InterlockSettings.ACTIVATED_COLDPLATE: 'Interlock_coldplate'}
