# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 19:50:17 2020

@author: magne.lauritzen

This file defines the class PCTMHandler. This class is the handling point for all
interaction with the PCTM.

Two classes, DefaultSample and RxDataHandler, are also defined here. These
classes are used to unpack and interpret the data received from the PCTM.
"""
import threading, time
from autologging import traced  # type: ignore
from queue import Queue, Full
import struct
from PyQt5 import QtCore  # type: ignore
from uuid import uuid4
from typing import Dict, Optional, Union, Tuple, Any, no_type_check, TYPE_CHECKING
import numpy as np
from numpy_ringbuffer import RingBuffer  # type: ignore
from pySerialTransfer import pySerialTransfer as txfer  # type: ignore
from .enums import SampleSource, PeltierSettings, MonitorFSettings, SystemSettings, Requests, RxPacketID, SystemInfo, InterlockSettings
from .definitions import NotAPCTM
from . import loggers

logger = loggers.get_logger(__name__)

if TYPE_CHECKING:
    spenum_t = Union[RxPacketID, SystemInfo, MonitorFSettings, PeltierSettings, InterlockSettings]


class HandlerSignals(QtCore.QObject):
    """
    This class defines the signals which the PCTMHandler can emit.

    newData     : Emitted when new data has been received from the PCTM.
    outDataFull : Emitted when the output data queue "Full" state changes. Emits
        either True (when the queue becomes Full) or False (when the queue becomes
        not Full)
    """
    newData = QtCore.pyqtSignal(SampleSource)
    outDataFull = QtCore.pyqtSignal(bool)


@traced(logger, "putSample", "__populateDataStruct", exclude=True)
class PCTMHandler():
    """
    The PCTMHandler class handles all interactions with the PCTM. It connects
    to one PCTM and asyncronously logs all the samples in rolling numpy buffers.

    The PCTMHandler emits a newData signal when a new sample is received from the
    PCTM.

    The PCTM spins up two new threads, one for reading and one for timing sync
    events. These threads must always be properly closed with join(). The
    PCTMHandler wraps these threads join() functions, so that you can just call
    PCTMHandler.join().

    Example:
        try:
            h = PCTMHandler()
            h.connect('COM5')
            time.sleep(1)
        finally:
            h.join() #The PCTMHandler **must** be joined.
            print(h.data)
    """

    def __init__(self) -> None:
        super().__init__()
        self.signals = HandlerSignals()
        # Set up the sample data circular buffers and output queues
        self.data: Dict[SampleSource, RingBuffer] = {}
        self.dataqueue = Queue(100)  # type: ignore
        self.dataqueue_influx = Queue(100)  # type: ignore
        self.dataqueue_thermal = Queue(100)  # type: ignore
        self.dataqueue_interlock = Queue(100)  # type: ignore
        self.__populateDataStruct()
        # Set up the communication stuff
        self.comThread = threading.Thread(target=self._comReader)
        self.txqueue = Queue(1000)  # type: ignore
        self.link: Optional[txfer.SerialTransfer] = None
        self._running = True
        self.rxdatahandler = RxDataHandler(self)
        self.last_tx_ack = True
        self.last_tx_time = time.time()
        # Other variables
        self.uptime = 0.0
        self._sync_time = 0.0
        self._outqueue_full = False
        # Begin the sync routine
        self.syncTimer = timedRoutine(60, lambda: self.synchronize())
        self.syncTimer.start()

    def reconnect(self) -> None:
        """Attempts to reconnect to the port. Does nothing if the port is already
        open, or if this PCTMHandler has not connected to a port previously."""
        if self.link is not None:
            self.connect(self.link.connection.port)

    def connect(self, port: str) -> None:
        """Connects to a device on the port given by argument 'port'.
        If the device connected to does not respond with the expected
        acknowledgement that a PCTM would return, the connection fails with
        NotAPCTM exception."""
        if self.link is not None and self.link.connection.isOpen():
            return  # Don't try to connect to a port if one is already open
        if self.comThread.is_alive():
            self.comThread.join()  # Disconnect and join the running async txrx thread if it is alive
        try:
            self.link = txfer.SerialTransfer(port, baud=115200)
        except:
            logger.exception("Could not open link.")
            self.link = None
            return
        self.comThread.start()
        time.sleep(0.2)
        self._request_uptime()
        time.sleep(0.5)
        # If we haven't received an uptime response from the device we just connected
        # to, it indicates that it did not understand the command and therefore that
        # it is not a PCTM.
        if self.uptime == 0:
            self.disconnect()
            raise NotAPCTM(f"The device on port {port} does not appear to be a "
                           "PCTM.")
        self.synchronize()

    def disconnect(self) -> None:
        """Disconnects this handler from the PCTM."""
        self.stop()
        self.uptime = 0
        self._sync_time = 0

    def putSample(self, source: SampleSource, sample: Tuple[np.float32, np.uint16, np.float64]) -> None:
        """Adds the sample 'sample' to the rolling buffers that holds samples
        from the PCTM. Then transmits the sample on the dataqueue to be picked
        up elsewhere."""
        if self._sync_time != 0:
            if source not in self.data.keys():
                raise ValueError(f"The data source {source} is not valid.")
            try:
                # Log data in rolling buffers
                self.data[source].append(sample)
                # Put data on output queue. Catch Full exceptions.
                try:
                    self.dataqueue.put_nowait((source, sample))
                    self.dataqueue_influx.put_nowait((source, sample))
                    self.dataqueue_thermal.put_nowait((source, sample))
                    self.dataqueue_interlock.put_nowait((source, sample))
                    self.signals.newData.emit(source)
                    if self._outqueue_full:
                        self._outqueue_full = False
                        self.signals.outDataFull.emit(False)
                except Full:
                    if not self._outqueue_full:
                        self._outqueue_full = True
                        self.signals.outDataFull.emit(True)
            except:
                logger.exception("An exception occurred when attempting to append "
                                 f"the sample {sample} to the ring buffer {source}.")

    def getSampleFrequencies(self) -> None:
        """Requests that the PCTM responds with all the sample frequencies it is
        using for the various sampling routines."""
        self._sendCommand(Requests.REQ_SAMPLEFREQS)

    def setSampleFrequency(self, fsets: Dict[MonitorFSettings, float]) -> None:
        """Sets the sample frequency of each monitored parameter on the PCTM.
        Frequency limit is 20Hz. You must give the new frequencies as a dict
        of {MonitorFSettings:frequency} items.

        Example:
            setSampleFrequency({MonitorFSettings.NTC_2_F:50, MonitorFSettings.GAS_FLOW_F:2})
        """
        for key, val in fsets.items():
            if key not in MonitorFSettings:
                raise ValueError("The sample setting \"{key}\" is not valid.")
            val = float(val)
            F = min(val, 20)  # Cap to 20Hz
            if F <= 10 ^ -7 or F == 0.0:
                delay = 0.0  # Turns off the sampler
            else:
                delay = 1 / F
            self._sendCommand(key, delay * 1000)

    def getPidParameters(self) -> None:
        """Requests that the PCTM responds with all the PID settings it is
        currently using."""
        self._sendCommand(Requests.REQ_PIDS)
        pass

    def setTecPidParameters(self, parameters: Dict[str, float]) -> None:
        """Uploads new Peltier PID loop settings to the PCTM. 'Parameters' is
        a dict: {'kp':val, 'ki':val, 'kp':val}."""
        kp = parameters['kp']
        ki = parameters['ki']
        kd = parameters['kd']
        pon = parameters['pon']
        for key, val in zip([PeltierSettings.KP_TEC,
                             PeltierSettings.KI_TEC,
                             PeltierSettings.KD_TEC,
                             PeltierSettings.PON_TEC], [kp, ki, kd, pon]):
            self._sendCommand(key, val)

    def setPsuPidParameters(self, parameters: Dict[str, float]) -> None:
        """Uploads new PSU PID loop settings to the PCTM. 'Parameters' is
        a dict: {'kp':val, 'ki':val, 'kp':val}."""
        kp = parameters['kp']
        ki = parameters['ki']
        kd = parameters['kd']
        for key, val in zip([PeltierSettings.KP_PSU,
                             PeltierSettings.KI_PSU,
                             PeltierSettings.KD_PSU], [kp, ki, kd]):
            self._sendCommand(key, val)

    def setPSUPidFrequency(self, psu_freq: float) -> None:
        """Uploads new PSU PID sample frequency to the PCTM. """
        if psu_freq < 1 or psu_freq == 0.0:
            psu_freq = 1
        self._sendCommand(PeltierSettings.PSU_FREQ, psu_freq)

    def setTECPidFrequency(self, tec_freq: float) -> None:
        """Uploads new TEC PID sample frequency to the PCTM. """
        if tec_freq < 1 or tec_freq == 0.0:
            tec_freq = 1
        self._sendCommand(PeltierSettings.TEC_FREQ, tec_freq)

    def setPSUImax(self, i_max: float) -> None:
        self._sendCommand(PeltierSettings.PSU_MAX, i_max)

    def controlTempSetpoint(self, setpoint: float, rampvalue: float) -> None:
        """Uploads a new module temperature setpoint for the Peltier PID loop and/or sets ramp rate"""
        if rampvalue == 0.0:
            self._sendCommand(PeltierSettings.SETPOINT, setpoint)
        else:
            self._sendCommand(PeltierSettings.SETRAMP, rampvalue)
            self._sendCommand(PeltierSettings.RAMP, setpoint)

    def setCILimits(self, limits: Dict[str, float]) -> None:
        vlimit = limits['vlimit']
        ilimit = limits['ilimit']
        for key, val in zip([PeltierSettings.VLIMIT,
                             PeltierSettings.ILIMIT], [vlimit, ilimit]):
            self._sendCommand(key, val)

    def setTecControl(self, control: Dict[str, Any]) -> None:
        """ Sets the TEC PID loop control mode."""
        mode = PeltierSettings(control['mode'])
        if mode == PeltierSettings.PID_TEC:
            val = 0
        elif mode == PeltierSettings.CC:
            val = control['cc']
        else:
            val = control['cv']
        self._sendCommand(mode, val)

    def setPsuControl(self, control: Dict[str, Any]) -> None:
        """ Sets the PSU PID loop control mode."""
        mode =  PeltierSettings(control['mode'])
        if mode == PeltierSettings.PID_PSU:
            val = 0
        else:
            val = control['cctrl']
        self._sendCommand(mode, val)

    def setPsuCommMode(self, val: int) -> None:
        """ Sets the PSU PID loop control mode."""
        self._sendCommand(PeltierSettings.COMM_MODE, float(val))

    def disablePID(self) -> None:
        self._sendCommand(Requests.REQ_PIDDISABLE)

    def enablePID(self) -> None:
        self._sendCommand(Requests.REQ_PIDENABLE)

    def setConstantMode(self, state: bool) -> None:
        if state:
            self._sendCommand(Requests.REQ_CONSTANTENABLE)
        else:
            self._sendCommand(Requests.REQ_CONSTANTDISABLE)

    def setInterlockSettings(self, parameters: Dict[Any, Any]) -> None:
        """Uploads new interlock settings to the PCTM"""
        upper_limit = parameters[InterlockSettings.UPPER.name]
        lower_limit = parameters[InterlockSettings.LOWER.name]
        dewpoint_limit = parameters[InterlockSettings.DIFF_DP_NTC.name]
        coldplate_limit = parameters[InterlockSettings.COLDPLATE.name]

        for key, val in zip([InterlockSettings.UPPER,
                             InterlockSettings.LOWER,
                             InterlockSettings.DIFF_DP_NTC,
                             InterlockSettings.COLDPLATE],
                            [upper_limit,
                             lower_limit,
                             dewpoint_limit,
                             coldplate_limit]):
            self._sendCommand(key, val)

    def getInterlockSettings(self) -> None:
        self._sendCommand(Requests.REQ_INTERLOCK)

    def enablePCTMInterlock(self) -> None:
        self._sendCommand(Requests.REQ_ACTIVATEINTERLOCK)

    def disablePCTMInterlock(self) -> None:
        self._sendCommand(Requests.REQ_DEACTIVATEINTERLOCK)

    def synchronize(self) -> None:
        """Synchronizes the PCTM with this class to maintain correct sampling time."""
        self._sendCommand(Requests.REQ_SYNC)

    def calibrateISensor(self) -> None:
        """ Samples the Peltier current sensor for one second and sets the zero-point the the average value. """
        self._sendCommand(Requests.REQ_ICALIBRATE)

    def clearNonvolatileSettings(self) -> None:
        """ Clears all nonvolatile settings stored in the PCTM EEPROM. """
        self._sendCommand(Requests.REQ_RESET)

    def setId(self, id: int) -> None:
        """Uploads a new nonvolatile identity to the PCTM. Parameter 'id' must
        lie between 0 and F."""
        if id < 0 or id > 15:
            raise ValueError(f"PCTM ID must lie between 0 and F. {id} is not valid.")
        self._sendCommand(SystemSettings.ID, float(id))
        self.getId()

    def getId(self) -> None:
        self._sendCommand(Requests.REQ_ID)

    def _sendCommand(self, variable: Union[PeltierSettings,
                                           MonitorFSettings,
                                           Requests,
                                           SystemSettings,
                                           InterlockSettings],
                     value: Optional[float] = None) -> None:
        """
        Sends a command to the PCTM.
        The command is put on the command queue and handled by self._comReader.
        Command reception is verified with an acknowledge-response from the PCTM.
        If no acknowledgement of the command is returned, this either indicates
        that the PCTM did not receive the command, or it did not know how to
        handle it. All commands are resent up to 5 times if no acknowledgement is
        returned.

        Arguments:
            variable: Specifier. PCTM performs its action based on this parameter.
            value   : Optional value. For example used when setting sampling
                frequency or temperature setpoint.
        """
        if value is None:
            cmd = struct.pack("<H", variable.value)
        else:
            cmd = struct.pack("<Hf", variable.value, value)
        if self.link is not None and self.link.open():
            try:
                self.txqueue.put_nowait((cmd, variable._packID(), uuid4()))
            except Full:
                logger.exception(f"Could not send command {variable}, {value}. "
                                 "The transmission queue is full.")

    def _request_uptime(self) -> None:
        """Request the number of seconds since the PCTM booted."""
        self._sendCommand(Requests.GET_UPTIME)

    def stop(self) -> None:
        """Stops this handler from communicating any more with the PCTM."""
        self._running = False

    def _comReader(self) -> None:
        """Continuously reads data arriving on the com port. Also sends commands
        and waits for acknowledgement. Will run until self._running is False or
        an exception occurs."""
        last_uuid = None
        retries = 0
        retry_lim = 10
        command_time = 0.0
        try:
            while self._running:
                if self.link is not None and self.link.open():
                    bytes_read = self.link.available()
                    while (not bytes_read) and self._running:
                        if (self.txqueue.qsize() != 0 and self.last_tx_ack) or (
                                not self.last_tx_ack and time.time() - self.last_tx_time > 0.1):
                            # Send the first command in the tx queue if it exists and the last command was acknowledged.
                            # Or send the first command if the last was not acknowledged.
                            # We resend the command every 100ms until it is ackowledged,
                            # or until it has been sent 5 times, whichever comes first.
                            cmd, id, uuid = self.txqueue.queue[0]  # Get command without dequeueing it
                            if last_uuid == uuid:
                                retries += 1
                            else:
                                retries = 0
                            if retries < retry_lim:
                                attempt = retries + 1 if retries > 0 else None
                                self._tx(cmd, id, attempt)
                                if retries == 0:
                                    command_time = time.time()
                                last_uuid = uuid
                            else:
                                logger.warning(f"Arduino did not acknowledge "
                                               f"(PackID {id}, Cmd {cmd}) after "
                                               f"{retry_lim} retries.")
                                self.txqueue.get_nowait()
                                self.last_tx_ack = True
                        status = self.link.status
                        if status < 0:
                            if status == txfer.CRC_ERROR:
                                logger.error(f'{self.link.connection.port} : CRC_ERROR')
                            elif status == txfer.PAYLOAD_ERROR:
                                logger.error(f'{self.link.connection.port} : PAYLOAD ERROR')
                            elif status == txfer.STOP_BYTE_ERROR:
                                logger.error(f'{self.link.connection.port} : STOP BYTE ERROR')
                            else:
                                logger.error(f'{self.link.connection.port} : ERROR STATUS {status}')
                        bytes_read = self.link.available()
                    if self._running:
                        # Parse the received bytes
                        buff = bytearray(self.link.rxBuff[0:bytes_read])
                        id = self.link.idByte
                        if id == RxPacketID.SAMPLE_DATA.value:
                            sample = DefaultSample(buff, self._sync_time)
                            tup = sample.sampleTuple()
                            if tup and sample.source is not None:
                                self.putSample(sample.source, tup)
                        elif id == RxPacketID.ACK.value:
                            # ret:Any = struct.unpack("<BHf", buff)
                            p_id, p_cmd = (buff[0], buff[1:])
                            if self.txqueue.qsize() > 0:
                                delay = (time.time() - command_time)*1000
                                q_cmd, q_id, _ = self.txqueue.queue[0]
                                if q_id == p_id and q_cmd == p_cmd[:len(q_cmd)]:
                                    self.txqueue.get_nowait()
                                    logger.info(f"Acknowledged after {delay:.1f} ms")
                                    self.last_tx_ack = True
                        else:
                            self.rxdatahandler.handleData(buff, id)
        except:
            # Any exception that may occur is logged and we also make sure to close the link
            # in all cases with the 'finally' clause
            if self.link is not None:
                logger.exception("An exception occurred in the communication thread "
                                 f"for the PCTM on port {self.link.connection.port}.")
            else:
                logger.exception("An exception occurred in a communication thread.")
        finally:
            if self.link is not None:
                self.link.close()
            time.sleep(1)

    def _tx(self, cmd: bytearray, id: int, attempt: int = None) -> None:
        """Takes care of the actual transmission of a bytearray to the PCTM."""
        addstr = ""
        if attempt is not None:
            addstr = f" Attempt number {attempt}."

        if self.link is not None:
            l = len(cmd)
            self.link.txBuff[0:l] = cmd
            self.link.send(l, id)
            self.last_tx_ack = False
            self.last_tx_time = time.time()
            logger.info(f"Sending Txbuffer: {self.link.txBuff[0:l]}, Packet ID: {id}.{addstr}")

    def join(self, *args: Any, **kwargs: Any) -> None:
        """Joins this thread. Must be called before the application exits,
        otherwise the thread lives on and haunts you."""
        logger.info(f"Joining handler {self}...")
        if self.syncTimer.is_alive():
            self.syncTimer.cancel()
        if self.comThread.is_alive():
            self.disconnect()
            self.comThread.join(*args, **kwargs)
        logger.info("Joined succesfully.")

    def __populateDataStruct(self) -> None:
        # The data struct is populated down here to keep the init less crowded
        for source in SampleSource:
            self.data.update({source: RingBuffer(int(1E6), dtype=DefaultSample._dtype_np)})


class timedRoutine(threading.Timer):
    """This is a subclass of the Timer class from the threading library. It allows
    a function to be called repeatedly with a certain interval. Currently used
    by the PCTMHandler class to maintain synchronization with the PCTM."""

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

    @no_type_check  # This cannot be type checked
    def run(self) -> None:
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)
        logger.info("syncTimer stopped.")


class DefaultSample(object):
    """This class represents a "normal" sample returned from the PCTM. Currently,
    all samples are of this type. The DefaultSample class has the following three
    variables:
        source  : A SampleSource enum that designates what the sample is of
        value   : The analogue value of the sample
        time    : The time of which the sample was made, in seconds since the PCTM booted
        counts  : The raw ADC counts of the sample

    Keyword arguments:
        buffer      : A Bytearray that will be unpacked according to self._dtype_C
        boot_time   : A float representing the POSIX time when the PCTM was booted.
    """
    # __dtype defines how to unpack the byte package from the PCTM.
    # __dtype_c defines the dtype for the structured array that the sample will later enter
    _dtype_C = "<BffH"  # Unsigned char, Float, Float, Unsigned 16bit integer
    _dtype_np = [('value', np.float16), ('counts', np.uint16), ('time', np.float64)]

    def __init__(self, buffer: bytearray, boot_time: float = 0) -> None:
        self._boot_time = boot_time
        self.source: Optional[SampleSource] = None
        self.value: Optional[np.float32] = None
        self.time: Optional[np.float32] = None
        self.counts: Optional[np.uint16] = None
        self.unpack(buffer)

    def sampleTuple(self) -> Optional[Tuple[np.float32, np.uint16, np.float64]]:
        """Returns the sample as a tuple of the form (value, counts, timestamp).
        Value is the actual analog value of the sample.
        Counts is the ADC counts value if it is available.
        Timestamp is a UNIX timestamp with millisecond precision designating when
            the sample was taken (not received)."""
        try:
            assert self.value is not None
            assert self.counts is not None
            assert self.time is not None
        except AssertionError:
            return None
        return (self.value, self.counts, self.time / 1000 + self._boot_time)

    def unpack(self, buffer: bytearray) -> None:
        """Unpacks a bytearray as received from the PCTM, containing one sample."""
        try:
            s = struct.unpack(self._dtype_C, buffer)
        except:
            logger.exception(f"Could not decode sample buffer {buffer.hex()} with "
                             f"unpack string \"{self._dtype_C}\".")
            return
        source_enum = s[0]
        val = s[1]
        time = s[2]
        counts = s[3]
        if source_enum not in set(src.value for src in SampleSource):
            raise TypeError(f"The source enumerator {source_enum} is not defined "
                            "in the SampleSource enumerator class.")
        # if time < 0:
            # raise ValueError(f"Sample time is negative: {time} seconds")

        self.source = SampleSource(source_enum)
        self.value = val
        self.time = time
        self.counts = counts


@traced(logger, "__init__", "_getSpecifierInt", "_unpack", exclude=True)
class RxDataHandler(QtCore.QObject):
    """
    This class handles and unpacks all data (except samples) that is
    received from the PCTM. Sample data is taken care of by the DefaultSample
    class.
    Sample frequencies and PID settings reported by the PCTM are emitted as events.

    Keyword arguments:
        handler     : The PCTMHandler this RxDataHandler works for.
    """
    freq_rx = QtCore.pyqtSignal(tuple)
    pid_rx = QtCore.pyqtSignal(tuple)
    interlock_rx = QtCore.pyqtSignal(tuple)
    pctm_rx = QtCore.pyqtSignal(tuple)

    def __init__(self, handler: 'PCTMHandler') -> None:
        super().__init__()
        self.handler = handler

    def handleData(self, buffer: bytearray, packetID: int) -> None:
        """Unpacks a bytearray buffer as received from the PCTM. The packetID
        and the specifier enum defines how to act on the data."""
        packetID_enum = RxPacketID(packetID)  # Get the enum representation of the packet ID int
        if packetID_enum == RxPacketID.TEXT:  # Text data is a special case
            data = buffer.decode('utf8').split('\x00')[0]
            specifier_enum: 'spenum_t' = packetID_enum
        else:
            specifier_int = self._getSpecifierInt(buffer)
            ctype, specifier_enum = self._getPacketInfo(packetID_enum, specifier_int)
            data = self._unpack(buffer[2:], ctype)
        if data is not None:
            self._takeAction(data, specifier_enum)

    def _getSpecifierInt(self, buffer: bytearray) -> int:
        """Unpacks the specifier, i.e. the integer that the packet begins
        with."""
        val: int = struct.unpack("<B", buffer[0:1])[0]
        return val

    def _getPacketInfo(self, packetID: RxPacketID, specifier_int: int) -> Tuple[str, 'spenum_t']:
        """Figures out the ctype needed to unpack the data and what the
        specifier enum is."""
        SystemInfo_types = {SystemInfo.UPTIME: "<LL",
                            SystemInfo.SYNC_READY: ""}
        RxPacketID_types = {RxPacketID.SYS_INFO: (SystemInfo_types, None, SystemInfo),
                            RxPacketID.SAMPLE_FREQ: ({}, "<f", MonitorFSettings),
                            RxPacketID.PID_INFO: ({}, "<f", PeltierSettings),
                            RxPacketID.INTERLOCK_INFO: ({}, "<f", InterlockSettings),
                            RxPacketID.SYS_SETTING_INFO: ({}, "<f", SystemSettings)}
        types, default, enum = RxPacketID_types.get(packetID, (None, None, None))
        if types is None or enum is None:
            raise ValueError(f"The packet ID enum \"{packetID}\" does not have a "
                             "defined type struct.")
        specifier_enum = enum(specifier_int)
        ctype: str = types.get(specifier_enum, default)  # type: ignore #this is always a dict, but mypu thinks "object"
        if ctype is None:
            raise ValueError(f"The specifier enum \"{specifier_enum}\" does not "
                             "have a defined type struct.")
        return ctype, specifier_enum  # type:ignore

    def _unpack(self, buffer: bytearray, ctype: str) -> Any:
        """Unpacks the bytearray 'buffer' using the type string 'ctype'. In case
        it fails, the exception is logged and None is returned."""
        try:
            s: Any = struct.unpack(ctype, buffer)
        except:
            logger.exception(f"Could not decode buffer {buffer}")
            s = None
        return s

    def _takeAction(self, data: Any,
                    specifier_enum: 'spenum_t'
                    ) -> None:
        """Performs actions based on the data that was received from the PCTM."""
        if specifier_enum == SystemInfo.UPTIME:
            self.handler.uptime = (data[0] * (pow(2, 32) - 1) + data[1]) / 1000
        if specifier_enum == SystemInfo.SYNC_READY:
            # We now know that the PCTM is in a blocking function waiting for a sync
            # command. We must, as fast as possible, perform the synchronization
            # to not waste time in the blocking call when we could be sampling.
            var = Requests.DO_SYNC
            cmd = struct.pack("<H", var.value)
            if self.handler.link is not None and self.handler.link.open():
                self.handler.link.txBuff[0:len(cmd)] = cmd
                self.handler.link.send(len(cmd), var._packID())
                self.handler._sync_time = time.time()
        if specifier_enum in MonitorFSettings:
            self.freq_rx.emit((specifier_enum, data[0]))
        if specifier_enum in PeltierSettings:
            self.pid_rx.emit((specifier_enum, data[0]))
        if specifier_enum in InterlockSettings:
            self.interlock_rx.emit((specifier_enum, data[0]))
        if specifier_enum in SystemSettings:
            self.pctm_rx.emit((specifier_enum, data[0]))
        if specifier_enum == RxPacketID.TEXT:
            logger.info(f"PCTM says : \"{data}\"")
