# -*- coding: utf-8 -*-
"""
Created on Thu JULY 08 ‏‎17:01:09 ‎2021

@author: Wai Chun Leung

This file contains the Influx class which handles the logging of data from the 
PCTM to a influx db.
"""

import influxdb_client # type: ignore
from influxdb_client import WritePrecision
from influxdb_client.client.write_api import ASYNCHRONOUS  # type: ignore
from autologging import traced  # type: ignore
from .enums import SampleSource, InterlockSettings
from .definitions import influxdb_key_name_definitions
from . import loggers
from src.definitions import PCTMInfluxException
from typing import Dict, Any, Optional, Tuple

logger = loggers.get_logger(__name__)

@traced(logger, "writeInterlockInflux", "__init__", "writeSampleInflux", exclude=True)
class Influx():
    """Influx class handles the logging of data and connections from PCTM to an Influx db. """
    
    def __init__(self) -> None:
        self.client : influxdb_client.InfluxDBClient = influxdb_client.InfluxDBClient(url="", token = "", org = "")
        self.write_api = self.client.write_api(write_options=ASYNCHRONOUS)
        self.connections: Dict[Any, Any] = {}
        self.influx_enabled = False
         
    def connectInflux(self, c: Dict[Any, Any]) -> None:
        self.connections = c
        self.influx_enabled = False
        
        if self.connections.get('username') == " ":
            token_new = self.connections['password']
        else:
            token_new = f"{self.connections['username']}:{self.connections['password']}"
    
        self.client = influxdb_client.InfluxDBClient(
            url=f"{self.connections['host']}:{self.connections['port']}",
            token = token_new,
            org = self.connections['organization'])
        
        self.write_api = self.client.write_api(write_options=ASYNCHRONOUS)
        
    def writeSampleInflux(self, source: SampleSource, datum: Tuple[float, int, float]) -> None:
        if self.connections.get(source.name) == 2:
            dataPoint = influxdb_client.Point(self.connections['measurement']).tag("location", self.connections['location']).field(influxdb_key_name_definitions[source], datum[0]).time(int(datum[2]*1000), write_precision=WritePrecision.MS)
            write_results = self.write_api.write(bucket=f"{self.connections['database']}", record=dataPoint)
            try:
                write_results.get()
            except:
                raise
                
    def writeInterlockInflux(self, source: InterlockSettings, datum: Tuple[Any, Any]) -> None:
        if self.connections.get(source.name) == 2:
            dataPoint = influxdb_client.Point(self.connections['measurement']).tag("location", self.connections['location']).field(influxdb_key_name_definitions[source], datum[0]).time(int(datum[1]*1000), write_precision=WritePrecision.MS)
            write_results = self.write_api.write(bucket=f"{self.connections['database']}", record=dataPoint)
            try:
                write_results.get()
            except:
                raise

    def checkConnection(self) -> bool:
        try:
            health = self.client.health()
        except:
            raise 
         
        if health.status == "pass":
            logger.info(f"Influx DB found at {self.connections['host']}:{self.connections['port']}")
            try:
                logger.info(f"Connecting to Influx DB at {self.connections['host']}:{self.connections['port']}")
                dataPoint = influxdb_client.Point(self.connections['measurement'])
                write_results = self.write_api.write(bucket=f"{self.connections['database']}", record=dataPoint)
                write_results.get()
                self.influx_enabled = True
                return True
            except Exception as e:
                if str(e)[1:4] == "400": #Ignore Error Code 400. 
                    self.influx_enabled = True
                    return True
                else:
                    raise
        else:
            raise PCTMInfluxException(f"Failed to find an Influx DB at {self.connections['host']}:{self.connections['port']}: {health.message}")
            
        

        
        