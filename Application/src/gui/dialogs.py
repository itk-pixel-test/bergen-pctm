# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 20:30:02 2020

@author: magne.lauritzen

A file containing various dialogs used in the PCTM-GUI application.
"""
from .gui_src.ui_pctmconnector import Ui_PCTMConnectionDialog
from .gui_src.ui_pid_settings import Ui_pidSettingsDialog
from .gui_src.ui_simple_selector import Ui_SimpleSelector
from .gui_src.ui_logging_settings import Ui_loggingSettingsDialog
from .gui_src.ui_pctm_settings import Ui_pctmSettingsDialog
from .gui_src.ui_advancedPCTMsettings import Ui_AdvancedPCTMSettingsDialog
from .gui_src.ui_psu_pid_settings import Ui_psuPidSettingsDialog
from .gui_src.ui_influx_settings import Ui_influxSettingsDialog
from .gui_src.ui_thermal_cycle_settings import Ui_thermalCycleSettingDialog
from .gui_src.ui_interlock_settings import Ui_InterlockSettingDialog
from .. import enums
from ..enums import SampleSource as ss
from ..enums import MonitorFSettings as mfs
from ..enums import PeltierSettings as ps
from ..enums import ThermalCycle as tc
from ..enums import InterlockSettings as il
import json
import traceback
import pyvisa # type: ignore
from pathlib import Path
from PyQt5.QtWidgets import QDialog, QButtonGroup  # type: ignore
from PyQt5 import QtWidgets  # type: ignore
from PyQt5.QtWidgets import QFileDialog as Qfd
from PyQt5.QtGui import QMessageBox, QKeyEvent  # type: ignore
from PyQt5 import QtCore
from typing import List, Dict, Optional, Any, Tuple, TYPE_CHECKING
from contextlib import redirect_stdout
from autologging import traced  # type: ignore
from io import StringIO
from ..appconfig import AppConfig
from .. import loggers

logger = loggers.get_logger(__name__)
config = AppConfig()

if TYPE_CHECKING:
    from ..pctm import PCTMAggregator
    from ..pctmhandler import PCTMHandler
    from enum import Enum


@traced(logger)
class AdvancedPctmSettingsDialog(QDialog, Ui_AdvancedPCTMSettingsDialog):
    """A dialog for setting advanced PCTM settings."""

    def __init__(self, handler: 'PCTMHandler') -> None:
        super(AdvancedPctmSettingsDialog, self).__init__()
        self.setupUi(self)
        self.pctmhandler = handler
        self.connectSignals()

    def connectSignals(self) -> None:
        self.id_upload_pushButton.clicked.connect(self._uploadID)
        self.calibrate_pushButton.clicked.connect(self._calibrate)
        self.reset_pushButton.clicked.connect(self._reset)

    def _uploadID(self, _: Any) -> None:
        self.pctmhandler.setId(self.id_spinBox.value())

    def _calibrate(self, _: Any) -> None:
        self.pctmhandler.calibrateISensor()

    def _reset(self, _: Any) -> None:
        self.pctmhandler.clearNonvolatileSettings()


@traced(logger, "_loadSettings", "_saveSettings", "getSettings", "_setSetting")
class BaseSaveableSettingsDialog():
    """
    BaseSaveableSettingsDialog is a base class for all dialogs that presents
    saveable / loadable settings.

    You must define self._map in the subclass. It is a dict of {enum:input element}.
    See the two settings dialog classes PctmSettingsDialog and LoggingSettingsDialog
    for examples of how to use.

    Keyword arguments:
        type    : A string used in the dialog title, logger statements, file
            dialogs, and more.
    """

    def __init__(self, type: str) -> None:
        self.type = type
        self.settings = QtCore.QSettings()
        self._map: Dict['Enum', Any] = {}

    def saveLoadSettings(self, action: str) -> None:
        """
        Save or load the settings to file. Argument 'action' is 'save' or
        'load'. Opens a file explorer dialog where the user can select the file.
        """
        save = action == 'save'
        p = str(self.settings.value(f"{self.type}settings/path",
                                    (config.get_userdatadir())))
        if save:
            fname, _ = Qfd.getSaveFileName(self, caption=f"Save {self.type} settings",
                                           directory=p, filter="JSON (*.json)")
        else:
            fname, _ = Qfd.getOpenFileName(self, caption=f"Load {self.type} settings",
                                           directory=p, filter="JSON (*.json)")
        if fname != "":
            if fname.endswith(".json") != True:
                fname = fname + ".json"
            fpath = Path(fname)
            if save:
                self._saveSettings(fpath)
            else:
                self._loadSettings(fpath)
            p = self.settings.setValue(f"{self.type}settings/path", fpath.parents[0])

    def getSettings(self) -> Dict[Any, Any]:
        """Returns a dict of the current settings in the dialog."""
        d = {}
        for enum, spinbox in self._map.items():
            d.update({enum: spinbox.value()})
        return d

    def setSettings(self, d: Dict[Any, Optional[Any]]) -> None:
        """Sets the current settings in the dialog from a dict."""
        for enum, value in d.items():
            self._setSetting(enum, value)

    def keyPressEvent(self, evt: QKeyEvent) -> None:
        """Reimplementation that blocks Enter from closing the dialog"""
        if evt.key() == QtCore.Qt.Key_Enter or evt.key() == QtCore.Qt.Key_Return:
            return
        QDialog.keyPressEvent(evt)

    def _loadSettings(self, path: Path) -> None:
        """Loads the settings from a file given by 'path'"""
        try:
            with open(path, 'r') as f:
                _d = json.load(f)
                d: Dict['Enum', Optional[float]] = {}
                for enum_str, value in _d.items():
                    try:
                        enum_class_str, enum_name = enum_str.split(".")
                        enum_class = getattr(enums, enum_class_str)
                        d[enum_class[enum_name]] = _d.get(enum_str)
                    except KeyError:
                        logger.warning(f"No enum item named {enum_name} in {enum_class_str}.")
                    except AttributeError:
                        logger.warning(f"No enum class named {enum_class_str} in "
                                       "enums.py. The loaded file may be too old. "
                                       f"Skipping loading of \"{enum_name}\"")
                self.setSettings(d)
        except Exception as e:
            logger.exception(f"Could not load {self.type} settings file")
            ExceptionDialog(e)

    def _saveSettings(self, path: Path) -> None:
        """Saves the settings to a file given by 'path'"""
        try:
            _d = self.getSettings()
            d: Dict[str, float] = {}
            for enum in _d.keys():
                d[str(enum)] = _d.get(enum, 0)
            with open(path, 'w') as f:
                json.dump(d, f)
        except Exception as e:
            logger.exception(f"Could not save {self.type} settings file")
            ExceptionDialog(e)

    def _setSetting(self, enum: 'Enum', value: Optional[Any]) -> None:
        """Sets the value of one setting in the dialog."""
        if value is not None:
            self._map[enum].setValue(value)


class PctmSettingsDialog(QDialog, Ui_pctmSettingsDialog, BaseSaveableSettingsDialog):
    """A dialog for setting PCTM settings. Provides saving and loading
    functionality by subclassing BaseSaveableSettingsDialog. """

    def __init__(self) -> None:
        QDialog.__init__(self)
        Ui_pctmSettingsDialog.__init__(self)
        BaseSaveableSettingsDialog.__init__(self, "PCTM")
        self.setupUi(self)
        self.connectSignals()
        self._map = {mfs.NTC_1_F: self.NTC1DoubleSpinBox,
                     mfs.NTC_2_F: self.NTC2DoubleSpinBox,
                     mfs.NTC_3_F: self.NTC3DoubleSpinBox,
                     mfs.HUMIDITY_1_F: self.humidity1DoubleSpinBox,
                     mfs.HUMIDITY_2_F: self.humidity2DoubleSpinBox,
                     mfs.CP_T_F: self.coldplateDoubleSpinBox,
                     mfs.TEC: self.peltierDoubleSpinBox,
                     mfs.VC_T_F: self.vacuumChuckDoubleSpinBox,
                     mfs.GAS_FLOW_F: self.gasFlowDoubleSpinBox,
                     mfs.PID: self.pIDDoubleSpinBox,
                     mfs.VACUUM_PRESSURE: self.vacuumPressureDoubleSpinBox,
                     }

    def connectSignals(self) -> None:
        self.setAllNTCdoubleSpinBox.valueChanged.connect(self._setAllNTC)
        self.setAllHumdoubleSpinBox.valueChanged.connect(self._setAllHum)
        self.setAllPeltierdoubleSpinBox.valueChanged.connect(self._setAllPeltier)
        self.save_pushButton.clicked.connect(lambda: self.saveLoadSettings('save'))
        self.load_pushButton.clicked.connect(lambda: self.saveLoadSettings('load'))

    def _setAllNTC(self, val: float) -> None:
        self._map[mfs.NTC_1_F].setValue(val)
        self._map[mfs.NTC_2_F].setValue(val)
        self._map[mfs.NTC_3_F].setValue(val)

    def _setAllHum(self, val: float) -> None:
        self._map[mfs.HUMIDITY_1_F].setValue(val)
        self._map[mfs.HUMIDITY_2_F].setValue(val)

    def _setAllPeltier(self, val: float) -> None:
        self._map[mfs.CP_T_F].setValue(val)
        self._map[mfs.TEC].setValue(val)
        self._map[mfs.PID].setValue(val)

    def _updateFromPCTM(self, tup: Tuple['Enum', float]) -> None:
        """Updates the settings directly from the PCTM. This method is called
        when the PCTM returns its settings."""
        enum, val = tup
        control = self._map.get(enum, None)
        if control is None:
            logger.warning(f"The enum {enum} cannot be found in _map.")
            return
        control.setValue(val)


class LoggingSettingsDialog(QDialog, Ui_loggingSettingsDialog, BaseSaveableSettingsDialog):
    """A dialog for setting logging frequencies. Provides saving and loading
    functionality by subclassing BaseSaveableSettingsDialog. """

    def __init__(self) -> None:
        QDialog.__init__(self)
        Ui_loggingSettingsDialog.__init__(self)
        BaseSaveableSettingsDialog.__init__(self, "logging")
        self.setupUi(self)
        self.connectSignals()
        self._map = {ss.NTC_1: self.moduleNTC1DoubleSpinBox,
                     ss.NTC_2: self.moduleNTC2DoubleSpinBox,
                     ss.NTC_3: self.moduleNTC3DoubleSpinBox,
                     ss.DP_1: self.dewPointDoubleSpinBox,
                     ss.RH_1: self.relativeHumidityDoubleSpinBox,
                     ss.GAS_T_1: self.gasTemperatureDoubleSpinBox,
                     ss.DP_2: self.dewPointDoubleSpinBox_2,
                     ss.RH_2: self.relativeHumidityDoubleSpinBox_2,
                     ss.GAS_T_2: self.gasTemperatureDoubleSpinBox_2,
                     ss.VC_T: self.vacuumChuckTemperatureDoubleSpinBox,
                     ss.CP_T: self.coldplateTemperatureDoubleSpinBox,
                     ss.TEC_PID_SETPOINT: self.setpointDoubleSpinBox,
                     ss.TEC_I: self.peltierCurrentDoubleSpinBox,
                     ss.TEC_V: self.peltierVoltageDoubleSpinBox,
                     ss.GAS_FLOW: self.gasFlowDoubleSpinBox,
                     ss.VACUUM_PRESSURE: self.vacuumLoggingDoubleSpinBox
                     }

    def connectSignals(self) -> None:
        self.onlyLogNTC1pushButton.clicked.connect(self._onlyLogNTC1)
        self.onlyLogDP1pushButton.clicked.connect(self._onlyLogDP1)
        self.onlyLogDP2pushButton.clicked.connect(self._onlyLogDP2)
        self.onlyLogCurrentAndVoltagepushButton.clicked.connect(self._onlyLogTECIandV)
        self.setAllHum1doubleSpinbox.valueChanged.connect(self._setAllHum1)
        self.setAllHum2doubleSpinbox.valueChanged.connect(self._setAllHum2)
        self.setAllNTCdoubleSpinbox.valueChanged.connect(self._setAllNTC)
        self.setAllPeltierdoubleSpinbox.valueChanged.connect(self._setAllPeltier)
        self.save_pushButton.clicked.connect(lambda: self.saveLoadSettings('save'))
        self.load_pushButton.clicked.connect(lambda: self.saveLoadSettings('load'))

    def _onlyLogNTC1(self) -> None:
        self._setSetting(ss.NTC_2, 0)
        self._setSetting(ss.NTC_3, 0)

    def _onlyLogDP1(self) -> None:
        self._setSetting(ss.RH_1, 0)
        self._setSetting(ss.GAS_T_1, 0)

    def _onlyLogDP2(self) -> None:
        self._setSetting(ss.RH_2, 0)
        self._setSetting(ss.GAS_T_2, 0)

    def _onlyLogTECIandV(self) -> None:
        self._setSetting(ss.CP_T, 0)
        self._setSetting(ss.TEC_PID_SETPOINT, 0)

    def _setAllHum1(self, val: float) -> None:
        self._map[ss.DP_1].setValue(val)
        self._map[ss.RH_1].setValue(val)
        self._map[ss.GAS_T_1].setValue(val)

    def _setAllHum2(self, val: float) -> None:
        self._map[ss.DP_2].setValue(val)
        self._map[ss.RH_2].setValue(val)
        self._map[ss.GAS_T_2].setValue(val)

    def _setAllNTC(self, val: float) -> None:
        self._map[ss.NTC_1].setValue(val)
        self._map[ss.NTC_2].setValue(val)
        self._map[ss.NTC_3].setValue(val)

    def _setAllPeltier(self, val: float) -> None:
        self._map[ss.CP_T].setValue(val)
        self._map[ss.TEC_PID_SETPOINT].setValue(val)
        self._map[ss.TEC_I].setValue(val)
        self._map[ss.TEC_V].setValue(val)


class PsuPidSettingsDialog(QDialog, Ui_psuPidSettingsDialog, BaseSaveableSettingsDialog):
    """A dialog to set the PSU PID settings."""

    def __init__(self) -> None:
        QDialog.__init__(self)
        Ui_psuPidSettingsDialog.__init__(self)
        BaseSaveableSettingsDialog.__init__(self, "logging")
        self.setupUi(self)
        self.connectSignals()
        # self._map = {ps.KP_PSU: self.pid_kp_spinBox,
                     # ps.KI_PSU: self.pid_ki_spinBox,
                     # ps.KD_PSU: self.pid_kp_spinBox,
                     # ps.CCTRL: self.manual_doubleSpinBox,
                     # ps.PSU_FREQ: self.psu_freq_doubleSpinBox}

    def connectSignals(self) -> None:
        self.save_pushButton.clicked.connect(lambda: self.saveLoadSettings('save'))
        self.load_pushButton.clicked.connect(lambda: self.saveLoadSettings('load'))
        self.manual_radioButton.toggled.connect(self.setManualSpinBoxes)
        self.manual_doubleSpinBox.valueChanged.connect(self.setAnalogValue)
        self.analogdoubleSpinBox.valueChanged.connect(self.setManualValue)

    def setAnalogValue(self) -> None:
        self.analogdoubleSpinBox.blockSignals(True)
        maxValue = 1.0 if self.imax_doubleSpinBox.value() == 0 else self.imax_doubleSpinBox.value()
        analogValue = (self.manual_doubleSpinBox.value() / maxValue) * 255
        self.analogdoubleSpinBox.setValue(analogValue)
        self.analogdoubleSpinBox.blockSignals(False)

    def setManualValue(self) -> None:
        self.manual_doubleSpinBox.blockSignals(True)
        manualValue = (self.analogdoubleSpinBox.value() / 255) * self.imax_doubleSpinBox.value()
        self.manual_doubleSpinBox.setValue(manualValue)
        self.manual_doubleSpinBox.blockSignals(False)

    def setManualSpinBoxes(self, toggled: bool) -> None:
        if toggled:
            self.manual_doubleSpinBox.setEnabled(True)
            self.analogdoubleSpinBox.setEnabled(True)
        else:
            self.manual_doubleSpinBox.setEnabled(False)
            self.analogdoubleSpinBox.setEnabled(False)

    def _loadSettings(self, path: Path) -> None:
        """Loads the settings from a file given by 'path'"""
        try:
            with open(path, 'r') as f:
                d = json.load(f)
                self.setSettings(d)
        except Exception as e:
            logger.exception(f"Could not load {self.type} settings file")
            ExceptionDialog(e)

    def getSettings(self) -> Dict[str, Any]:
        if self.PID_radioButton.isChecked():
            mode = ps.PID_PSU
        else:
            mode = ps.CCTRL

        d = {'pid_parameters':
            {
                'kp': self.pid_kp_spinBox.value(),
                'ki': self.pid_ki_spinBox.value(),
                'kd': self.pid_kd_spinBox.value()
            },
            'psu_sample_freq':
            {
                'psu_freq' : self.psu_freq_doubleSpinBox.value()
            },
            'psu_details':
            {
                'imax': self.imax_doubleSpinBox.value(),
                'psu_comm': self.psucomm_comboBox.currentIndex()
            },
            'control_mode':
                {
                    'cctrl': self.manual_doubleSpinBox.value(),
                    'analog': self.analogdoubleSpinBox.value(),
                    'mode': mode.value
                }
        }
        return d

    def setSettings(self, d: Dict[str, Any]) -> None:
        if d.get('pid_parameters', None) is not None:
            if d['pid_parameters'].get('kp', None) is not None:
                self.pid_kp_spinBox.setValue(d['pid_parameters'].get('kp'))
            if d['pid_parameters'].get('ki', None) is not None:
                self.pid_ki_spinBox.setValue(d['pid_parameters'].get('ki'))
            if d['pid_parameters'].get('kd', None) is not None:
                self.pid_kd_spinBox.setValue(d['pid_parameters'].get('kd'))

        if d.get('psu_sample_freq', None) is not None:
            if d['psu_sample_freq'].get('psu_freq', None) is not None:
                self.psu_freq_doubleSpinBox.setValue(d['psu_sample_freq'].get('psu_freq'))

        if d.get('psu_details', None) is not None:
            if d['psu_details'].get('imax', None) is not None:
                self.imax_doubleSpinBox.setValue(d['psu_details'].get('imax'))
            if d['psu_details'].get('psu_comm', None) is not None:
                self.psucomm_comboBox.setCurrentIndex(d['psu_details'].get('psu_comm'))

        if d.get('control_mode', None) is not None:
            if d['control_mode'].get('cctrl', None) is not None:
                self.manual_doubleSpinBox.setValue(d['control_mode'].get('cctrl'))
            if d['control_mode'].get('analog', None) is not None:
                self.analogdoubleSpinBox.setValue(d['control_mode'].get('analog'))
            if d['control_mode'].get('mode', None) is not None:
                mode = ps(d['control_mode']['mode'])
                if mode == ps.PID_PSU:
                    self.PID_radioButton.toggle()
                else:
                    self.manual_radioButton.toggle()

    def _updateFromPCTM(self, tup: Tuple['Enum', float]) -> None:
        """Updates the settings directly from the PCTM. This method is called
        when the PCTM returns its PID settings."""
        enum, val = tup
        if enum == ps.KP_PSU:
            self.pid_kp_spinBox.setValue(val)
        if enum == ps.KI_PSU:
            self.pid_ki_spinBox.setValue(val)
        if enum == ps.KD_PSU:
            self.pid_kd_spinBox.setValue(val)
        if enum == ps.PSU_MAX:
            self.imax_doubleSpinBox.setValue(val)
        if enum == ps.COMM_MODE:
            self.psucomm_comboBox.setCurrentIndex(int(val))
        if enum == ps.PSU_FREQ:
            self.psu_freq_doubleSpinBox.setValue(val)

        if enum == ps.PSU_MANUAL:
            self.manual_radioButton.setChecked(True)
            self.manual_doubleSpinBox.setValue(val)
            self.manual_doubleSpinBox.setEnabled(True)
            self.analogdoubleSpinBox.setEnabled(True)

        if enum == ps.PSU_AUTO:
            self.PID_radioButton.setChecked(True)
            self.manual_doubleSpinBox.setValue(val)
            self.manual_doubleSpinBox.setDisabled(True)

class PidSettingsDialog(QDialog, Ui_pidSettingsDialog, BaseSaveableSettingsDialog):
    """A dialog to set the peltier controller PID settings."""

    def __init__(self) -> None:
        QDialog.__init__(self)
        Ui_pidSettingsDialog.__init__(self)
        BaseSaveableSettingsDialog.__init__(self, "logging")
        self.setupUi(self)
        self.connectSignals()
        # self._map = {ps.KP_TEC: self.pid_kp_spinBox,
                     # ps.KI_TEC: self.pid_ki_spinBox,
                     # ps.KD_TEC: self.pid_kp_spinBox,
                     # ps.VLIMIT: self.v_maxDoubleSpinBox,
                     # ps.ILIMIT: self.i_maxDoubleSpinBox,
                     # ps.CC: self.manualA_doubleSpinBox,
                     # ps.CV: self.manualV_doubleSpinBox}

    def connectSignals(self) -> None:
        self.save_pushButton.clicked.connect(lambda: self.saveLoadSettings('save'))
        self.load_pushButton.clicked.connect(lambda: self.saveLoadSettings('load'))

    def _loadSettings(self, path: Path) -> None:
        """Loads the settings from a file given by 'path'"""
        try:
            with open(path, 'r') as f:
                d = json.load(f)
                self.setSettings(d)
        except Exception as e:
            logger.exception(f"Could not load {self.type} settings file")
            ExceptionDialog(e)

    def getSettings(self) -> Dict[str, Any]:
        if self.PID_radioButton.isChecked():
            mode = ps.PID_TEC
        elif self.manualA_radioButton.isChecked():
            mode = ps.CC
        else:
            mode = ps.CV

        d = {'pid_parameters':
            {
                'kp': self.pid_kp_spinBox.value(),
                'ki': self.pid_ki_spinBox.value(),
                'kd': self.pid_kd_spinBox.value(),
                'pon': self.PonMcheckBox.isChecked()
            },
            'tec_sample_freq':
            {
                'tec_freq' : self.tec_freq_doubleSpinBox.value()
            },
            'limits':
                {
                    'vlimit': self.v_maxDoubleSpinBox.value(),
                    'ilimit': self.i_maxDoubleSpinBox.value()
                },
            'control_mode':
                {
                    'cc': self.manualA_doubleSpinBox.value(),
                    'cv': self.manualV_doubleSpinBox.value(),
                    'mode': mode.value
                }
        }
        return d

    def setSettings(self, d: Dict[str, Any]) -> None:
        if d.get('pid_parameters', None) is not None:
            if d['pid_parameters'].get('kp', None) is not None:
                self.pid_kp_spinBox.setValue(d['pid_parameters'].get('kp'))
            if d['pid_parameters'].get('ki', None) is not None:
                self.pid_ki_spinBox.setValue(d['pid_parameters'].get('ki'))
            if d['pid_parameters'].get('kd', None) is not None:
                self.pid_kd_spinBox.setValue(d['pid_parameters'].get('kd'))
            if d['pid_parameters'].get('pon', None) is not None:
                self.PonMcheckBox.setChecked(d['pid_parameters'].get('pon'))

        if d.get('tec_sample_freq', None) is not None:
            if d['tec_sample_freq'].get('tec_freq', None) is not None:
                self.tec_freq_doubleSpinBox.setValue(d['tec_sample_freq'].get('tec_freq'))

        if d.get('limits', None):
            if d['limits'].get('vlimit', None) is not None:
                self.v_maxDoubleSpinBox.setValue(d['limits'].get('vlimit'))
            if d['limits'].get('ilimit', None) is not None:
                self.i_maxDoubleSpinBox.setValue(d['limits'].get('ilimit'))

        if d.get('control_mode', None) is not None:
            if d['control_mode'].get('cc', None) is not None:
                self.manualA_doubleSpinBox.setValue(d['control_mode'].get('cc'))
            if d['control_mode'].get('cv', None) is not None:
                self.manualV_doubleSpinBox.setValue(d['control_mode'].get('cv'))
            if d['control_mode'].get('mode', None) is not None:
                mode = ps(d['control_mode']['mode'])
                if mode == ps.PID_TEC:
                    self.PID_radioButton.toggle()
                elif mode == ps.CC:
                    self.manualA_radioButton.toggle()
                else:
                    self.manualV_radioButton.toggle()

    def _updateFromPCTM(self, tup: Tuple['Enum', float]) -> None:
        """Updates the settings directly from the PCTM. This method is called
        when the PCTM returns its PID settings."""
        enum, val = tup
        if enum == ps.KP_TEC:
            self.pid_kp_spinBox.setValue(val)
        if enum == ps.KI_TEC:
            self.pid_ki_spinBox.setValue(val)
        if enum == ps.KD_TEC:
            self.pid_kd_spinBox.setValue(val)
        if enum == ps.PON_TEC:
            self.PonMcheckBox.setChecked(bool(val))
        if enum == ps.ILIMIT:
            self.i_maxDoubleSpinBox.setValue(val)
        if enum == ps.TEC_FREQ:
            self.tec_freq_doubleSpinBox.setValue(val)
        if enum == ps.TEC_MANUAL:
            self.manualA_radioButton.setChecked(True)
            self.manualA_doubleSpinBox.setValue(val)
            self.manualA_doubleSpinBox.setEnabled(True)
        if enum == ps.TEC_AUTO:
            self.PID_radioButton.setChecked(True)
            self.manualA_doubleSpinBox.setValue(val)
            self.manualA_doubleSpinBox.setDisabled(True)

class PCTMConnectionDialog(QDialog, Ui_PCTMConnectionDialog):
    """A dialog for connecting to PCTMs with a drop-down COM port list.
    Requires a PCTMAggregator class object which is used to connect to the
    PCTM."""

    def __init__(self, pctmaggr: 'PCTMAggregator') -> None:
        super(PCTMConnectionDialog, self).__init__()
        self.setupUi(self)
        self.pctmaggr = pctmaggr
        self.pctmaggr.signals.PCTMConnected.connect(self.updatePorts)
        self.pctmaggr.signals.PCTMDetached.connect(self.updatePorts)
        self.updatePorts()
        self.pushButton.clicked.connect(self.connect)

    def updatePorts(self, _: str = None) -> None:
        """Updates the list of ports where a PCTM appears to be present."""
        self.comboBox.clear()
        self.comboBox.addItems(self.pctmaggr.PCTMPorts)

    def connect(self) -> None:
        """Connects to PCTM on the currently selected COM port. This method is
        supposed to redirect the stdout messages to a text box in the connection
        dialog, but it is not working."""
        self.textBrowser.clear()
        buf = StringIO()
        with redirect_stdout(buf):  # Why is this not working?
            try:
                port = self.comboBox.currentText()
                self.pctmaggr.connectNewPCTM(port)
            except:
                logger.exception("An exception occurred when attempting to "
                                 "connect to the PCTM on port \"{port}\".")
            s = buf.getvalue()
            self.textBrowser.setText(s)  # Why is this not working?


class AutoConnectDialog(QMessageBox):
    """A dialog that asks the user if they want to connect to a PCTM on the
    port 'port'."""

    def __init__(self, port: str) -> None:
        super(AutoConnectDialog, self).__init__()
        self.port = port
        self.setIcon(QMessageBox.Question)
        self.setText(f"A PCTM has been detected on port {port}. Do you wish "
                     "to connect to it?")
        self.setWindowTitle("Autoconnect")
        self.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)

    def disconnected(self, port: str) -> None:
        if port == self.port:
            self.reject()


class ExceptionDialog(QMessageBox):
    """
    A dialog that can be used to display exceptions to the user. Will show
    the exception message and traceback.

    Arguments:
        exc     : The exception object to display.
    """

    def __init__(self, exc: Exception) -> None:
        super(ExceptionDialog, self).__init__()
        self.setIcon(QMessageBox.Critical)
        self.setText("An exception has occurred:")
        self.setInformativeText(str(exc))
        tb_list = traceback.format_exception(etype=type(exc), value=exc, tb=exc.__traceback__)
        self.setDetailedText("".join(tb_list))
        self.setWindowTitle("Exception")
        self.exec()

class WarningDialog(QMessageBox):
    """
    A dialog that can be used to display warnings to the user. Will show
    the warning message"""


    def __init__(self, warning: str) -> None:
        super(WarningDialog, self).__init__()
        self.setIcon(QMessageBox.Warning)
        self.setText("Warning:")
        self.setInformativeText(warning)
        #tb_list = traceback.format_exception(etype=type(exc), value=exc, tb=exc.__traceback__)
        #self.setDetailedText("".join(tb_list))
        self.setWindowTitle("Warning")
        self.exec()

@traced(logger)
class SimpleSelectorDialog(QDialog, Ui_SimpleSelector):
    """
    A very simple dialog for selecting items from a list.
    Get the selected items by accessing SimpleSelectorDialog.selected after
    the dialog is closed. This is a list of QListWidgetItem objects.

    Arguments
        items : List of string, each representing one selectable item
        text  : Explanatory text to display to user. Default: ""
        n_selectable : Number of items the user may select concurrently. Default: 1.
    """

    def __init__(self, items: List[str], text: str = "", n_selectable: int = 1) -> None:
        super(SimpleSelectorDialog, self).__init__()
        self.setupUi(self)
        self.label.setText(text)
        self.n_slct = n_selectable
        self.selected: List[QtWidgets.QListWidgetItem] = []
        # Connect selection changing
        self.listWidget.itemSelectionChanged.connect(self.selection_changed)
        # Connect cancel button
        self.buttonBox.rejected.connect(self.unselect_all)
        # Populate list
        for n, item in enumerate(items):
            self.listWidget.insertItem(n, item)

    def selection_changed(self) -> None:
        """Enforces maximum allowed simulataneous selections"""
        selection = self.listWidget.selectedItems()
        if self.n_slct == 1:
            citem = self.listWidget.currentItem()
            for item in selection:
                if item is not citem:
                    item.setSelected(False)
        elif len(selection) > self.n_slct:
            citem = self.listWidget.currentItem()
            citem.setSelected(False)
        self.selected = self.listWidget.selectedItems()

    def unselect_all(self) -> None:
        selection = self.listWidget.selectedItems()
        for item in selection:
            item.setSelected(False)
        self.selected = self.listWidget.selectedItems()


def GenericDialog(severity: str, text: str, info: str = "", title: str = None,
                  buttons: List[QtWidgets.QMessageBox.StandardButton] = None
                  ) -> QDialog.DialogCode:
    """
    Raises a generic modal dialog with information to the user.
    Defaults to showing only an "OK" button.
    This function is blocking.
    Arguments:
        severity    : String, type of dialog. [Question, Information, Warning, Critical]
        text        : Small text snippet.
        info        : Filling information. Optional.
        title       : Title of window. Optional.
        buttons     : List of QMessageBox buttons to display. Optional.
                        First item in list becomes default.
    Returns:
        The button that was clicked
    """
    severity = severity.lower()
    if severity not in ["question", "warning", "information", "critical"]:
        raise ValueError("Argument severity must be one of [Question, Information, Warning, Critical], "
                         f"not {severity}")
    msg = QtWidgets.QMessageBox()
    logger_message = text
    if info:
        logger_message += " --- " + info
    if severity == "question":
        logger.info(logger_message)
        msg.setIcon(QtWidgets.QMessageBox.Question)
        default_title = "Question"
    elif severity == "information":
        logger.info(logger_message)
        msg.setIcon(QtWidgets.QMessageBox.Information)
        default_title = "Information"
    elif severity == "warning":
        logger.warning(logger_message)
        msg.setIcon(QtWidgets.QMessageBox.Warning)
        default_title = "Warning"
    else:
        logger.critical(logger_message)
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        default_title = "Critical"

    if not title:
        title = default_title
    if buttons:
        buttons_or = 0
        for b in buttons:
            buttons_or = buttons_or | b
        msg.setStandardButtons(buttons_or)
        msg.setDefaultButton(buttons[0])
    msg.setText(text)
    msg.setInformativeText(info)
    msg.setWindowTitle(title)
    return msg.exec()

class InfluxSettingsDialog(QDialog, Ui_influxSettingsDialog, BaseSaveableSettingsDialog):
    """A dialog for setting Influx db settings."""

    def __init__(self) -> None:
        QDialog.__init__(self)
        Ui_influxSettingsDialog.__init__(self)
        BaseSaveableSettingsDialog.__init__(self, "Influx")
        self.setupUi(self)
        self.connectSignals()
        self._map = {ss.NTC_1: self.moduleNTC1CheckBox,
                     ss.NTC_2: self.moduleNTC2CheckBox,
                     ss.NTC_3: self.moduleNTC3CheckBox,
                     ss.DP_1: self.dewPointCheckBox,
                     ss.RH_1: self.relativeHumidityCheckBox,
                     ss.GAS_T_1: self.gasTemperatureCheckBox,
                     ss.DP_2: self.dewPointCheckBox_2,
                     ss.RH_2: self.relativeHumidityCheckBox_2,
                     ss.GAS_T_2: self.gasTemperatureCheckBox_2,
                     ss.VC_T: self.vacuumChuckTemperatureCheckBox,
                     ss.CP_T: self.coldPlateTemperatureCheckBox,
                     ss.TEC_PID_SETPOINT: self.setPointCheckBox,
                     ss.TEC_I: self.peltierCurrentCheckBox,
                     ss.TEC_V: self.peltierVoltageCheckBox,
                     ss.GAS_FLOW: self.purgeGasFlowCheckBox,
                     ss.VACUUM_PRESSURE: self.pressureSensorCheckBox,
                     il.ACTIVATED_HIGH: self.TemperatureHighcheckBox,
                     il.ACTIVATED_LOW: self.TemperatureLowcheckBox,
                     il.ACTIVATED_DEW: self.DewpointLimitcheckBox,
                     il.OVERHEAT: self.OverheatcheckBox,
                     il.ACTIVATED_ABSENT: self.AbsentCheckBox,
                     il.ACTIVATED_COLDPLATE: self.ColdplateCheckBox}

    def connectSignals(self) -> None:
        self.save_pushButton.clicked.connect(lambda: self.saveLoadSettings('save'))
        self.load_pushButton.clicked.connect(lambda: self.saveLoadSettings('load'))
        self.onlyLogNTC1pushButton.clicked.connect(self._onlyLogNTC1)
        self.onlyLogDP1pushButton.clicked.connect(self._onlyLogDP1)
        self.onlyLogDP2pushButton.clicked.connect(self._onlyLogDP2)
        self.onlyLogCurrentAndVoltagepushButton.clicked.connect(self._onlyLogTECIandV)

    def _onlyLogNTC1(self) -> None:
        self.moduleNTC1CheckBox.setCheckState(2)
        self.moduleNTC2CheckBox.setCheckState(0)
        self.moduleNTC3CheckBox.setCheckState(0)

    def _onlyLogDP1(self) -> None:
        self.dewPointCheckBox.setCheckState(2)
        self.relativeHumidityCheckBox.setCheckState(0)
        self.relativeHumidityCheckBox.setCheckState(0)

    def _onlyLogDP2(self) -> None:
        self.dewPointCheckBox_2.setCheckState(2)
        self.relativeHumidityCheckBox_2.setCheckState(0)
        self.relativeHumidityCheckBox_2.setCheckState(0)

    def _onlyLogTECIandV(self) -> None:
        self.peltierCurrentCheckBox.setCheckState(2)
        self.peltierVoltageCheckBox.setCheckState(2)
        self.setPointCheckBox.setCheckState(0)
        self.coldPlateTemperatureCheckBox.setCheckState(0)

    def getSettings(self) -> Dict[Any, Any]:
        d = {'database': self.database_lineEdit.text(),
             'host': self.host_lineEdit.text(),
             'port': self.port_lineEdit.text(),
             'measurement': self.measurement_lineEdit.text(),
             'location': self.location_lineEdit.text(),
             'username': self.username_lineEdit.text(),
             'password': self.passwordLineEdit.text(),
             'organization': self.organizationLineEdit.text()}

        for enum, checkbox in self._map.items():
            d.update({enum.name: checkbox.checkState()})

        return d

    def _loadSettings(self, path: Path) -> None:
        """Loads the settings from a file given by 'path'"""
        try:
            with open(path, 'r') as f:
                d = json.load(f)
                self.setSettings(d)
        except Exception as e:
            logger.exception(f"Could not load {self.type} settings file")
            ExceptionDialog(e)

    def setSettings(self, d: Dict[str, Any]) -> None:
        """Sets the influx settings."""
        if d.get('database', None) is not None:
            self.database_lineEdit.setText(d.get('database'))
        if d.get('host', None) is not None:
            self.host_lineEdit.setText(d.get('host'))
        if d.get('port', None) is not None:
            self.port_lineEdit.setText(d.get('port'))
        if d.get('measurement', None) is not None:
            self.measurement_lineEdit.setText(d.get('measurement'))
        if d.get('location', None) is not None:
            self.location_lineEdit.setText(d.get('location'))
        if d.get('username', None) is not None:
            self.username_lineEdit.setText(d.get('username'))
        if d.get('password', None) is not None:
            self.passwordLineEdit.setText(d.get('password'))
        if d.get('organization', None) is not None:
            self.organizationLineEdit.setText(d.get('organization'))

        for enum, checkbox in self._map.items():
            if d.get(enum.name, None) is not None:
                checkbox.setCheckState(d.get(enum.name))

class ThermalCycleDialog(QDialog, Ui_thermalCycleSettingDialog, BaseSaveableSettingsDialog):
    """A dialog for setting Influx db settings."""

    def __init__(self) -> None:
        QDialog.__init__(self)
        Ui_thermalCycleSettingDialog.__init__(self)
        BaseSaveableSettingsDialog.__init__(self, "ThermalCycling")
        self.setupUi(self)
        self.connectSignals()
        self._map = {tc.START_CYCLE_T_1 : self.startCycledoubleSpinBox_1,
                     tc.START_CYCLE_T_2 : self.startCycledoubleSpinBox_2,
                     tc.END_CYCLE_T_1 : self.endCycledoubleSpinBox_1,
                     tc.END_CYCLE_T_2 : self.endCycledoubleSpinBox_2,
                     tc.NUMBER_CYCLES_1: self.numCyclesdoubleSpinBox_1,
                     tc.NUMBER_CYCLES_2: self.numCyclesdoubleSpinBox_2,
                     tc.SOAK_TIME : self.soakTimedoubleSpinBox,
                     tc.RAMP: self.rampRatedoubleSpinBox}

    def connectSignals(self) -> None:
        self.save_pushButton.clicked.connect(
            lambda: self.saveLoadSettings('save'))
        self.load_pushButton.clicked.connect(
            lambda: self.saveLoadSettings('load'))
        self.startCycledoubleSpinBox_1.valueChanged.connect(
            self.endCycledoubleSpinBox_1.setMinimum)
        self.startCycledoubleSpinBox_2.valueChanged.connect(
            self.endCycledoubleSpinBox_2.setMinimum)

class InterlockSettingDialog(QDialog, Ui_InterlockSettingDialog,
                             BaseSaveableSettingsDialog):
    """A dialog for setting Interlock Settings"""

    def __init__(self, handler: 'PCTMHandler') -> None:
        QDialog.__init__(self)
        Ui_InterlockSettingDialog.__init__(self)
        BaseSaveableSettingsDialog.__init__(self, "SoftwareInterlock")
        self.pctmhandler = handler
        self.setupUi(self)
        self.connectSignals()
        self._map1 = {il.UPPER : self.UpperLimitdoubleSpinBox,
                      il.LOWER : self.LowerLimitdoubleSpinBox,
                      il.DIFF_DP_NTC : self.DewPointdoubleSpinBox,
                      il.OVERHEAT: self.overheatDoubleSpinBox,
                      il.LV_CHANNEL: self.pyVisaChanneldoubleSpinBox,
                      il.HV_PORT: self.HV_mode_port_spinBox,
                      il.LV_PORT: self.LV_mode_port_spinBox,
                      il.COLDPLATE: self.Coldplate_limit_doubleSpinBox,
                      }

        self._map2 = {il.LV_ENABLE: self.LVInterlockRadioButton,
                      il.HV_ENABLE: self.HVInterlockRadioButton,
                      il.LV_QUEUE: self.LV_open_queue_button,
                      il.HV_QUEUE: self.HV_open_queue_button,
                      }

        self.LVInterlockRadioButton.setChecked(True)
        self.HVInterlockRadioButton.setChecked(True)

        self.choosenPyvisa: str = ""
        self.choosenPyvisa2: str = ""

    def connectSignals(self) -> None:
        self.save_pushButton.clicked.connect(
            lambda: self.saveLoadSettings('save'))
        self.load_pushButton.clicked.connect(
            lambda: self.saveLoadSettings('load'))
        self.LowerLimitdoubleSpinBox.valueChanged.connect(
            self.UpperLimitdoubleSpinBox.setMinimum)
        self.InterlockUploadPushButton.clicked.connect(self.uploadInterlock)
        self.buttonBox.accepted.connect(self.setChoosenPyVisa)

    def uploadInterlock(self) -> None:
        d = self.getSettings()
        self.pctmhandler.setInterlockSettings(d)

    def getSettings(self) -> Dict[Any, Any]:
        """Returns a dict of the current settings in the dialog."""
        d : Dict[Any, Any] = {'LV_PYVISA': self.PyVisaComboBox.currentText(),
                              'HV_PYVISA': self.PyVisaComboBox2.currentText(),
                              'HV_MODE': self.HV_mode_comboBox.currentText(),
                              'LV_MODE': self.LV_mode_comboBox.currentText()
                              }


        for enum, spinbox in self._map1.items():
            d.update({enum.name: spinbox.value()})

        for enum, radioBox in self._map2.items():
            d.update({enum.name: radioBox.isChecked()})

        return d

    def _loadSettings(self, path: Path) -> None:
        """Loads the settings from a file given by 'path'"""
        try:
            with open(path, 'r') as f:
                d = json.load(f)
                self.setSettings(d)
        except Exception as e:
            logger.exception(f"Could not load {self.type} settings file")
            ExceptionDialog(e)

    def setSettings(self, d: Dict[Any, Optional[Any]]) -> None:
        """Sets the current settings in the dialog from a dict."""
        if d.get('LV_PYVISA', None) is not None:
            self.PyVisaComboBox.setCurrentText(d.get('PyVisa Address'))
        if d.get('HV_PYVISA', None) is not None:
            self.PyVisaComboBox2.setCurrentText(d.get('PyVisa Address'))

        for enum, spinbox in self._map1.items():
            if d.get(enum.name, None) is not None:
                spinbox.setValue(d.get(enum.name))

        for enum, radioBox in self._map2.items():
            if d.get(enum.name, None) is not None:
                radioBox.setChecked(d.get(enum.name))

    def getPyVisaResource(self) -> None:
        rm = pyvisa.ResourceManager()
        list_r = rm.list_resources()

        self.PyVisaComboBox.clear()
        self.PyVisaComboBox2.clear()

        for i in list_r:
            self.PyVisaComboBox.addItem(i, 0)
            self.PyVisaComboBox2.addItem(i, 0)

        if self.choosenPyvisa != "":
            self.PyVisaComboBox.setCurrentText(self.choosenPyvisa)
        if self.choosenPyvisa2 != "":
            self.PyVisaComboBox2.setCurrentText(self.choosenPyvisa2)

    def _updateFromPCTM(self, tup: Tuple[Any, float]) -> None:
        """Updates the settings directly from the PCTM. This method is called
        when the PCTM returns its settings."""
        enum, val = tup
        control = self._map1.get(enum)
        if control is None:
            logger.warning(f"The enum {enum} cannot be found in _map.")
            return
        control.setValue(val)

    def setChoosenPyVisa(self) -> None:
        self.choosenPyvisa = self.PyVisaComboBox.currentText()
        self.choosenPyvisa2 = self.PyVisaComboBox2.currentText()
