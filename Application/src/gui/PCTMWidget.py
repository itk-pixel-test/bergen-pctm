# -*- coding: utf-8 -*-
"""
Created on Sat Oct 31 14:57:39 2020

@author: magne.lauritzen

The PCTMWidget is the GUI equivalent of one PCTM. It contains all the graphs
and buttons that interact with one unique PCTM.

One PCTMWidget is placed in each Tab of the PCTM-GUI main window.
"""
from PyQt5 import QtCore  # type: ignore
from PyQt5.QtGui import QWidget, QCloseEvent  # type: ignore
from PyQt5.QtWidgets import QFileDialog as Qfd  # type: ignore
import pyqtgraph as pg  # type: ignore
from autologging import traced  # type: ignore
from typing import Dict, TYPE_CHECKING, Any, Tuple
from .gui_src.ui_PCTMWidget import Ui_PCTMPlotWidget
from .gui_src.popup import popup
from .dialogs import PidSettingsDialog, LoggingSettingsDialog, ExceptionDialog, PctmSettingsDialog, \
    AdvancedPctmSettingsDialog, PsuPidSettingsDialog, InfluxSettingsDialog, ThermalCycleDialog, InterlockSettingDialog, WarningDialog
from ..pg_mods import CustomPlotItem, AutoUpdatingPlot
from ..enums import SampleSource, MonitorFSettings, ThermalCycleStates, InterlockSettings, SystemSettings
from .. import loggers
from ..appconfig import AppConfig
from src.logfile import Logfile

logger = loggers.get_logger(__name__)
config = AppConfig()

if TYPE_CHECKING:
    from ..pctm import PCTM


@traced(logger, "showThermalCycleState", exclude=True)
class PCTMWidget(QWidget, Ui_PCTMPlotWidget):
    """
    The PCTMWidget is a QT widget that contains plots and buttons for displaying
    data from, and interacting with, one PCTM.

    Keyword arguments:
        pctm    : The PCTM that this PCTMWidget shall represent.
    """
    closing = QtCore.pyqtSignal()

    def __init__(self, pctm: 'PCTM') -> None:
        super(PCTMWidget, self).__init__()
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setupUi(self)
        self.logButton.setDisabled(True)
        self.settings = QtCore.QSettings()
        self.pctm = pctm
        self.pctm.signals.closing.connect(self._close)
        self._setupPlots()
        self.pidSettingsDialog = PidSettingsDialog()
        self.psuPidSettingsDialog = PsuPidSettingsDialog()
        self.loggingSettingsDialog = LoggingSettingsDialog()
        self.pctmSettingsDialog = PctmSettingsDialog()
        self.advancedPctmSettingsDialog = AdvancedPctmSettingsDialog(self.pctm.handler)
        self.influxSettingsDialog = InfluxSettingsDialog()
        self.thermalCycleSettingsDialog = ThermalCycleDialog()
        self.interlockSettingsDialog = InterlockSettingDialog(self.pctm.handler)
        self._connectSignals()
        self._getAllSettings()

        self.interlockActive = False


    def setLoggingState(self, enable: bool) -> None:
        """Pauses or resumes logging of data to file."""
        if enable:
            self.pctm.continueLogging()
        else:
            self.pctm.pauseLogging()

    def setInfluxState(self, enable: bool) -> None:
        """Pauses or resumes logging of data to Influx db."""
        if enable:
            try:
                self.pctm.influx.connectInflux(self.influxSettingsDialog.getSettings())
                self.pctm.influx.checkConnection()
                self.pctm.influxState = True
                logger.info(f"Success! Logging to Influx DB at {self.pctm.influx.connections['host']}:{self.pctm.influx.connections['port']}")
            except Exception as e:
                self.pctm.influxState = False
                self.influx_Button.setChecked(False)
                ExceptionDialog(e)
                logger.exception(e)
        else:
            logger.info(f"Stopped connection to Influx DB at {self.pctm.influx.connections['host']}:{self.pctm.influx.connections['port']}")
            self.pctm.influxState = False

    def setThermalCycleState(self, enable: bool) -> None:
        """Starts the automation of thermal cycling"""
        if enable:
            self.thermalCycleStartpushButton.setText("Thermal Cycling ENABLED ✔")
            self.pctm.thermalcycleState = True
            self.uploadPushButton.setDisabled(True)
            self.thermalCycleSettingsDialog.startCycledoubleSpinBox_1.setDisabled(True)
            self.thermalCycleSettingsDialog.endCycledoubleSpinBox_1.setDisabled(True)
            self.thermalCycleSettingsDialog.numCyclesdoubleSpinBox_1.setDisabled(True)
            self.thermalCycleSettingsDialog.startCycledoubleSpinBox_2.setDisabled(True)
            self.thermalCycleSettingsDialog.endCycledoubleSpinBox_2.setDisabled(True)
            self.thermalCycleSettingsDialog.numCyclesdoubleSpinBox_2.setDisabled(True)
            self.thermalCycleSettingsDialog.soakTimedoubleSpinBox.setDisabled(True)
            self.thermalCycleSettingsDialog.rampRatedoubleSpinBox.setDisabled(True)
            self.cyclingStatusLabel.setFrameStyle(1)
            self.pctm.thermalcycle.getThermalCycleSettings(self.thermalCycleSettingsDialog.getSettings())
            self.pctm.thermalcycle.restartCycle()
            #self.pctm.thermalcycle.startCycle()
        else:
            self.thermalCycleStartpushButton.setText("Thermal Cycling DISABLED ❌")
            self.thermalCycleStartpushButton.setChecked(False)
            self.uploadPushButton.setEnabled(True)
            self.thermalCycleSettingsDialog.startCycledoubleSpinBox_1.setEnabled(True)
            self.thermalCycleSettingsDialog.endCycledoubleSpinBox_1.setEnabled(True)
            self.thermalCycleSettingsDialog.numCyclesdoubleSpinBox_1.setEnabled(True)
            self.thermalCycleSettingsDialog.startCycledoubleSpinBox_2.setEnabled(True)
            self.thermalCycleSettingsDialog.endCycledoubleSpinBox_2.setEnabled(True)
            self.thermalCycleSettingsDialog.numCyclesdoubleSpinBox_2.setEnabled(True)
            self.thermalCycleSettingsDialog.soakTimedoubleSpinBox.setEnabled(True)
            self.thermalCycleSettingsDialog.rampRatedoubleSpinBox.setEnabled(True)
            self.pctm.thermalcycleState = False
            self.cyclingStatusLabel.setFrameStyle(0)
            self.cyclingStatusLabel.setText("")

    def setInterlockState(self, state: Any, reason: int = 0) -> None:
        if state == True:
            self.interlockActive = True
            self.interlockpushButton.setText("Interlock ENABLED ✔")
            # self.interlockSettingsDialog.UpperLimitdoubleSpinBox.setDisabled(True)
            # self.interlockSettingsDialog.LowerLimitdoubleSpinBox.setDisabled(True)
            # self.interlockSettingsDialog.DewPointdoubleSpinBox.setDisabled(True)
            self.pctm.interlock.getSettings(self.interlockSettingsDialog.getSettings())

            self.pctm.handler.enablePCTMInterlock()
            try:
                self.pctm.interlock.init()
                self.pctm.interlockState = True
            except Exception as e:
                ExceptionDialog(e)
                logger.exception(e)
                self.pctm.interlockState = False
                state = False

        if state == False:
            self.interlockActive = False
            self.interlockpushButton.setText("Interlock DISABLED ❌")
            # self.interlockSettingsDialog.UpperLimitdoubleSpinBox.setEnabled(True)
            # self.interlockSettingsDialog.LowerLimitdoubleSpinBox.setEnabled(True)
            # self.interlockSettingsDialog.DewPointdoubleSpinBox.setEnabled(True)
            self.interlockpushButton.setChecked(False)
            self.interlock_statusLabel.setFrameStyle(0)
            self.interlock_statusLabel.setText("")

            self.pctm.handler.disablePCTMInterlock()
            self.pctm.interlockState = False
            self.pctm.interlock.close()

        elif state == 3:
            #TODO what happens to interlockActive?
            self.interlockpushButton.setText("Interlock ACTIVATED ✔")
            self.interlock_statusLabel.setFrameStyle(1)
            if reason == 1:
                self.interlock_statusLabel.setText(
                    "Reason: Temperature has exceeded upper limit")
                #WarningDialog("Interlock has been activated! \nReason:Temperature has exceeded upper limit")
            elif reason == 2:
                self.interlock_statusLabel.setText(
                    "Reason: Temperature has exceeded lower limit")
                #WarningDialog("Interlock has been activated!\nReason: Temperature has exceeded lower limit")
            elif reason == 3:
                self.interlock_statusLabel.setText(
                    "Reason: Temperature has exceeded dewpoint limit")
                #WarningDialog("Interlock has been activated!\nReason: Temperature is too close to dewpoint")
            elif reason == 4:
                self.interlock_statusLabel.setText(
                    "Reason: Module is overheating")
                #WarningDialog("Interlock has been activated!\nReason: Module is overheating")
            elif reason == 5:
                self.interlock_statusLabel.setText(
                    "Reason: Lost connections to the sensors!")
            elif reason == 6:
                self.interlock_statusLabel.setText(
                    "Reason: Coldplate temperature is too high!")
                #WarningDialog("Interlock has been activated!\nReason: Coldplate temperature is too high")

            #self.pctm.interlockState = False

    def handle_interlock(self, tup: Tuple[Any, float]) -> None:
        enum, val = tup
        if enum == InterlockSettings.ACTIVATED_HIGH:
            self.pctm.interlock.ActivateInterlockLimit(1)
        elif enum == InterlockSettings.ACTIVATED_LOW:
            self.pctm.interlock.ActivateInterlockLimit(2)
        elif enum == InterlockSettings.ACTIVATED_DEW:
            self.pctm.interlock.ActivateInterlockLimit(3)
        elif enum == InterlockSettings.ACTIVATED_ABSENT:
            self.pctm.interlock.ActivateInterlockLimit(5)
        elif enum == InterlockSettings.ACTIVATED_COLDPLATE:
            self.pctm.interlock.ActivateInterlockLimit(6)
        elif enum == InterlockSettings.ENABLE:
             #if val == 1:
             #    self.interlockpushButton.setText("Interlock ENABLED ✔")
             #    self.interlockpushButton.setChecked(True)
             #else:
             #    self.interlockpushButton.setText("Interlock DISABLED ❌")
             #    self.interlockpushButton.setChecked(False)
            pass
        else:
            self.interlockSettingsDialog._updateFromPCTM(tup)

    def setPCTMSettings(self, tup: Tuple[Any, float]) -> None:
        enum, val = tup
        if val >= 0 and val < 16:
            new_val = hex(int(val)).upper()
            if enum == SystemSettings.ID:
                self.pctm.setName("PCTM ID: " + str(new_val)[2:])

    def setConstantModeState(self, checked: bool) -> None:
        if checked:
            self.constantModepushButton.setText("Constant Mode ENABLED ✔")
            self.pctm.handler.setConstantMode(True)
        else:
            self.constantModepushButton.setText("Constant Mode DISABLED ❌")
            self.pctm.handler.setConstantMode(False)
            self.constantModepushButton.setChecked(False)

    def logfileDialog(self, _: bool = None) -> None:
        """Opens a file explorer where the user can select the file they want
        to log data to."""
        p = self.settings.value("pctmwidget/logsavepath",
                                str(config.get_userdatadir()))
        f, _ = Qfd.getSaveFileName(self, caption="Select a file to log data to",
                                   directory=p, filter="HDF5 (*.hdf *.hdf5)")
        if f != "":
            try:
                self.pctm.stopLogging()
                self.pctm.startLogging(f)
                self.pctm.pauseLogging()
                self.logButton.setDisabled(False)
            except Exception as e:
                ExceptionDialog(e)
                logger.exception("Could not set the log file.")

    def openLoggingSettingsDialog(self, _: bool = None) -> None:
        """Opens the logging settings dialog where the user can set the
        logging frequency of all sources."""
        settings = self.pctm.logger.getLoggingFrequencies()
        self.loggingSettingsDialog.setSettings(
            settings)  # type: ignore #We provide a dict of Float to a function expecting a dict of Optional[Float]
        accepted = self.loggingSettingsDialog.exec()
        if accepted:
            s = self.loggingSettingsDialog.getSettings()
            # Cast to SampleSource enums and floats, with checks
            d: Dict[SampleSource, float] = {}
            for enum, val in s.items():
                if not isinstance(enum, SampleSource):
                    logger.warning(f"Received a non-SampleSource setting ({enum}) "
                                   "from the logging settings dialog.")
                    continue
                if not isinstance(val, float):
                    logger.warning(f"Received a non-float setting value ({val}) "
                                   "from the logging settings dialog.")
                    continue
                d[enum] = val
            self.pctm.logger.setLoggingFrequencies(d)

    def openPidSettingsDialog(self, _: bool = None) -> None:
        old_settings = self.pidSettingsDialog.getSettings()
        self.pctm.handler.getPidParameters()
        accepted = self.pidSettingsDialog.exec()
        if not accepted:
            self.pidSettingsDialog.setSettings(old_settings)
        return

    def openPsuPidSettingsDialog(self, _: bool = None) -> None:
        old_settings = self.psuPidSettingsDialog.getSettings()
        self.pctm.handler.getPidParameters()
        accepted = self.psuPidSettingsDialog.exec()
        if not accepted:
            self.psuPidSettingsDialog.setSettings(old_settings)
        return

    def uploadPidSettings(self, _: bool = None) -> None:
        """Uploads the settings from the PID Settings dialog to the PCTM."""
        settings = self.pidSettingsDialog.getSettings()
        self.pctm.handler.setTecPidParameters(settings['pid_parameters'])
        self.pctm.handler.setTECPidFrequency(settings['tec_sample_freq']['tec_freq'])
        self.pctm.handler.controlTempSetpoint(self.targetTempSpinBox.value(), self.rampdoubleSpinBox.value())
        self.pctm.handler.setCILimits(settings['limits'])
        self.pctm.handler.setTecControl(settings['control_mode'])
        settings = self.psuPidSettingsDialog.getSettings()
        self.pctm.handler.setPsuPidParameters(settings['pid_parameters'])
        self.pctm.handler.setPSUPidFrequency(settings['psu_sample_freq']['psu_freq'])
        self.pctm.handler.setPSUImax(settings['psu_details']['imax'])
        self.pctm.handler.setPsuControl(settings['control_mode'])
        self.pctm.handler.setPsuCommMode(settings['psu_details']['psu_comm'])

    def openPctmSettingsDialog(self, _: bool = None) -> None:
        self.pctm.handler.getSampleFrequencies()
        old_settings = self.pctmSettingsDialog.getSettings()
        accepted = self.pctmSettingsDialog.exec()
        if not accepted:
            self.pctmSettingsDialog.setSettings(old_settings)
        return

    def uploadPctmSettings(self, _: bool = None) -> None:
        """Uploads the settings from the PCTM Settings dialog to the PCTM."""
        pctm_settings = self.pctmSettingsDialog.getSettings()
        s: Dict[MonitorFSettings, float] = {}
        for key, val in pctm_settings.items():
            assert isinstance(key, MonitorFSettings)
            assert isinstance(val, float)
            s[key] = val
        self.pctm.handler.setSampleFrequency(s)

    def openAdvancedPctmSettingsDialog(self, _: bool = None) -> None:
        self.advancedPctmSettingsDialog.exec()

    def openInfluxSettingsDialog(self, _: bool = None) -> None:
        self.influxSettingsDialog.exec()

    def openThermalCycleSettingsDialog(self, _: bool = None) -> None:
        self.thermalCycleSettingsDialog.exec()

    def openInterlockSettingsDialog(self, _: bool = None) -> None:
        self.interlockSettingsDialog.getPyVisaResource()
        self.pctm.handler.getInterlockSettings()
        self.interlockSettingsDialog.exec()

    def requestPctmClose(self) -> None:
        """Calling this method will close the PCTM object that this widget
        represents. This in turn calls self._close() which closes the widget."""
        self.pctm.close()

    def closeEvent(self, event: QtCore.QEvent) -> None:
        """closeEvents are ignored. Instead we call the custom closing method,
        requestPctmClose(), which ensures the PCTM is closed first."""
        self.requestPctmClose()
        event.ignore()

    def _setPIDstate_pushButton_toggled(self, checked: bool) -> None:
        if not self.interlockActive:
            logger.exception("No interlock detected!")
            popup("No interlock detected!")
            return

        if checked:
            self.pctm.handler.enablePID()
            self.setPIDstate_pushButton.setText("PID ENABLED ✔")
        else:
            self.pctm.handler.disablePID()
            self.setPIDstate_pushButton.setText("PID DISABLED ❌")
            self.setPIDstate_pushButton.setChecked(False)
            self.setConstantModeState(False)

    def _setupPlots(self) -> None:
        #Use OpenGL for performance
        #pg.setConfigOptions(useOpenGL = True)

        # Set up graph area 1
        # Top plot
        plitem = CustomPlotItem()
        self._replacePlotItem(self.topPlotWidget, plitem)

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.DP_1, name="DP 1")
        pl.setPen('r')
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='Degrees C')

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.DP_2, name="DP 2")
        pl.setPen('r', style=QtCore.Qt.DashLine)
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='Degrees C')

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.NTC_1, name="Module")
        pl.setPen('w')
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='Degrees C')

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.NTC_2, name="NTC 2")
        pl.setPen('w', style=QtCore.Qt.DashLine)
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='Degrees C')

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.NTC_3, name="NTC 3")
        pl.setPen('w', style=QtCore.Qt.DotLine)
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='Degrees C')

        # Bottom plot
        plitem = CustomPlotItem()
        self._replacePlotItem(self.bottomPlotWidget, plitem)

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.RH_1, name="RH 1")
        pl.setPen('g')
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='Percent')

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.GAS_T_1, name="Gas T 1")
        pl.setPen('r')
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='Degrees C')

        # Set up graph area 2
        # Top plot
        plitem = CustomPlotItem()
        self._replacePlotItem(self.topTecPlotWidget, plitem)

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.TEC_PID_SETPOINT, name="TEC PID setpoint")
        pl.setPen(pg.mkPen([150,150,150], width=3))
        #pl.setPen([150,150,150])
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='Degrees C')

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.NTC_1, name="Module")
        pl.setPen('w')
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='Degrees C')

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.CP_T, name="Coldplate")
        pl.setPen('y')
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='Degrees C')

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.VC_T, name="Vacuum chuck")
        pl.setPen('g')
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='Degrees C')

        # Bottom plot
        plitem = CustomPlotItem()
        self._replacePlotItem(self.bottomTecPlotWidget, plitem)

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.PSU_CONTROL, name="PSU control")
        #pl.setPen(pg.mkPen('w', width=1))
        pl.setPen('w')
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='V / I')

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.PSU_PID_SETPOINT, name="PSU PID setpoint")
        pl.setPen(pg.mkPen([0,150,0], width=3))
        #pl.setPen([0,150,0])
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='V / I')

        pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.TEC_I, name="TEC current")
        pl.setPen('g')
        pl.setDownsampling(auto=True, method='subsample')
        pl.setClipToView(True)
        plitem.addPlot(pl, axis='V / I')

        # This block can be uncommented when the PCTM has a way of measuring PCTM voltage.
        # pl = AutoUpdatingPlot(self.pctm.handler, SampleSource.TEC_V, name="TEC voltage")
        # pl.setPen('y')
        # plitem.addPlot(pl, axis='V / I')

    def _close(self) -> None:
        """Closes the widget. Emits the 'closing' signal."""
        logger.info(f"Closing PCTMWidget {self.pctm.name}")
        self.closing.emit()
        super().closeEvent(QCloseEvent())

    def _connectSignals(self) -> None:
        self.pidSettingsPushButton.clicked.connect(self.openPidSettingsDialog)
        self.psuPidSettingsPushButton.clicked.connect(self.openPsuPidSettingsDialog)
        self.loggingSettingsPushButton.clicked.connect(self.openLoggingSettingsDialog)
        self.uploadPushButton.clicked.connect(self.uploadPidSettings)
        self.setLogfileButton.clicked.connect(self.logfileDialog)
        self.logButton.toggled.connect(self.setLoggingState)
        self.pctmSettings_pushButton.clicked.connect(self.openPctmSettingsDialog)
        self.advancedPctmSettings_pushButton.clicked.connect(self.openAdvancedPctmSettingsDialog)
        self.pctmSettingsUpload_pushButton.clicked.connect(self.uploadPctmSettings)
        self.setPIDstate_pushButton.toggled.connect(self._setPIDstate_pushButton_toggled)
        self.influxSettings_pushButton.clicked.connect(self.openInfluxSettingsDialog)
        self.influx_Button.toggled.connect(self.setInfluxState)
        self.thermalCyclepushButton.clicked.connect(self.openThermalCycleSettingsDialog)
        self.thermalCycleStartpushButton.toggled.connect(self.setThermalCycleState)
        self.interlockSettingspushButton.clicked.connect(self.openInterlockSettingsDialog)
        self.interlockpushButton.toggled.connect(self.setInterlockState)
        self.constantModepushButton.toggled.connect(self.setConstantModeState)
        # Connect settings dialogs with settings data rx from PCTM
        self.pctm.handler.rxdatahandler.freq_rx.connect(self.pctmSettingsDialog._updateFromPCTM)
        self.pctm.handler.rxdatahandler.pid_rx.connect(self.pidSettingsDialog._updateFromPCTM)
        self.pctm.handler.rxdatahandler.pid_rx.connect(self.psuPidSettingsDialog._updateFromPCTM)
        self.pctm.handler.rxdatahandler.interlock_rx.connect(self.handle_interlock)
        self.pctm.handler.rxdatahandler.pctm_rx.connect(self.setPCTMSettings)
        # Connect signal from Misc Classes
        self.pctm.thermalcycle.signals.cycling_done.connect(self.setThermalCycleState)
        self.pctm.interlock.signals.pid_button.connect(self._setPIDstate_pushButton_toggled)
        self.pctm.interlock.signals.interlock.connect(self.setInterlockState)
        self.pctm.thermalcycle.signals.stateChanged.connect(self.showThermalCycleState)

    def _getAllSettings(self) -> None:
        self.pctm.handler.getSampleFrequencies()
        self.pctm.handler.getPidParameters()
        self.pctm.handler.getId()

    def _replacePlotItem(self, pw: pg.PlotWidget, pl: pg.PlotItem) -> None:
        """Replaces the default PlotItem in the PlotWidget 'pw' with a new
        PlotItem."""
        pw.plotItem.close()
        pw.plotItem = pl
        pw.setCentralItem(pl)
        ## Explicitly wrap methods from plotItem
        for m in ['addItem', 'removeItem', 'autoRange', 'clear', 'setAxisItems', 'setXRange',
                  'setYRange', 'setRange', 'setAspectLocked', 'setMouseEnabled',
                  'setXLink', 'setYLink', 'enableAutoRange', 'disableAutoRange',
                  'setLimits', 'register', 'unregister', 'viewRect']:
            setattr(pw, m, getattr(pw.plotItem, m))
        pw.plotItem.sigRangeChanged.connect(pw.viewRangeChanged)

    def showThermalCycleState(self, state: ThermalCycleStates, target: float, ramp_rate: float, time_left: float) -> None:
        if state == ThermalCycleStates.SOAKING:
            sec = int(time_left) % 60
            min = int(time_left / 60)
            self.cyclingStatusLabel.setText("Cycling Status: " + state.name + " " + "{:0>2}".format(min) + ":" + "{:0>2}".format(sec))
        elif state == ThermalCycleStates.ENDCYCLE:
            self.cyclingStatusLabel.setFrameStyle(0)
            self.cyclingStatusLabel.setText("")
        else:
            self.cyclingStatusLabel.setText("Cycling Status: " + state.name)
            self.targetTempSpinBox.setValue(target)
            self.rampdoubleSpinBox.setValue(ramp_rate)
