# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 17:47:29 2020

@author: Magne Lauritzen

This file defines the class for the main window of the PCTM-GUI application.
"""
from PyQt5 import QtCore  # type: ignore
from PyQt5.QtGui import QMainWindow  # type: ignore
from typing import TYPE_CHECKING
from autologging import traced  # type: ignore
from .. import loggers
from .PCTMWidget import PCTMWidget
from .pyqtcustom import EditableTabBar
from .gui_src.ui_mainwindow import Ui_PCTMLoggerGUI

logger = loggers.get_logger(__name__)

if TYPE_CHECKING:
    from ..pctm import PCTM


@traced(logger, "__init__", exclude=True)
class PCTMGUI(QMainWindow, Ui_PCTMLoggerGUI):
    """
    PCTMGUI is the main window of the PCTM-GUI application. 
    signals:
        closeSignal     : Emitted when the window closes.
    """
    closeSignal = QtCore.pyqtSignal()

    def __init__(self) -> None:
        super(PCTMGUI, self).__init__()
        self.setupUi(self)
        self._loadSettings()
        self.tabWidget.setTabBar(EditableTabBar(self))
        self.tabWidget.setTabsClosable(True)
        self.tabWidget.tabCloseRequested.connect(self.closeTab)

    def indexOf(self, pctmw: PCTMWidget) -> int:
        """An explicit wrapping of the tabWidget indexOf()"""
        idx: int = self.tabWidget.indexOf(pctmw)
        if idx == -1:
            raise Exception(f"The PCTM Widget by name \"{pctmw.pctm.name}\" "
                            "was not found in the main window TabWidget.")
        return idx

    def closeTab(self, idx: int) -> None:
        """We tell the PCTMWidget we wish to close the tab that holds it. The 
        PCTMWidget tells the PCTM object it represents to close down. When it 
        has closed down, the widget closes itself, which finally tells the 
        TabWidget to close the tab."""
        self.tabWidget.widget(idx).requestPctmClose()

    def closeEvent(self, event: QtCore.QEvent) -> None:
        # Write window size and position to config file
        self.settings.setValue("size", self.size())
        self.settings.setValue("pos", self.pos())
        self.closeSignal.emit()  # Is caught by the GUI owner (PctmApp)
        event.accept()

    def _addPctm(self, pctm: 'PCTM') -> None:
        """
        Opens the GUI elements corresponding to the new PCTM object pctm and
        adds it to a new tab.
        Returns the PCTMWidget."""
        pctmw = PCTMWidget(pctm)
        pctmw.closing.connect(lambda: self.tabWidget.removeTab(self.indexOf(pctmw)))
        pctmw.pctm.signals.renamed.connect(lambda: self._pctmRenamed(pctmw))
        self.tabWidget.tabBar().labelChanged.connect(self._tabRenamed)
        self.tabWidget.addTab(pctmw, pctmw.pctm.name)

    def _tabRenamed(self, tab_idx: int, name: str) -> None:
        """Called when a tab is renamed. Updates the name of the PCTM to match."""
        self.tabWidget.widget(tab_idx).pctm.setName(name)

    def _pctmRenamed(self, pctmw: PCTMWidget) -> None:
        """Called when a PCTM is renamed. Updates its tab label to match."""
        name = pctmw.pctm.name
        idx = self.indexOf(pctmw)
        self.tabWidget.setTabText(idx, name)

    def _loadSettings(self) -> None:
        """Loads QT settings for the main window such as position and size."""
        # Get settings object for PCTM-GUI
        self.settings = QtCore.QSettings()
        # Set size and position. Use defaults if not yet set.
        self.resize(self.settings.value("size", self.size()))
        self.move(self.settings.value("pos", self.pos()))
