from PyQt5.QtWidgets import QMessageBox, QApplication

def popup(message):
    msgBox = QMessageBox()
    #msgBox.setIcon(QMessageBox.Information)
    msgBox.setText(message)
    msgBox.setStandardButtons(QMessageBox.Ok)
    returnValue = msgBox.exec()

if __name__ == "__main__":
    app = QApplication([])
    popup("test")
