# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_pctmconnector.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_PCTMConnectionDialog(object):
    def setupUi(self, PCTMConnectionDialog):
        PCTMConnectionDialog.setObjectName("PCTMConnectionDialog")
        PCTMConnectionDialog.resize(338, 176)
        self.verticalLayout = QtWidgets.QVBoxLayout(PCTMConnectionDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.comboBox = QtWidgets.QComboBox(PCTMConnectionDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox.sizePolicy().hasHeightForWidth())
        self.comboBox.setSizePolicy(sizePolicy)
        self.comboBox.setObjectName("comboBox")
        self.horizontalLayout.addWidget(self.comboBox)
        self.pushButton = QtWidgets.QPushButton(PCTMConnectionDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout.addWidget(self.pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.line = QtWidgets.QFrame(PCTMConnectionDialog)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.label = QtWidgets.QLabel(PCTMConnectionDialog)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.textBrowser = QtWidgets.QTextBrowser(PCTMConnectionDialog)
        self.textBrowser.setObjectName("textBrowser")
        self.verticalLayout.addWidget(self.textBrowser)

        self.retranslateUi(PCTMConnectionDialog)
        QtCore.QMetaObject.connectSlotsByName(PCTMConnectionDialog)

    def retranslateUi(self, PCTMConnectionDialog):
        _translate = QtCore.QCoreApplication.translate
        PCTMConnectionDialog.setWindowTitle(_translate("PCTMConnectionDialog", "Connect to a PCTM"))
        self.pushButton.setText(_translate("PCTMConnectionDialog", "Connect"))
        self.label.setText(_translate("PCTMConnectionDialog", "Debugging output:"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    PCTMConnectionDialog = QtWidgets.QDialog()
    ui = Ui_PCTMConnectionDialog()
    ui.setupUi(PCTMConnectionDialog)
    PCTMConnectionDialog.show()
    sys.exit(app.exec_())

