# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_PCTMLoggerGUI(object):
    def setupUi(self, PCTMLoggerGUI):
        PCTMLoggerGUI.setObjectName("PCTMLoggerGUI")
        PCTMLoggerGUI.resize(1233, 905)
        self.centralwidget = QtWidgets.QWidget(PCTMLoggerGUI)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setTabsClosable(True)
        self.tabWidget.setMovable(True)
        self.tabWidget.setObjectName("tabWidget")
        self.horizontalLayout.addWidget(self.tabWidget)
        PCTMLoggerGUI.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(PCTMLoggerGUI)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1233, 31))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        PCTMLoggerGUI.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(PCTMLoggerGUI)
        self.statusbar.setObjectName("statusbar")
        PCTMLoggerGUI.setStatusBar(self.statusbar)
        self.actionPctmConnect = QtWidgets.QAction(PCTMLoggerGUI)
        self.actionPctmConnect.setObjectName("actionPctmConnect")
        self.menuFile.addAction(self.actionPctmConnect)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(PCTMLoggerGUI)
        self.tabWidget.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(PCTMLoggerGUI)

    def retranslateUi(self, PCTMLoggerGUI):
        _translate = QtCore.QCoreApplication.translate
        PCTMLoggerGUI.setWindowTitle(_translate("PCTMLoggerGUI", "ITKPix PCTM Logger"))
        self.menuFile.setTitle(_translate("PCTMLoggerGUI", "File"))
        self.actionPctmConnect.setText(_translate("PCTMLoggerGUI", "Connect"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    PCTMLoggerGUI = QtWidgets.QMainWindow()
    ui = Ui_PCTMLoggerGUI()
    ui.setupUi(PCTMLoggerGUI)
    PCTMLoggerGUI.show()
    sys.exit(app.exec_())

