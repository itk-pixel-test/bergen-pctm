# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_advancedPCTMsettings.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_AdvancedPCTMSettingsDialog(object):
    def setupUi(self, AdvancedPCTMSettingsDialog):
        AdvancedPCTMSettingsDialog.setObjectName("AdvancedPCTMSettingsDialog")
        AdvancedPCTMSettingsDialog.resize(482, 249)
        self.verticalLayout = QtWidgets.QVBoxLayout(AdvancedPCTMSettingsDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(AdvancedPCTMSettingsDialog)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_2 = QtWidgets.QLabel(AdvancedPCTMSettingsDialog)
        self.label_2.setWordWrap(True)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.id_spinBox = QtWidgets.QSpinBox(AdvancedPCTMSettingsDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.id_spinBox.sizePolicy().hasHeightForWidth())
        self.id_spinBox.setSizePolicy(sizePolicy)
        self.id_spinBox.setMaximum(15)
        self.id_spinBox.setDisplayIntegerBase(16)
        self.id_spinBox.setObjectName("id_spinBox")
        self.horizontalLayout.addWidget(self.id_spinBox)
        self.id_upload_pushButton = QtWidgets.QPushButton(AdvancedPCTMSettingsDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.id_upload_pushButton.sizePolicy().hasHeightForWidth())
        self.id_upload_pushButton.setSizePolicy(sizePolicy)
        self.id_upload_pushButton.setObjectName("id_upload_pushButton")
        self.horizontalLayout.addWidget(self.id_upload_pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.label_4 = QtWidgets.QLabel(AdvancedPCTMSettingsDialog)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.verticalLayout.addWidget(self.label_4)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setContentsMargins(-1, -1, -1, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_5 = QtWidgets.QLabel(AdvancedPCTMSettingsDialog)
        self.label_5.setWordWrap(True)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_2.addWidget(self.label_5)
        self.calibrate_pushButton = QtWidgets.QPushButton(AdvancedPCTMSettingsDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.calibrate_pushButton.sizePolicy().hasHeightForWidth())
        self.calibrate_pushButton.setSizePolicy(sizePolicy)
        self.calibrate_pushButton.setObjectName("calibrate_pushButton")
        self.horizontalLayout_2.addWidget(self.calibrate_pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.label_6 = QtWidgets.QLabel(AdvancedPCTMSettingsDialog)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.verticalLayout.addWidget(self.label_6)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setContentsMargins(-1, -1, -1, 10)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_7 = QtWidgets.QLabel(AdvancedPCTMSettingsDialog)
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_3.addWidget(self.label_7)
        self.reset_pushButton = QtWidgets.QPushButton(AdvancedPCTMSettingsDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.reset_pushButton.sizePolicy().hasHeightForWidth())
        self.reset_pushButton.setSizePolicy(sizePolicy)
        self.reset_pushButton.setObjectName("reset_pushButton")
        self.horizontalLayout_3.addWidget(self.reset_pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)

        self.retranslateUi(AdvancedPCTMSettingsDialog)
        QtCore.QMetaObject.connectSlotsByName(AdvancedPCTMSettingsDialog)

    def retranslateUi(self, AdvancedPCTMSettingsDialog):
        _translate = QtCore.QCoreApplication.translate
        AdvancedPCTMSettingsDialog.setWindowTitle(_translate("AdvancedPCTMSettingsDialog", "Advanced PCTM settings"))
        self.label.setText(_translate("AdvancedPCTMSettingsDialog", "<html><head/><body><p><span style=\" font-weight:600;\">PCTM ID</span></p></body></html>"))
        self.label_2.setText(_translate("AdvancedPCTMSettingsDialog", "<html><head/><body><p>Change the identity of the PCTM. You can select a value betwen 0 and F. The ID is nonvolatile and is displayed on the PCTM\'s segmented display.</p></body></html>"))
        self.id_upload_pushButton.setText(_translate("AdvancedPCTMSettingsDialog", "Upload"))
        self.label_4.setText(_translate("AdvancedPCTMSettingsDialog", "CALIBRATION"))
        self.label_5.setText(_translate("AdvancedPCTMSettingsDialog", "<html><head/><body><p>Calibrate the Peltier current sensor. It will be sampled continuously for one second, and the average value set as 0 A. Shut off or disconnect the Peltier PSU before calibrating. The calibration value is nonvolatile.</p></body></html>"))
        self.calibrate_pushButton.setText(_translate("AdvancedPCTMSettingsDialog", "Calibrate"))
        self.label_6.setText(_translate("AdvancedPCTMSettingsDialog", "FACTORY RESET"))
        self.label_7.setText(_translate("AdvancedPCTMSettingsDialog", "Deletes all nonvolatile PCTM settings (ID, calibrations, PSU and Peltier info, etc)"))
        self.reset_pushButton.setText(_translate("AdvancedPCTMSettingsDialog", "Reset"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AdvancedPCTMSettingsDialog = QtWidgets.QDialog()
    ui = Ui_AdvancedPCTMSettingsDialog()
    ui.setupUi(AdvancedPCTMSettingsDialog)
    AdvancedPCTMSettingsDialog.show()
    sys.exit(app.exec_())

