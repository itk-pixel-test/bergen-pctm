# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui/gui_src/ui_pctm_settings.ui'
#
# Created by: PyQt5 UI code generator 5.15.7
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_pctmSettingsDialog(object):
    def setupUi(self, pctmSettingsDialog):
        pctmSettingsDialog.setObjectName("pctmSettingsDialog")
        pctmSettingsDialog.resize(434, 601)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(pctmSettingsDialog)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label_9 = QtWidgets.QLabel(pctmSettingsDialog)
        self.label_9.setWordWrap(True)
        self.label_9.setObjectName("label_9")
        self.verticalLayout_5.addWidget(self.label_9)
        self.line_7 = QtWidgets.QFrame(pctmSettingsDialog)
        self.line_7.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_7.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_7.setObjectName("line_7")
        self.verticalLayout_5.addWidget(self.line_7)
        self.label = QtWidgets.QLabel(pctmSettingsDialog)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout_5.addWidget(self.label)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setContentsMargins(-1, 0, -1, 0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.formLayout_3 = QtWidgets.QFormLayout()
        self.formLayout_3.setObjectName("formLayout_3")
        self.nTC1Label = QtWidgets.QLabel(pctmSettingsDialog)
        self.nTC1Label.setObjectName("nTC1Label")
        self.formLayout_3.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.nTC1Label)
        self.nTC3Label = QtWidgets.QLabel(pctmSettingsDialog)
        self.nTC3Label.setObjectName("nTC3Label")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.nTC3Label)
        self.nTC2Label = QtWidgets.QLabel(pctmSettingsDialog)
        self.nTC2Label.setObjectName("nTC2Label")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.nTC2Label)
        self.NTC1DoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.NTC1DoubleSpinBox.setMaximum(20.0)
        self.NTC1DoubleSpinBox.setObjectName("NTC1DoubleSpinBox")
        self.formLayout_3.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.NTC1DoubleSpinBox)
        self.NTC2DoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.NTC2DoubleSpinBox.setMaximum(20.0)
        self.NTC2DoubleSpinBox.setObjectName("NTC2DoubleSpinBox")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.NTC2DoubleSpinBox)
        self.NTC3DoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.NTC3DoubleSpinBox.setMaximum(20.0)
        self.NTC3DoubleSpinBox.setObjectName("NTC3DoubleSpinBox")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.NTC3DoubleSpinBox)
        self.horizontalLayout_5.addLayout(self.formLayout_3)
        self.line_4 = QtWidgets.QFrame(pctmSettingsDialog)
        self.line_4.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.horizontalLayout_5.addWidget(self.line_4)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.humidity1Label_3 = QtWidgets.QLabel(pctmSettingsDialog)
        self.humidity1Label_3.setObjectName("humidity1Label_3")
        self.horizontalLayout_3.addWidget(self.humidity1Label_3)
        self.setAllNTCdoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.setAllNTCdoubleSpinBox.setMaximum(20.0)
        self.setAllNTCdoubleSpinBox.setObjectName("setAllNTCdoubleSpinBox")
        self.horizontalLayout_3.addWidget(self.setAllNTCdoubleSpinBox)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem1)
        self.horizontalLayout_5.addLayout(self.verticalLayout_2)
        self.verticalLayout_4.addLayout(self.horizontalLayout_5)
        self.line_2 = QtWidgets.QFrame(pctmSettingsDialog)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout_4.addWidget(self.line_2)
        self.label_5 = QtWidgets.QLabel(pctmSettingsDialog)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_5.setFont(font)
        self.label_5.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_5.setTextFormat(QtCore.Qt.AutoText)
        self.label_5.setScaledContents(False)
        self.label_5.setObjectName("label_5")
        self.verticalLayout_4.addWidget(self.label_5)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.formLayout_2 = QtWidgets.QFormLayout()
        self.formLayout_2.setObjectName("formLayout_2")
        self.humidity2Label = QtWidgets.QLabel(pctmSettingsDialog)
        self.humidity2Label.setObjectName("humidity2Label")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.humidity2Label)
        self.humidity1Label = QtWidgets.QLabel(pctmSettingsDialog)
        self.humidity1Label.setObjectName("humidity1Label")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.humidity1Label)
        self.humidity1DoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.humidity1DoubleSpinBox.setMaximum(20.0)
        self.humidity1DoubleSpinBox.setObjectName("humidity1DoubleSpinBox")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.humidity1DoubleSpinBox)
        self.humidity2DoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.humidity2DoubleSpinBox.setMaximum(20.0)
        self.humidity2DoubleSpinBox.setObjectName("humidity2DoubleSpinBox")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.humidity2DoubleSpinBox)
        self.horizontalLayout_2.addLayout(self.formLayout_2)
        self.line_5 = QtWidgets.QFrame(pctmSettingsDialog)
        self.line_5.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_5.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_5.setObjectName("line_5")
        self.horizontalLayout_2.addWidget(self.line_5)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.humidity1Label_2 = QtWidgets.QLabel(pctmSettingsDialog)
        self.humidity1Label_2.setObjectName("humidity1Label_2")
        self.horizontalLayout.addWidget(self.humidity1Label_2)
        self.setAllHumdoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.setAllHumdoubleSpinBox.setMaximum(20.0)
        self.setAllHumdoubleSpinBox.setObjectName("setAllHumdoubleSpinBox")
        self.horizontalLayout.addWidget(self.setAllHumdoubleSpinBox)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem3 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem3)
        self.horizontalLayout_2.addLayout(self.verticalLayout)
        self.verticalLayout_4.addLayout(self.horizontalLayout_2)
        self.line = QtWidgets.QFrame(pctmSettingsDialog)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout_4.addWidget(self.line)
        self.label_6 = QtWidgets.QLabel(pctmSettingsDialog)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_6.setFont(font)
        self.label_6.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_6.setTextFormat(QtCore.Qt.AutoText)
        self.label_6.setScaledContents(False)
        self.label_6.setObjectName("label_6")
        self.verticalLayout_4.addWidget(self.label_6)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.formLayout_4 = QtWidgets.QFormLayout()
        self.formLayout_4.setObjectName("formLayout_4")
        self.coldplateLabel = QtWidgets.QLabel(pctmSettingsDialog)
        self.coldplateLabel.setObjectName("coldplateLabel")
        self.formLayout_4.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.coldplateLabel)
        self.coldplateDoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.coldplateDoubleSpinBox.setMaximum(20.0)
        self.coldplateDoubleSpinBox.setObjectName("coldplateDoubleSpinBox")
        self.formLayout_4.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.coldplateDoubleSpinBox)
        self.peltierILabel = QtWidgets.QLabel(pctmSettingsDialog)
        self.peltierILabel.setObjectName("peltierILabel")
        self.formLayout_4.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.peltierILabel)
        self.peltierDoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.peltierDoubleSpinBox.setMaximum(20.0)
        self.peltierDoubleSpinBox.setObjectName("peltierDoubleSpinBox")
        self.formLayout_4.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.peltierDoubleSpinBox)
        self.pIDLabel = QtWidgets.QLabel(pctmSettingsDialog)
        self.pIDLabel.setObjectName("pIDLabel")
        self.formLayout_4.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.pIDLabel)
        self.pIDDoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.pIDDoubleSpinBox.setMaximum(20.0)
        self.pIDDoubleSpinBox.setObjectName("pIDDoubleSpinBox")
        self.formLayout_4.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.pIDDoubleSpinBox)
        self.horizontalLayout_6.addLayout(self.formLayout_4)
        self.line_6 = QtWidgets.QFrame(pctmSettingsDialog)
        self.line_6.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_6.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_6.setObjectName("line_6")
        self.horizontalLayout_6.addWidget(self.line_6)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem4)
        self.humidity1Label_4 = QtWidgets.QLabel(pctmSettingsDialog)
        self.humidity1Label_4.setObjectName("humidity1Label_4")
        self.horizontalLayout_4.addWidget(self.humidity1Label_4)
        self.setAllPeltierdoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.setAllPeltierdoubleSpinBox.setMaximum(20.0)
        self.setAllPeltierdoubleSpinBox.setObjectName("setAllPeltierdoubleSpinBox")
        self.horizontalLayout_4.addWidget(self.setAllPeltierdoubleSpinBox)
        self.verticalLayout_3.addLayout(self.horizontalLayout_4)
        spacerItem5 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem5)
        self.horizontalLayout_6.addLayout(self.verticalLayout_3)
        self.verticalLayout_4.addLayout(self.horizontalLayout_6)
        self.line_3 = QtWidgets.QFrame(pctmSettingsDialog)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.verticalLayout_4.addWidget(self.line_3)
        self.label_10 = QtWidgets.QLabel(pctmSettingsDialog)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_10.setFont(font)
        self.label_10.setObjectName("label_10")
        self.verticalLayout_4.addWidget(self.label_10)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setContentsMargins(-1, -1, -1, 20)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.formLayout_5 = QtWidgets.QFormLayout()
        self.formLayout_5.setObjectName("formLayout_5")
        self.gasFlowLabel = QtWidgets.QLabel(pctmSettingsDialog)
        self.gasFlowLabel.setObjectName("gasFlowLabel")
        self.formLayout_5.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.gasFlowLabel)
        self.vacuumChuckDoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.vacuumChuckDoubleSpinBox.setMaximum(20.0)
        self.vacuumChuckDoubleSpinBox.setObjectName("vacuumChuckDoubleSpinBox")
        self.formLayout_5.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.vacuumChuckDoubleSpinBox)
        self.vacuumPressureDoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.vacuumPressureDoubleSpinBox.setMaximum(20.0)
        self.vacuumPressureDoubleSpinBox.setObjectName("vacuumPressureDoubleSpinBox")
        self.formLayout_5.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.vacuumPressureDoubleSpinBox)
        self.vacuumPressureLabel = QtWidgets.QLabel(pctmSettingsDialog)
        self.vacuumPressureLabel.setObjectName("vacuumPressureLabel")
        self.formLayout_5.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.vacuumPressureLabel)
        self.vacuumChuckLabel = QtWidgets.QLabel(pctmSettingsDialog)
        self.vacuumChuckLabel.setObjectName("vacuumChuckLabel")
        self.formLayout_5.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.vacuumChuckLabel)
        self.gasFlowDoubleSpinBox = QtWidgets.QDoubleSpinBox(pctmSettingsDialog)
        self.gasFlowDoubleSpinBox.setMaximum(20.0)
        self.gasFlowDoubleSpinBox.setObjectName("gasFlowDoubleSpinBox")
        self.formLayout_5.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.gasFlowDoubleSpinBox)
        self.horizontalLayout_7.addLayout(self.formLayout_5)
        self.line_8 = QtWidgets.QFrame(pctmSettingsDialog)
        self.line_8.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_8.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_8.setObjectName("line_8")
        self.horizontalLayout_7.addWidget(self.line_8)
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        spacerItem6 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem6)
        self.horizontalLayout_7.addLayout(self.horizontalLayout_8)
        self.verticalLayout_4.addLayout(self.horizontalLayout_7)
        self.verticalLayout_5.addLayout(self.verticalLayout_4)
        spacerItem7 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_5.addItem(spacerItem7)
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_9.setContentsMargins(-1, -1, -1, 0)
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.save_pushButton = QtWidgets.QPushButton(pctmSettingsDialog)
        self.save_pushButton.setObjectName("save_pushButton")
        self.horizontalLayout_9.addWidget(self.save_pushButton)
        self.load_pushButton = QtWidgets.QPushButton(pctmSettingsDialog)
        self.load_pushButton.setObjectName("load_pushButton")
        self.horizontalLayout_9.addWidget(self.load_pushButton)
        self.buttonBox = QtWidgets.QDialogButtonBox(pctmSettingsDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.horizontalLayout_9.addWidget(self.buttonBox)
        self.verticalLayout_5.addLayout(self.horizontalLayout_9)

        self.retranslateUi(pctmSettingsDialog)
        self.buttonBox.accepted.connect(pctmSettingsDialog.accept) # type: ignore
        self.buttonBox.rejected.connect(pctmSettingsDialog.reject) # type: ignore
        QtCore.QMetaObject.connectSlotsByName(pctmSettingsDialog)

    def retranslateUi(self, pctmSettingsDialog):
        _translate = QtCore.QCoreApplication.translate
        pctmSettingsDialog.setWindowTitle(_translate("pctmSettingsDialog", "PCTM settings"))
        self.label_9.setText(_translate("pctmSettingsDialog", "These settings set the actual sampling rate on the PCTM. All samples are sent to the PCTM-GUI application and displayed in the plots."))
        self.label.setText(_translate("pctmSettingsDialog", "Module NTC temperature"))
        self.nTC1Label.setText(_translate("pctmSettingsDialog", "Module NTC 1"))
        self.nTC3Label.setText(_translate("pctmSettingsDialog", "Module NTC 3"))
        self.nTC2Label.setText(_translate("pctmSettingsDialog", "Module NTC 2"))
        self.NTC1DoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.NTC2DoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.NTC3DoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.humidity1Label_3.setText(_translate("pctmSettingsDialog", "Set all to: "))
        self.setAllNTCdoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.label_5.setText(_translate("pctmSettingsDialog", "Humidity sensors"))
        self.humidity2Label.setText(_translate("pctmSettingsDialog", "Humidity 2"))
        self.humidity1Label.setText(_translate("pctmSettingsDialog", "Humidity 1"))
        self.humidity1DoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.humidity2DoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.humidity1Label_2.setText(_translate("pctmSettingsDialog", "Set all to: "))
        self.setAllHumdoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.label_6.setText(_translate("pctmSettingsDialog", "Peltier controller items"))
        self.coldplateLabel.setText(_translate("pctmSettingsDialog", "Coldplate temperature"))
        self.coldplateDoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.peltierILabel.setText(_translate("pctmSettingsDialog", "Peltier current / voltage"))
        self.peltierDoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.pIDLabel.setText(_translate("pctmSettingsDialog", "PID Values"))
        self.pIDDoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.humidity1Label_4.setText(_translate("pctmSettingsDialog", "Set all to: "))
        self.setAllPeltierdoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.label_10.setText(_translate("pctmSettingsDialog", "Various"))
        self.gasFlowLabel.setText(_translate("pctmSettingsDialog", "Vacuum chuck temperature"))
        self.vacuumChuckDoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.vacuumPressureDoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.vacuumPressureLabel.setText(_translate("pctmSettingsDialog", "Vacuum Pressure"))
        self.vacuumChuckLabel.setText(_translate("pctmSettingsDialog", "Purge gas flow"))
        self.gasFlowDoubleSpinBox.setSuffix(_translate("pctmSettingsDialog", " Hz"))
        self.save_pushButton.setText(_translate("pctmSettingsDialog", "Save to file"))
        self.load_pushButton.setText(_translate("pctmSettingsDialog", "Load from file"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    pctmSettingsDialog = QtWidgets.QDialog()
    ui = Ui_pctmSettingsDialog()
    ui.setupUi(pctmSettingsDialog)
    pctmSettingsDialog.show()
    sys.exit(app.exec_())
