# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 17:25:58 2020

@author: Magne Lauritzen

This file holds the PctmApp class which, when instantiated, runs the PCTM-GUI
application.
"""
from . import initialize  # Must be called before all other imports to set up local-specific paths
import logging
from typing import TYPE_CHECKING, Any
from autologging import traced  # type: ignore
from .gui.mainwindow import PCTMGUI
from .gui import dialogs
from . import loggers
from .pctm import PCTMAggregator
from PyQt5.QtGui import QMessageBox

logger = loggers.get_logger(__name__)

if TYPE_CHECKING:
    from .pctm import PCTM


@traced(logger, "connectDialog", "_close", "__exit__")
class PctmApp():
    """
    This class is the toplevel class for the PCTM-GUI application. When instantiated,
    it will open up the main application window. You should not need to 
    instantiate this yourself, that is already performed by PCTM-GUI.py.
    """

    def __init__(self) -> None:
        self.PCTMAggregator = PCTMAggregator()
        self.gui = PCTMGUI()
        self.gui.closeSignal.connect(self._close)
        self._connectMainWindowButtons()
        self._connectSignals()
        self.gui.show()

    def connectDialog(self, _: Any = None) -> None:
        """Opens up a dialog where the user can connect to a PCTM on one of the 
        USB ports."""
        d = dialogs.PCTMConnectionDialog(self.PCTMAggregator)
        d.exec_()

    def _showAutoConnectDialog(self, port: str) -> None:
        """Called when a new PCTM is connected, according to the PCTMAggregator.
        Opens a window asking if the user wishes to connect to it."""
        d = dialogs.AutoConnectDialog(port)
        self.PCTMAggregator.signals.PCTMDetached.connect(d.disconnected)
        accepted = d.exec_()
        if accepted == QMessageBox.Ok:
            self.PCTMAggregator.connectNewPCTM(port)

    def _connectMainWindowButtons(self) -> None:
        self.gui.actionPctmConnect.triggered.connect(self.connectDialog)

    def _connectSignals(self) -> None:
        self.PCTMAggregator.signals.PCTMAttached.connect(self._showAutoConnectDialog)
        self.PCTMAggregator.signals.PCTMConnected.connect(self.gui._addPctm)

    def _close(self) -> None:
        """Closes the PCTMAggregator, which in turn causes all PCTM handlers,
        loggers, etc. to also close."""
        logger.info("Closing the PCTM Aggregator")
        self.PCTMAggregator.close()

    def __enter__(self) -> 'PctmApp':
        """Implements 'with ... as ...' compatibility"""
        return self

    def __exit__(self, type, value, traceback) -> bool:  # type: ignore
        # shut down logging
        try:
            logger.info("Shutting down loggers.")
            loggers.clean_loggers()
            logging.shutdown()
        except:
            logger.exception("An exception occurred while trying to shutdown "
                             "the logger module.")
        return False
