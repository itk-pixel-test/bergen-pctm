# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 13:08:44 2020

@author: Magne Lauritzen

Just a file containing small helpful functions that don't fit in anywhere else.
"""
from . import loggers
from typing import Union
from pathlib import Path
import os

logger = loggers.get_logger(__name__)


def ensure_dir(dir_path: Union[str, Path]) -> None:
    """Creates a path if it does not exist from before."""
    if isinstance(dir_path, Path):
        dir_path = str(dir_path)
    if not os.path.exists(dir_path):
        logger.debug(f'''Creating path {dir_path}''')
        os.makedirs(dir_path)
