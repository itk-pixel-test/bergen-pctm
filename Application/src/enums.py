# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 22:41:50 2020

@author: magne.lauritzen

This file contains the Enums that define the communication with the PCTM. The
PCTM has a matching set of Enums in its firmware. Making changes to this file
WILL break communication with the PCTM unless the PCTM firmware has matching
changes.
"""
from enum import Enum


class Requests(Enum):
    """The Requests enum define commands that falls under the "Do something, PCTM"
    umbrella of commands."""
    GET_UPTIME = 0  # Request the PCTM to return its uptime
    REQ_SYNC = 1  # Request the PCTM to synchronize with the PC
    DO_SYNC = 2  # Perform a synchronization with the PC
    REQ_SAMPLEFREQS = 3  # Request the PCTM to return sample frequency settings
    REQ_PIDS = 4  # Request the PCTM to return PID settings
    REQ_ICALIBRATE = 5  # Request that the PCTM calibrates its current sensor
    REQ_PIDDISABLE = 6  # Request that the PCTM disables its Peltier controller
    REQ_PIDENABLE = 7  # Request that the PCTM enables its Peltier controller
    REQ_RESET = 8  # Request that the PCTM resets its nonvolatile memory
    REQ_CONSTANTENABLE = 9 # Request that the PCTM disables TEC PID (Turns on constant current)
    REQ_CONSTANTDISABLE = 10 # Request that the PCTM enables TEC PID (Turns off constant current)
    REQ_ACTIVATEINTERLOCK = 11 # Request that the PCTM activates interlock
    REQ_DEACTIVATEINTERLOCK = 12 # Request that the PCTM deactivates interlock
    REQ_INTERLOCK = 13 # Request interlock settings
    REQ_ID = 14
    def _packID(self) -> int:
        return 0


class PeltierSettings(Enum):
    """The PeltierSettings enum define the parameters that are affiliated with
    the Peltier PID loop that runs on the PCTM."""
    KP_TEC = 0  # TEC P PID parameter
    KI_TEC = 1  # TEC I PID parameter
    KD_TEC = 2  # TEC D PID parameter
    SETPOINT = 3  # Temperature setpoint
    VLIMIT = 4  # voltage limit
    ILIMIT = 5  # current limit
    CC = 6  # constant current
    CV = 7  # constant voltage
    PID_TEC = 8  # switch to PID control after being in CC or CI mode
    PSU_FREQ = 9  # PSU PID sample frequency
    TEC_FREQ = 10 # TEC PID sample frequency

    KP_PSU = 11  # PSU P PID parameter
    KI_PSU = 12  # PSU I PID parameter
    KD_PSU = 13  # PSU D PID parameter
    CCTRL = 14 # Constant PSU control value
    PID_PSU = 15  # Switch to PID control after being in CCTRL mode

    PSU_MAX = 16  # Maximum PSU current. Only relevant when using ratiometric control.
    COMM_MODE = 17  # Ratiometric (0) or RS232 (1) PSU control TODO: Implement
    RAMP = 18 # Activate ramping of setpoint
    SETRAMP = 19 #Sets the ramp rate
    TEC_MANUAL = 20
    TEC_AUTO = 21
    PSU_MANUAL = 22
    PSU_AUTO = 23
    PON_TEC = 24
    def _packID(self) -> int:
        return 1


class MonitorFSettings(Enum):
    """The MonitorFSettings enum define the parameters that correspond with the
    sampling frequencies of each data source."""
    NTC_1_F = 0  # Module NTC 1 sample frequency
    NTC_2_F = 1  # Module NTC 2 sample frequency
    NTC_3_F = 2  # Module NTC 3 sample frequency
    HUMIDITY_1_F = 3  # Humidity sensor 1 sample frequency
    HUMIDITY_2_F = 4  # Humidity sensor 2 sample frequency
    GAS_FLOW_F = 5  # Gas flow sensor sample frequency
    CP_T_F = 6  # Coldplate temperature sample frequency
    VC_T_F = 7  # Vacuum chuck temperature sample frequency
    TEC = 8  # Peltier current sample frequency
    PID = 9 # PID values sample frequency
    VACUUM_PRESSURE = 10 #Vacuum pressure sample frequency

    def _packID(self) -> int:
        return 2


class SystemSettings(Enum):
    """The SystemSettings enum define the parameters that are affiliated with
    low-level settings on the PCTM microcontroller."""
    ID = 0  # Set PCTM identity (0-F)
    def _packID(self) -> int:
        return 3

class SampleSource(Enum):
    """The SampleSource enum define the source of datums. These can include
    sensor samples or settings values that are returned periodically."""
    NTC_1 = 0
    NTC_2 = 1
    NTC_3 = 2
    RH_1 = 3
    RH_2 = 4
    DP_1 = 5
    DP_2 = 6
    GAS_T_1 = 7
    GAS_T_2 = 8
    GAS_FLOW = 9
    CP_T = 10
    VC_T = 11
    TEC_I = 12
    TEC_V = 13
    TEC_PID_SETPOINT = 14  # The temperature the module should reach
    PSU_PID_SETPOINT = 15
    PSU_CONTROL = 16  # The control value sent to the TEC PSU
    VACUUM_PRESSURE = 17 #vacuum chuck pressure gauge

class ThermalCycle(Enum):
    """ Defines the various settings for automation of thermal cycling"""
    START_CYCLE_T_1 = 0
    START_CYCLE_T_2 = 1
    END_CYCLE_T_1 = 2
    END_CYCLE_T_2 = 3
    NUMBER_CYCLES_1 = 4
    NUMBER_CYCLES_2 = 5
    SOAK_TIME = 6
    RAMP = 7

class ThermalCycleStates(Enum):
    """ Defines the different thermal cycling states"""
    TARGET_LOW_TEMP = 0
    TARGET_HIGH_TEMP = 1
    NEXT_TARGET = 2
    SOAKING = 3
    IDLE = 4
    ENDCYCLE = 5
    CYCLE_OFF = 6

class InterlockSettings(Enum):
    """ Defines the various settings for interlock"""
    UPPER = 0
    LOWER = 1
    DIFF_DP_NTC = 2
    ACTIVATED_HIGH = 3
    ACTIVATED_LOW = 4
    ACTIVATED_DEW = 5
    ENABLE = 6 #Used for PCTM to send enable info
    ACTIVATED_ABSENT = 7
    COLDPLATE = 8
    ACTIVATED_COLDPLATE = 9
    OVERHEAT = 10
    LV_ENABLE = 11
    LV_CHANNEL = 12
    HV_ENABLE = 13
    HV_QUEUE = 14
    HV_PORT = 15
    LV_PORT = 16
    LV_QUEUE = 17
    LV_MODE = 18
    HV_MODE = 19


    def _packID(self) -> int:
        return 4

class SystemInfo(Enum):
    """The SystemInfo enum define the parameters corresponding to the different
    types of PCTM system information like uptime and whether it is ready to
    perform a syncrhonization of its clock."""
    UPTIME = 0
    SYNC_READY = 1


class RxPacketID(Enum):
    """The RxPacketID enum define the types of data packets that can be returned
    from the PCTM."""
    SAMPLE_DATA = 0  # As the name implies, packets with Packet ID 0 contains a sensor sample
    SYS_INFO = 1
    SAMPLE_FREQ = 2
    PID_INFO = 3
    TEXT = 4
    ACK = 5  # An ACK command is returned after each received command by the PCTM.
    INTERLOCK_INFO = 6
    SYS_SETTING_INFO = 7
