# -*- coding: utf-8 -*-
"""
Created on Thu JULY 15 ‏‎10:54:40 ‎2021

@author: Wai Chun Leung

This file contains the ThermalCycling class which handles the automation of thermal cycling
"""

from .enums import ThermalCycle as tc
from .enums import ThermalCycleStates as tcstates
from .enums import SampleSource
from . import loggers
from typing import Dict, Any, Optional, Tuple, TYPE_CHECKING
from PyQt5 import QtCore  # type: ignore
from autologging import traced  # type: ignore
import time

if TYPE_CHECKING:
    from .pctmhandler import PCTMHandler

logger = loggers.get_logger(__name__)

class ThermalSignals(QtCore.QObject):
    cycling_done = QtCore.pyqtSignal(bool)
    stateChanged = QtCore.pyqtSignal(tcstates, float, float, float)

@traced(logger, "checkTemperature", "__init__", "restartCycle", "runCycle", "getState", "soaking", exclude=True)
class ThermalCycling():

    def __init__(self, pctmhandler: "PCTMHandler") -> None:
        self.thermalcyclesettings : Dict[Any, float] = {}
        self.start_time : float = 0
        self.num_cycle : int = 0
        self.cycle_target: float = 0
        self.handler = pctmhandler
        self.low_limit : float = 0
        self.high_limit: float = 0
        self.extreme_cycle: bool = False
        self.ramp_rate: float = 0
        self.signals = ThermalSignals()
        self.soak_done : bool = False
        self.temp_target: tcstates = tcstates.IDLE
        self.state: tcstates = tcstates.IDLE
        self.temp_target_low: bool = False
        self.temp_target_high: bool = False
        self.cycle_done: bool = False
        self.soak_in_range: bool = False
        self.oldState : tcstates = tcstates.CYCLE_OFF
        self.lastTime : float = 0.0


    def getThermalCycleSettings(self, d : Dict[Any, float]) -> None:
        self.thermalcyclesettings = d
        self.ramp_rate = self.thermalcyclesettings[tc.RAMP]

    def setTemperatureTarget(self, target: float, ramp: float) -> None:
        self.handler.controlTempSetpoint(target, ramp)

    def checkTemperature(self, datum: Tuple[float, int, float]) -> bool:
        """Check if the temperature is in the soak time range."""
        if ((datum[0] >= self.low_limit) & (datum[0] <= self.high_limit)):
            return True
        else:
            return False

    def restartCycle(self) -> None:
        self.num_cycle = 0
        self.start_time = 0
        self.soak_in_range = False
        self.cycle_target = 0
        self.low_limit = 0
        self.high_limit = 0
        self.temp_target_low = True
        self.temp_target_high = False
        self.soak_done = False
        self.temp_target = tcstates.IDLE
        self.cycle_done = False
        self.extreme_cycle = False
        self.state = tcstates.IDLE

        if (self.thermalcyclesettings[tc.NUMBER_CYCLES_1] == 0):
            self.extreme_cycle = True

        if ((self.thermalcyclesettings[tc.NUMBER_CYCLES_1] == 0) & (self.thermalcyclesettings[tc.NUMBER_CYCLES_2] == 0)):
            self.cycle_done = True

    def endCycle(self) -> bool:
        self.handler.controlTempSetpoint(25, self.ramp_rate) # Set the temperature target back to room temperature after cycling is done
        self.signals.cycling_done.emit(False) #Emit false to deactivate cycling
        return False

    def soaking(self, datum: Tuple[float, int, float]) -> bool:
        if not self.soak_in_range:
            self.start_time = datum[2] #Start time for soaking

        self.soak_in_range = True
        soaked_time = datum[2] - self.start_time
        if soaked_time >= (self.thermalcyclesettings[tc.SOAK_TIME] * 60):
            self.soak_in_range = False
            self.soak_done = True
        return True

    def nextTarget(self) -> bool:
        if (self.num_cycle >= self.thermalcyclesettings[tc.NUMBER_CYCLES_1]) & (self.extreme_cycle == False):
            self.num_cycle = 0
            self.extreme_cycle = True

        if (self.num_cycle >= self.thermalcyclesettings[tc.NUMBER_CYCLES_2]) & (self.extreme_cycle == True):
            self.cycle_done = True

        if self.temp_target == tcstates.TARGET_LOW_TEMP:
            self.temp_target_high = True
        if self.temp_target == tcstates.TARGET_HIGH_TEMP:
            self.temp_target_low = True
        return True

    def TargetLowTemp(self) -> bool:
        if self.extreme_cycle == False:
            self.cycle_target = self.thermalcyclesettings[tc.START_CYCLE_T_1]
        else:
            self.cycle_target = self.thermalcyclesettings[tc.START_CYCLE_T_2]
        self.setTemperatureTarget(self.cycle_target, self.ramp_rate)
        self.low_limit = self.cycle_target - 10
        self.high_limit = self.cycle_target + 5

        self.temp_target = tcstates.TARGET_LOW_TEMP
        self.temp_target_low = False

        return True

    def TargetHighTemp(self) -> bool:
        if self.extreme_cycle == False:
            self.cycle_target = self.thermalcyclesettings[tc.END_CYCLE_T_1]
        else:
            self.cycle_target = self.thermalcyclesettings[tc.END_CYCLE_T_2]
        self.setTemperatureTarget(self.cycle_target, self.ramp_rate)
        self.low_limit = self.cycle_target - 5
        self.high_limit = self.cycle_target + 10

        self.temp_target = tcstates.TARGET_HIGH_TEMP
        self.temp_target_high = False
        self.num_cycle += 1

        return True

    def getState(self, datum: Tuple[float, int, float]) -> tcstates:
        if self.cycle_done:
            return tcstates.ENDCYCLE

        if self.temp_target_low:
            return tcstates.TARGET_LOW_TEMP

        elif self.temp_target_high:
            return tcstates.TARGET_HIGH_TEMP

        if (self.soak_done):
            self.soak_done = False #Reset soak flag
            return tcstates.NEXT_TARGET

        if (self.checkTemperature(datum)):
            return tcstates.SOAKING

        return tcstates.IDLE

    def runCycle(self, source: SampleSource, datum: Tuple[float, int, float]) -> bool:

        returnValue : bool = True

        if source == SampleSource.NTC_1:
            self.state = self.getState(datum)

            """#No switch case function in python 3.8"""
            if self.state == tcstates.SOAKING:
                returnValue = self.soaking(datum)
            elif self.state == tcstates.NEXT_TARGET:
                returnValue = self.nextTarget()
            elif self.state == tcstates.TARGET_LOW_TEMP:
                returnValue = self.TargetLowTemp()
            elif self.state == tcstates.TARGET_HIGH_TEMP:
                returnValue = self.TargetHighTemp()
            elif self.state == tcstates.ENDCYCLE:
                returnValue = self.endCycle()
            elif self.state == tcstates.IDLE:
                returnValue = True

            time_left = (self.thermalcyclesettings[tc.SOAK_TIME] * 60) - (datum[2] - self.start_time)

            """Sending states to display"""
            if self.state == tcstates.SOAKING:
                if (time.time() - self.lastTime) > 1:
                    self.signals.stateChanged.emit(self.state, self.cycle_target, self.ramp_rate, time_left)
                    self.lastTime = time.time()
                self.oldState = self.state
                return returnValue

            if self.state != self.oldState:
                if self.state != tcstates.IDLE:
                    time_left = 0.0
                    self.signals.stateChanged.emit(self.state, self.cycle_target, self.ramp_rate, time_left)
                self.oldState = self.state
                return returnValue

        return returnValue
