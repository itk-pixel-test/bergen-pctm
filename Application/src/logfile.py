# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 10:40:47 2020

@author: Magne Lauritzen

This file contains the Logfile class which handles the logging of data from the 
PCTM to a HDF5 file.
"""
from .definitions import dataset_definitions
from .enums import SampleSource
from . import loggers
from pathlib import Path
from autologging import traced  # type: ignore
from typing import Union, Tuple, Dict, Optional
import numpy as np 
import h5py  # type: ignore

logger = loggers.get_logger(__name__)


@traced(logger, "saveDatum", "_getDset", exclude=True)
class Logfile():
    """
    The Logfile class handles the logging of data from the PCTM to an HDF5 
    file. It handles the opening and graceful closing of HDF5 files and subsampling
    of data.
    """

    def __init__(self, filepath: Union[str, Path] = None) -> None:
        self.fpath = filepath
        self.file: Optional[h5py.File] = None
        self._logPeriods: Dict[SampleSource, float] = {}

    def open(self, filepath: Union[str, Path] = None) -> None:
        """
        Opens the HDF5 file given by 'filepath' for logging. The file does not 
        need to exist from before. If it already exists, data will be appended 
        to it. If the Logfile instance already is logging to a different HDF5 
        file, it will be closed first.
        """
        if self.fpath == filepath:
            return
        if filepath:
            self.fpath = filepath
        self.close()  # Close any file that is already open
        self._openHDF5()

    def close(self) -> None:
        """Closes the HDF5 file that is being logged to."""
        logger.info(f"Closing logfile {self.fpath}")
        if self.file:
            self.file.flush()
            self.file.close()
            self.file = None

    def getLoggingFrequencies(self) -> Dict[SampleSource, float]:
        """Returns the logging frequencies for this Logfile. See 
        setLoggingFrequency for more details."""
        d = {}
        for ss_enum, period in self._logPeriods.items():
            freq = 1 / period if period != 0 else 0
            d.update({ss_enum: freq})
        return d

    def setLoggingFrequencies(self, frequencies: Dict[SampleSource, float]) -> None:
        """Sets the logging frequencies for this Logfile. See 
        setLoggingFrequency for more details."""
        for ss_enum, f in frequencies.items():
            self.setLoggingFrequency(ss_enum, f)

    def setLoggingFrequency(self, source: SampleSource, frequency: float) -> None:
        """Sets the frequency of logging to file. One can for example sample
        the module temperature at 20Hz, but log only at 1Hz. By default, all
        samples are logged to file.
        Set frequency to 0 to stop logging.
        """
        if frequency > 0:
            self._logPeriods[source] = 1 / frequency
        else:
            self._logPeriods[source] = 0

    def saveDatum(self, source: SampleSource, datum: Tuple[float, int, float]) -> None:
        """
        Saves one datum (i.e. a sensor sample) to the HDF5 file. Only saves it
        if enough time has passed since the last time its datasource last had a 
        datum saved, according to the logging frequencies that have been set with
        setLoggingFrequencies().
        """
        log_period = self._logPeriods.get(source, 1E-3)
        if log_period == 0:
            return
        dset = self._getDset(source)
        n_elements = dset.attrs['elements']
        if n_elements > 0 and (datum[2] - log_period) < dset[n_elements - 1, 'ts']:
            return
        if n_elements == dset.len():
            dset.resize((dset.shape[0] + 1000, dset.shape[1]))
        dset[n_elements] = datum
        dset.attrs['elements'] = n_elements + 1

    def _openHDF5(self) -> None:
        if not self.fpath:
            raise Exception("Cannot open logfile. No filepath has been set.")
        self.file = h5py.File(str(self.fpath), 'a')

    def _getDset(self, source: SampleSource) -> h5py.Dataset:
        """Returns the dataset that corresponds to the provided sample source.
        Creates it if it does not exist."""
        if self.file is None:
            raise OSError("Cannot open dataset. Log file has not been opened yet.")
        dsetdef = dataset_definitions[source]
        dsname = dsetdef['name']
        if dsname not in self.file.keys():
            # Create dataset
            dtype = np.dtype([('x', np.float32), ('c', np.uint8), ('ts', np.float64)])
            ds = self.file.create_dataset(dsname, shape=(0, 1),
                                          maxshape=(None, 1),
                                          dtype=dtype, chunks=True,
                                          compression="gzip",
                                          shuffle=True)
            ds.attrs['description'] = dsetdef['description']
            ds.attrs['unit'] = str(dsetdef['unit'])
            ds.attrs['elements'] = 0
        return self.file[dsname]
