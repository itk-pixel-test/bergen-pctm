# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 17:56:36 2020

@author: Magne Lauritzen

This file contains the AppConfig class, a singleton that holds the application
configuration.
"""

import configparser
from typing import Any
from pathlib import Path
from shutil import copy2
from copy import copy
import sys


class AppConfig():
    """
    The AppConfig class is a singleton that handles the application configuration
    and the affiliated configuration file, .appconfig.INI. 

    It exposes the functions read() and write() which reads .appconfig.INI and
    writes the current settings to it, respectively.
    
    AppConfig exposes a dict-like interface for fetching settings. For example,
    to obtain the ConsoleSeverity setting in the LOGGING section, you would do:
        cfg = AppConfig()
        severity = cfg['LOGGING']['ConsoleSeverity']
    
    Mypy does not understand that the singleton _does_ have a class attribute
    "config" once instantiated, so all references to self.config has been 
    type-ignored.
    """
    cfg_path = Path(__file__).parents[1] / Path(".appconfig.INI")
    default_cfg_path = Path(__file__).parents[0] / Path("assets/.appconfig_default.INI")
    _instance = None

    def __new__(cls) -> 'AppConfig':
        """Initialization occurs here in __new__ to enable singleton behaviour."""
        if cls._instance is None:
            cls._instance = super(AppConfig, cls).__new__(cls)
            cls._instance.config = configparser.ConfigParser()
            cls._instance.read()
        return cls._instance  # type: ignore

    def read(self) -> None:
        """Loads the settings in the application configuration file .appconfig.INI.
        If it is not found in the expected location, it copies a default config 
        file into the expected location and loads it."""
        if not self.cfg_path.exists():
            copy2(str(self.default_cfg_path), str(self.cfg_path))
        try:
            assert self.cfg_path.exists()
        except:
            print("Could not locate the configuration file {}".format(self.cfg_path))
            sys.exit(1)
        self.config.optionxform = str  # type: ignore # preserves capitalization
        self.config.read(self.cfg_path)  # type: ignore

    def write(self) -> None:
        """Writes the current application configuration to the configuration 
        file .appconfig.INI."""
        # We must first remove the slightly hacky "log file name" LogName from
        # ¤the appconfig
        cpy = copy(self.config)  # type: ignore
        try:
            del cpy['LOGGING']['LogName']
        except KeyError:
            pass
        # Write settings to file
        with open(self.cfg_path, "w+") as configfile:
            cpy.write(configfile)

    # Utility functions
    def get_userdatadir(self) -> Path:
        return Path(self['PATHS']['UserDataDir'])

    # Wrap internal CongigParser so that it is directly accessible through
    # AppConfig, like AppConfig['PATHS'] instead of AppConfig.config['PATHS']
    def __delitem__(self, key: Any) -> None:
        del self.config[key]  # type: ignore

    def __getitem__(self, key: Any) -> Any:
        return self.config[key]  # type: ignore

    def __setitem__(self, key: Any, value: Any) -> None:
        self.config[key] = value  # type: ignore
