# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 15:54:56 2020

@author: Magne Lauritzen

This file contains functions related to loggers. PCTM-GUI makes extensive
use of loggers, and each file has its own logger. Logfiles are placed in
bergen-pctm/Application/logs.

Get a new logger with get_logger().
"""
import logging
import autologging # type: ignore
from numpy import set_printoptions
from typing import Any, no_type_check
from datetime import datetime
from pathlib import Path
from termcolor import colored # type: ignore
import sys
from .appconfig import AppConfig

config = AppConfig()

# We need to reduce the amount of array elements which are logged by default.
# The setting is changed here so that all loggers obtained through get_logger()
# will be having the correct settings.
set_printoptions(precision=3, threshold=10, edgeitems=1)


def clean_loggers() -> None:
    """This function is called when the application quits, and ensures that 
    all loggers are properly removed. Otherwise we get double logger-statements
    the next time the app runs."""
    existing_loggers = logging.root.manager.loggerDict  
    for name in existing_loggers.keys():
        logger = logging.getLogger(name)
        logger.handlers = []
        del logger
    logging.root.manager.loggerDict.clear()  
    logging.shutdown()


@no_type_check
def logger_resetter(func):
    """
    This decorator ensures that loggers are reset between each import or script 
    run in IPython. Otherwise we get multiplied logger outputs.
    """

    def counter(*args, **kwargs):
        counter.calls += 1
        return func(*args, **kwargs)

    counter.calls = 0
    clean_loggers()
    counter.__name__ = func.__name__
    return counter


@logger_resetter
def get_logger(name: str) -> logging.Logger:
    """
    This function returns a new logger with the name that is passed as 
    the argument. The logger will print all its statements to the terminal and
    to the logfile, depending on the severity.
    """
    loggername = name.split(".")[-1]
    print("Creating logger \"{}\"".format(loggername))
    # Get a logger by name name
    logger = logging.getLogger(loggername)
    # Set its level to the lowest. Its handlers defined later sets the level for each output
    logger.setLevel(autologging.TRACE)
    # Create a formatter for log files
    logFormatter = logging.Formatter("[%(name)s][%(funcName)s][%(lineno)d][%(levelname)s] %(message)s")
    # Create a file handler that will output the messages of logger to file
    logpath = Path(config['PATHS']['TopDir']) / Path(config['LOGGING']['LogPath'])
    # We retain the log file name in the appconfig. This is slightly
    # hacky, since the log file name ought to be in a separate app state, but
    # since it is a very local variable, I think it is OK.
    try:
        filename = config['LOGGING']['LogName']
    except:
        filename = datetime.now().strftime("%Y-%m-%d %H-%M-%S")
        config['LOGGING']['LogName'] = filename
    fileHandler = logging.FileHandler("{0}/{1}.log".format(logpath, filename), 'a')
    fileHandler.setFormatter(logFormatter)
    # set logging level
    level = logging.getLevelName(config['LOGGING']['FileSeverity'])
    fileHandler.setLevel(level)
    logger.addHandler(fileHandler)

    # Create a formatter for console
    logFormatter = ColoredFormatter("[%(levelname)s]  %(message)s")
    # Create a console handler that will output the messages of logger to the console
    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(logFormatter)
    # set logging level
    level = logging.getLevelName(config['LOGGING']['ConsoleSeverity'])
    consoleHandler.setLevel(level)
    logger.addHandler(consoleHandler)
    # return logger
    return logger


# Custom formatter which prints logger statements in colors depending on their severity
class ColoredFormatter(logging.Formatter):
    """"
    ColoredFormatter is a custom formatter which prints logger statements in colors 
    depending on their severity.
    """
    colors = {logging.DEBUG: {'text': "white", 'bkg': "on_blue"},
              logging.INFO: {'text': "white", 'bkg': "on_grey"},
              logging.WARNING: {'text': "yellow", 'bkg': "on_grey"},
              logging.ERROR: {'text': "grey", 'bkg': "on_red"}}

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)

    def format(self, record: logging.LogRecord) -> str:
        # Save the original format configured by the user
        # when the logger formatter was instantiated
        format_orig = self._style._fmt

        # Replace the original format with one customized by logging level
        c = ColoredFormatter.colors.get(record.levelno, {'text': None, 'bkg': None})  # type: ignore
        if record.levelno == logging.DEBUG:
            self._style._fmt = colored(format_orig, c['text'], c['bkg'])

        elif record.levelno == logging.INFO:
            self._style._fmt = colored(format_orig, c['text'], c['bkg'])

        elif record.levelno == logging.WARNING:
            self._style._fmt = colored(format_orig, c['text'], c['bkg'])

        elif record.levelno == logging.ERROR:
            self._style._fmt = colored(format_orig, c['text'], c['bkg'])

        # Call the original formatter class to do the grunt work
        result = logging.Formatter.format(self, record)

        # Restore the original format configured by the user
        self._style._fmt = format_orig

        return result
