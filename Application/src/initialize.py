# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 12:58:46 2020

@author: Magne Lauritzen

This file initializes the PCTM-GUI application when imported. It should be called
even if you are creating an application that does not use the GUI.

* Creates the user data directory and saves the directory info to the .appconfig.INI file.
* Finds the absolute path of top dir and saves it to .appconfig.INI.
"""
from pathlib import Path
from .appconfig import AppConfig
import appdirs  # type: ignore
import os
import platform
import ctypes
import h5py  # type: ignore
import pyqtgraph as pg  # type: ignore
from PyQt5.QtGui import QIcon  # type: ignore
from PyQt5.QtCore import QSize, QCoreApplication  # type: ignore

config = AppConfig()

# Tell windows that application is its own user model so that the taskbar logo is correct
if platform.system() == "Windows":
    myappid = 'PCTM-GUI'
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
# Allows sharing OpenGL resources, like textures, between QOpenGLWidget
# instances that belong to different top-level windows.
QCoreApplication.setAttribute(18, True)
# Start the QT app and set logo
app = pg.mkQApp()
# TODO: Create an icon
#    p = Path(config['PATHS']['TopDir']) / "data_explorer/assets/logo.png"
#    app_icon = QIcon()
#    app_icon.addFile(str(p), QSize(64,64))
#    app.setWindowIcon(app_icon)

# Generate user data directory path, verify it exists, create if not,
# and save path to .appconfig.INI
override_userdir = config['PATHS'].getboolean('OverrideUserDataDir')
if not override_userdir:
    p = Path(appdirs.user_data_dir()) / Path(config['APPINFO']['AppName']) / Path(str(config['APPINFO']['AppVersion']))
    config['PATHS']['UserDataDir'] = str(p)
userdir_path = Path(config['PATHS']['UserDataDir'])
if not userdir_path.exists():
    os.makedirs(str(userdir_path))

# Set top directory for application in appconfig.INI
thisPath = Path(os.path.realpath(__file__))
topDir = thisPath.parents[1]
config['PATHS']['TopDir'] = str(topDir)
config.write()
print(f"Top dir set to {topDir}")

# Sett QAppConfig info
QCoreApplication.setOrganizationName(config['APPINFO']['AppOwner'])
QCoreApplication.setApplicationName(config['APPINFO']['AppName'])

# Set default filemode to 'r' to supress h5py warnings
h5py.get_config().default_file_mode = 'r'
