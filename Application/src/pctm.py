# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 14:52:23 2020

@author: Magne Lauritzen

This file defines the PCTM and PCTMAggregator classes. The PCTM class is the 
user interface for one PCTM and incorporates one PCTM handler and one data logger.

The PCTMAggregator class handles the creation and destruction of the PCTM 
instances.
"""

import serial  # type: ignore
from uuid import uuid4
from PyQt5 import QtCore  # type: ignore
from typing import Union, List, Optional, Any
from pathlib import Path
from autologging import traced  # type: ignore
from src.definitions import PCTMLogException
from src.logfile import Logfile
from src.pctmhandler import PCTMHandler
from src import loggers
from src.influx import Influx
from src.thermalcycle import ThermalCycling
from src.interlock import Interlock
from .gui.dialogs import ExceptionDialog

logger = loggers.get_logger(__name__)


@traced(logger, "__init__", "_listenForPCTMs", "_startPCTMListener",
        "getPCTMPorts", exclude=True)
class PCTMAggregator(QtCore.QObject):
    """
    The PCTMAggregator class is a a toplevel class that can organise any number 
    of simultaneously connected PCTMs. It listens on all COM ports for an 
    Arduino Nano Every and emits PCTMAttached when one is found.
    
    The PCTMAggregator handles connecting and graceful disconnecting from the 
    PCTMs. See connectNewPCTM() and disconnectPCTM().
    
    You must call PCTMAggregator.close() before application exit.
    """

    def __init__(self) -> None:
        super().__init__()
        self.signals = PCTMAggregatorSignals()
        self.PCTMPorts: List[str] = []  # All ports where an unconnected PCTM appears to be present
        self._PCTMs: List[PCTM] = []
        self._startPCTMListener()

    def connectNewPCTM(self, port: str) -> None:
        """Connects to a PCTM on the given port. Raises an exception if the 
        device does not appear to be a PCTM."""
        if port == "":
            return
        if port not in [p.handler.link.connection.name for p in self._PCTMs
                        if p.handler.link is not None]:
            pctm = PCTM(self)
            pctm.connect(port)
            self._PCTMs.append(pctm)
            self.signals.PCTMConnected.emit(pctm)
        else:
            raise OSError("Attempting to connect to a PCTM that is already connected")

    def disconnectPCTM(self, name: str = None, pctm: 'PCTM' = None) -> None:
        """Disconnects a PCTM and removes it from the application. Either give 
        the PCTM you wish to close by name or by object."""
        if name is None and pctm is None:
            raise ValueError("Either name or pctm must be provided.")
        if name is not None:
            name_pctm_dict = {p.name: p for p in self._PCTMs}
            if name in name_pctm_dict.keys():
                name_pctm_dict[name]._close()
                del self._PCTMs[list(name_pctm_dict.keys()).index(name)]
            else:
                raise ValueError(f"A PCTM by name \"{name}\" could not be found "
                                 "in the PCTM Aggregator.")
        if pctm is not None:
            if pctm not in self._PCTMs:
                raise ValueError("Somehow the PCTM provided could not be found "
                                 "in the PCTM Aggregator. Well done.")
            pctm._close()
            del self._PCTMs[self._PCTMs.index(pctm)]

    def getPCTMPorts(self) -> List[str]:
        """Returns all ports where a PCTM appears to be connected. Currently
        returns all ports where an Arduino Nano or Nano Every is connected.
        TODO: Reprogram the FTDI PID to a value that is uniquely identifying the
        arduino as a PCTM."""
        # find USB devices
        open = serial.tools.list_ports.comports(include_links=True)
        nanos = []
        for o in open:
            if o.description.lower().startswith("arduino nano"):
                nanos.append(o.device)
        return nanos

    def _listenForPCTMs(self) -> None:
        """Listens for new PCTMs being connected on the USB ports and emits
        the PCTMConnected signal when one is connected and the PCTMDisconnected
        when one is removed. Runs every 500ms."""
        ports = self.getPCTMPorts()
        disconnected_ports = list(set(self.PCTMPorts) - set(ports))
        connected_ports = list(set(ports) - set(self.PCTMPorts))
        self.PCTMPorts = ports
        for port in connected_ports:
            logger.info(f"PCTM detected on port {port}")
            self.signals.PCTMAttached.emit(port)
        for port in disconnected_ports:
            logger.info(f"PCTM removed from port {port}")
            self.signals.PCTMDetached.emit(port)

    def _startPCTMListener(self) -> None:
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self._listenForPCTMs)
        self.timer.start(500)

    def close(self) -> None:
        """Closes all the PCTMs organised by this aggregator."""
        for pctm in self._PCTMs:
            pctm.close()
        self.timer.stop()

    def __enter__(self) -> 'PCTM':
        """Enables "with ... as ..." support"""
        return self

    def __exit__(self, type, value, traceback) -> bool:  # type: ignore
        self.close()
        return False


class PCTMSignals(QtCore.QObject):
    """This class defines the signals emitted by the PCTM class.
        closing     : Emitted when the PCTM class closes down.
        renamed     : Emitted when the PCTM is renamed.
    """
    closing = QtCore.pyqtSignal()
    renamed = QtCore.pyqtSignal(str)


@traced(logger, "__init__", "_logDatum", "_logInflux", 
        "_thermalCycle", "_interlock", exclude=True)
class PCTM():
    """
    The PCTM class represents one PCTM and its accompanying datalogger.
    This is normally the class you will instantiate when connecting to a new PCTM.
    
    With this class you can connect, disconnect, reconnect to a PCTM.
    You can also control the logging (Start, stop, pause, continue).
    For more detailed control of the PCTM, such as uploading new settings,
    access the methods of PCTM.handler directly.
    """

    def __init__(self, aggregator: PCTMAggregator = None) -> None:
        self.name = str(uuid4())
        self.aggregator = aggregator
        self.signals = PCTMSignals()
        self.logger = Logfile()
        self.influx = Influx()
        self.handler = PCTMHandler()
        self.thermalcycle = ThermalCycling(self.handler)
        self.interlock = Interlock(self.handler, self.thermalcycle, self.influx)
        self.handler.signals.newData.connect(self._logDatum)
        self.handler.signals.newData.connect(self._logInflux)
        self.handler.signals.newData.connect(self._thermalCycle)
        self.handler.signals.newData.connect(self._interlock)
        self._logState = 'stopped'
        self.influxState = False
        self.thermalcycleState = False
        self.interlockState = False

    def connect(self, port: str) -> None:
        self.handler.connect(port)

    def disconnect(self) -> None:
        self.handler.disconnect()

    def reconnect(self) -> None:
        self.handler.reconnect()

    def startLogging(self, filepath: Union[str, Path]) -> None:
        """Begins logging to the HDF5 file given by 'filepath'. This file 
        does not need to exist."""
        if self._logState != 'stopped':
            raise PCTMLogException("Cannot start logging unless it has been "
                                   "stopped first.")
        p = Path(filepath)
        try:
            self.logger.open(p)
            self._logState = 'running'
        except:
            logger.exception(f"Could not start logging to \"{p}\".")

    def stopLogging(self) -> None:
        """Stops logging sample data to file."""
        if self._logState not in ['running', 'paused']:
            return
        self._logState = 'stopped'
        try:
            self.logger.close()
        except:
            logger.exception("Could not stop logging.")

    def pauseLogging(self) -> None:
        """Pauses logging of sample data to file."""
        if self._logState != 'running':
            return
        self._logState = 'paused'

    def continueLogging(self) -> None:
        """Continues logging of sample data to file after being paused."""
        if self._logState != 'paused':
            return
        self._logState = 'running'

    def getPortName(self) -> Optional[str]:
        """Returns the name of the COM port that the PCTM is connected to, or 
        None if no PCTM is connected."""
        if self.handler.link is None:
            return None
        return str(self.handler.link.connection.port)

    def setName(self, name: str) -> None:
        """Give this PCTM a name. But don't get attached to it :)"""
        if self.name != name:
            self.name = name
            self.signals.renamed.emit(self.name)

    def close(self) -> None:
        """Closes the connection with the PCTM. If this instance was created 
        through a PCTMAggregator, it asks the aggregator to disconnect itself
        such that it is also removed from the Aggregators list of connected 
        PCTMs."""
        logger.info(f"Closing PCTM {self.name}")
        if self.aggregator is None:
            self._close()
        else:
            self.aggregator.disconnectPCTM(pctm=self)

    def _close(self) -> None:
        self.logger.close()
        self.handler.join()
        self.signals.closing.emit()

    def _logDatum(self, _: Any) -> None:
        src, datum = self.handler.dataqueue.get_nowait()  # Should never fail
        if self._logState == 'running' and self.logger.file is not None:
            self.logger.saveDatum(src, datum)

    def __enter__(self) -> 'PCTM':
        return self

    def __exit__(self, type, value, traceback) -> bool:  # type: ignore
        self.close()
        return False
        
    def _logInflux(self, _: Any) -> None:
        src, datum = self.handler.dataqueue_influx.get_nowait()
        if self.influxState:
            try:
                self.influx.writeSampleInflux(src, datum)
            except Exception as e:
                self.influxState = False
                ExceptionDialog(e)
                logger.exception(e)
                
    def _thermalCycle(self, _: Any) -> None:
        src, datum = self.handler.dataqueue_thermal.get_nowait()
        if self.thermalcycleState:
            if self.thermalcycle.runCycle(src, datum) == False:
                self.thermalcycleState = False
                
    def _interlock(self, _: Any) -> None:
        src, datum = self.handler.dataqueue_interlock.get_nowait()
        if self.interlockState:
            #if self.interlock.checkInterlock(src, datum):
            self.interlock.checkInterlock(src, datum)
                #self.interlockState = False

# Needs to be defined below the PCTM class for the PCTMConnected signal
class PCTMAggregatorSignals(QtCore.QObject):
    """This class defines the signals that the PCTMAggregator can emit.
        PCTMAttached  : Emitted when a new PCTM appears to have been plugged into the PC.
        PCTMDetached  : Emitted when a PCTM appears to have been unplugged from the PC.
        PCTMConnected : Emitted when a new PCTM is connected to.
    """
    PCTMAttached = QtCore.pyqtSignal(str)
    PCTMDetached = QtCore.pyqtSignal(str)
    PCTMConnected = QtCore.pyqtSignal(PCTM)
