# -*- coding: utf-8 -*-
"""
Created on Tue JULY 20 ‏‎15:09:13 ‎2021

@author: Wai Chun Leung

This file contains the Interlock class which handles the cooling interlock
"""

from autologging import traced  # type: ignore
from PyQt5 import QtCore  # type: ignore
import threading
import time
from typing import Dict, Any, Optional, Tuple, TYPE_CHECKING

from . import loggers
from .enums import SampleSource
from .enums import InterlockSettings as il
from .psu_remote.src.psu_hmp4040 import HMP4040
from .psu_remote.src.psu_k2450 import K2450
if TYPE_CHECKING:
    from .pctmhandler import PCTMHandler
    from .thermalcycle import ThermalCycling
    from .influx import Influx

logger = loggers.get_logger(__name__)

class InterlockSignals(QtCore.QObject):
    pid_button = QtCore.pyqtSignal(bool)
    interlock = QtCore.pyqtSignal(int, int)

@traced(logger, "checkTemperatureLimit", "__init__", "checkTemperatureDewPoint",
        "checkInterlock", "setSourceData", "checkOverheat",
        "ActivateInterlockLimit", "ActivateInterlockOverheat", exclude=True)

class Interlock():
    def __init__(self, pctmhandler: "PCTMHandler",
                 thermalCycle: "ThermalCycling", influx: "Influx") -> None:
        self.temperature_now : float = 0
        self.dewpoint_now : float = 0
        self.vacuum_pressure_now: float = 0
        self.handler = pctmhandler
        self.thermal = thermalCycle
        self.influx = influx
        self.interlock_settings : Dict[Any, Any] = {}
        self.signals = InterlockSignals()
        self.temperature_checkData : bool = False

        self.lv_psu : Any = None
        self.hv_psu : Any = None

        self.influx_delay : float = 1.0
        self.time_before : float = 0.0

        self.interlock_high_limit : bool = False
        self.interlock_low_limit : bool = False
        self.interlock_dewpoint_limit : bool = False
        self.interlock_overheat : bool = False
        self.interlock_absent : bool = False
        self.interlock_coldplate: bool = False

    def init(self) -> None:
        self.temperature_checkData  = False

        """Interlock Status"""
        self.interlock_high_limit: bool  = False
        self.interlock_low_limit: bool = False
        self.interlock_dewpoint_limit: bool  = False
        self.interlock_overheat: bool  = False
        self.interlock_absent: bool = False
        self.interlock_coldplate: bool = False

        """Connect to PSU"""
        if self.interlock_settings[il.LV_ENABLE.name]:
            self.lv_psu = HMP4040()
            try:
                self.lv_psu.connect_psu(mode=self.interlock_settings[il.LV_MODE.name],
                                        #address=self.interlock_settings[il.LV_PYVISA.name], #TODO add back in
                                        port=self.interlock_settings[il.LV_PORT.name])
                logger.info("Connected to R&S HMP4040 in {} mode"
                            .format(self.interlock_settings[il.LV_MODE.name]))
                logger.info(self.lv_psu.get_id())
            except Exception as e:
                raise(e)

        else:
            self.lv_psu = None

        if self.interlock_settings[il.HV_ENABLE.name]:
            self.hv_psu = K2450()
            try:
                self.hv_psu.connect_psu(mode=self.interlock_settings[il.HV_MODE.name],
                                        #address=self.interlock_settings[il.HV_PYVISA.name], #TODO add back in
                                        port=self.interlock_settings[il.HV_PORT.name])
                logger.info("Connected to Keithley 2450 in {} mode"
                            .format(self.interlock_settings[il.HV_MODE.name]))
                logger.info(self.hv_psu.get_id())
            except Exception as e:
                raise(e)

        else:
            self.hv_psu = None

        #open a message queue. commands to the psu comming through this port
        #will be forwarded to the psu. Can be used to perform IV-test etc.
        if self.interlock_settings[il.HV_QUEUE.name]:
            if self.interlock_settings[il.HV_MODE.name] == "zmq":
                raise Exception("HV ZMQ listener queue cannot be started in ZMQ mode")
            self.hv_psu.start_queue(
                self.interlock_settings[il.HV_PORT.name])
        if self.interlock_settings[il.LV_QUEUE.name]:
            if self.interlock_settings[il.LV_MODE.name] == "zmq":
                raise Exception("LV ZMQ listener queue cannot be started in ZMQ mode")
            self.lv_psu.start_queue(
                self.interlock_settings[il.LV_PORT.name])

    def getSettings(self, d : Dict[Any, Any]) -> None:
        self.interlock_settings = d
        self.temperature_checkData = False

    def setSourceData(self, source: SampleSource, datum: Tuple[float, int, float]) -> None:
        if source == SampleSource.NTC_1:
            self.temperature_now = datum[0]
            self.temperature_checkData = True
        elif source == SampleSource.DP_1:
            self.dewpoint_now = datum[0]
        elif source == SampleSource.VACUUM_PRESSURE:
            self.vacuum_pressure_now = datum[0]

    def checkOverheat(self) -> bool:
        if self.temperature_now > self.interlock_settings[il.OVERHEAT.name]:
            return True
        return False

    def checkInterlock(self, source: SampleSource, datum: Tuple[float, int, float]) -> bool:
        self.setSourceData(source, datum)

        if (time.time() - self.time_before > self.influx_delay
            and self.influx.influx_enabled): #Interlock status updates every 1s

            time_now = time.time()
            self.influx.writeInterlockInflux(
                il.ACTIVATED_HIGH, (int(self.interlock_high_limit), time_now))
            self.influx.writeInterlockInflux(
                il.ACTIVATED_LOW, (int(self.interlock_low_limit), time_now))
            self.influx.writeInterlockInflux(
                il.ACTIVATED_DEW, (int(self.interlock_dewpoint_limit), time_now))
            self.influx.writeInterlockInflux(
                il.OVERHEAT, (int(self.interlock_overheat), time_now))
            self.influx.writeInterlockInflux(
                il.ACTIVATED_ABSENT, (int(self.interlock_absent), time_now))
            self.influx.writeInterlockInflux(
                il.ACTIVATED_COLDPLATE, (int(self.interlock_coldplate), time_now))
            self.time_before = time_now

        if self.temperature_checkData & (not self.interlock_overheat):
            if self.checkOverheat():
                self.ActivateInterlockOverheat()
                return True
        return False

    def ActivateInterlockOverheat(self) -> None:
        logger.warning(f"Interlock has activated! NTC_1 Temperature: ",
                       "{round(self.temperature_now, 3)} ",
                       "Reason: Module overheating ")
        self.signals.interlock.emit(3, 4) #Emit interlock activation signal
        self.interlock_overheat = True
        self.ActivatePSUInterlock()
        #turn off PID loop
        self.handler.disablePID()


    def ActivateInterlockLimit(self, reason: int) -> None:
        logger.warning(f"Interlock has activated, reason: {reason}")
        self.signals.interlock.emit(3, reason) #emit interlock activation signal
        HV = True
        LV = True
        if reason==1:
            self.interlock_high_limit = True
        elif reason == 2:
            self.interlock_low_limit = True
            LV = False #dont turn off LV when reason
        elif reason == 3:
            self.interlock_dewpoint_limit = True
        elif reason == 5:
            self.interlock_absent = True
        elif reason == 6:
            self.interlock_coldplate = True

        #todo, more sophisticated decisions on when to turn off
        self.ActivatePSUInterlock(LV, HV) #turn off PSUs


    def ActivatePSUInterlock(self, LV: bool=True, HV: bool=True) -> None:
        """Make non blocking thread for turning off PSUs"""
        logger.warning("Turning off powersupplies")
        hv_thread = threading.Thread(target = self.PSUInterlock, args=(LV, HV))
        hv_thread.setDaemon(True)
        hv_thread.start()

    def PSUInterlock(self, LV: bool=True, HV: bool=True) -> None:
        if self.lv_psu and LV:
            self.lv_psu.set_channel(self.interlock_settings[il.LV_CHANNEL.name])
            self.lv_psu.turn_off()
        if self.hv_psu and HV:
            self.hv_psu.ramp_down()

    def close(self) -> None:
        pass
    #TODO close is not defined yet for PSU class
    #    if self.lv_psu:
    #        self.lv_psu.close()
    #    if self.hv_psu:
    #        self.hv_psu.close()
