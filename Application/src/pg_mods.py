# -*- coding: utf-8 -*-
"""
Created on Sat Oct 24 16:03:40 2020

@author: magne.lauritzen

Subclassed PyQtGraph classes with custom behaviour.
"""
from typing import List, Tuple, Union, Any, Optional, Dict, TYPE_CHECKING
from typing_extensions import TypedDict
import pyqtgraph as pg  # type: ignore
from pyqtgraph.python2_3 import basestring  # type: ignore
from pyqtgraph.graphicsItems.ViewBox import ViewBox  # type: ignore
from PyQt5 import QtCore, QtGui, QtWidgets  # type: ignore
import math
import time
import copy
from time import mktime
from uuid import uuid4, UUID
from datetime import datetime, timedelta
import numpy as np 
import weakref
from autologging import traced  # type: ignore
from .enums import SampleSource
from .loggers import get_logger
from .gui import dialogs

logger = get_logger(__name__)

if TYPE_CHECKING:
    from pytz import BaseTzInfo # type: ignore
    from .pctmhandler import PCTMHandler



class TextItemWithClick(pg.TextItem):
    """ A TextItem subclass that emits sigClicked when it is clicked with the left mouse button """
    sigClicked = QtCore.Signal(object)

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)

    def mouseClickEvent(self, ev: QtCore.QEvent) -> None:
        if ev.button() == QtCore.Qt.LeftButton:
            ev.accept()
            self.sigClicked.emit(self)


class DataCursor(pg.CurvePoint):
    """ Provides an datacursor that displays the value of the nearest point to the cursor """
    sigClicked = QtCore.Signal(object)

    def __init__(self, curve: pg.PlotCurveItem, index: int = 0, pos: float = None, rotate: bool = True, **opts: Any):
        self.fixed = False
        self.cursorText = TextItemWithClick(text='', color='w', border=pg.mkPen('r'), fill=pg.mkBrush('k'),
                                            anchor=(-0.2, 1.5))
        pg.CurvePoint.__init__(self, curve, index=index, pos=pos, rotate=rotate)
        self.cursorText.setParentItem(self)
        self.cursorText.sigClicked.connect(lambda: self.sigClicked.emit(self))
        self.arrow = pg.ArrowItem(**opts)
        self.arrow.setParentItem(self)
        self.arrow.setStyle(angle=-45)

    def fix(self, fix: bool) -> None:
        self.fixed = fix

    def setArrowStyle(self, **opts: Any) -> None:
        self.arrow.setStyle(**opts)

    def event(self, ev: QtCore.QEvent) -> bool:
        if not self.fixed and self.property('index') is not None:
            event_result: bool = super().event(ev)
            index = self.property('index')
            x, y = self.curve().getData()
            try:
                self.cursorText.setText(f"Y: {y[index]:.1f}\nX: {x[index]:.1f}")
                # The following might be used in the future to switch the orientation of the datacursor if it appears to
                # be partly outside the viewbox.
                # viewRect = self.cursorText.textItem.boundingRect()
                # vb = self.getViewBox()
                # brect = vb.mapFromItemToView(self.cursorText, viewRect).boundingRect()

            except AttributeError as e:
                print(e)
            return event_result
        return False


class PlotCurveItemWithDataCursor(pg.PlotCurveItem):
    """ Subclasses PlotCurveItem to add datacursors. """
    sigHovered = QtCore.Signal(object)

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self.setClickable(True)
        self.menu: Optional[QtGui.QMenu] = None
        self.sigHovered.connect(self._displayDataCursor)
        self.dataCursors: List[DataCursor] = []

    def deleteDataCursor(self, cursor: DataCursor) -> None:
        """ Removes and deletes the datacursor given """
        self.parentItem().scene().removeItem(cursor)
        idx = self.dataCursors.index(cursor)
        del self.dataCursors[idx]

    def createNewDataCursor(self) -> None:
        """ Creates a new datacursor """
        try:
            dc = self.dataCursors[-1]
            if not dc.fixed:
                self.parentItem().scene().removeItem(self.dataCursors[-1])
                del self.dataCursors[-1]
                return
        except IndexError:
            pass
        dc = DataCursor(self, rotate=False)
        dc.cursorText.border = pg.mkPen(self.opts['pen'].color(), width=2)
        dc.sigClicked.connect(self.deleteDataCursor)
        self.dataCursors.append(dc)

    def mouseOnPlot(self, ev: QtCore.QEvent) -> bool:
        return bool(self.mouseShape().contains(ev.pos()))

    def mouseClickEvent(self, ev: QtCore.QEvent) -> None:
        """Raises the plot context menu on right click on plot"""
        if not self.clickable:
            return
        if self.mouseOnPlot(ev):
            ev.accept()
            self.sigClicked.emit(self)
            if ev.button() == QtCore.Qt.RightButton:
                self.raiseContextMenu(ev)
            if ev.button() == QtCore.Qt.LeftButton:
                self._fixDataCursor()

    def setCurveSelected(self, selected: bool) -> None:
        """Enables or disables highlighting of the plot"""
        if selected:
            self.org_width = self.opts['pen'].width()
            self.opts['pen'].setWidth(self.org_width + max(1, self.org_width * 0.3))
            self.update()
        else:
            if self.org_width:
                self.opts['pen'].setWidth(self.org_width)
                self.update()

    def hoverEvent(self, ev: QtCore.QEvent) -> None:
        """ Catches hoverevents and modifies them so that only those events that are within the curve's mouseShape are
        propagated to a sigHovered signal. """
        if not hasattr(ev, '_lastScenePos'):
            return
        ev.exit = self.mouseShape().contains(ev.lastPos()) and not self.mouseShape().contains(ev.pos())
        ev.enter = not self.mouseShape().contains(ev.lastPos()) and self.mouseShape().contains(ev.pos())
        if self.mouseShape().contains(ev.pos()) or ev.isEnter() or ev.isExit():
            self.sigHovered.emit(ev)

    def raiseContextMenu(self, ev: QtCore.QEvent) -> bool:
        """Called my mouseClickEvent"""
        menu = self.getContextMenus()
        pos = ev.screenPos()
        menu.popup(QtCore.QPoint(int(pos.x()), int(pos.y())))
        return True

    def getContextMenus(self, ev: QtCore.QEvent = None) -> QtGui.QMenu:
        """Creates the plot context menu if it does not exist. Then returns the plot context menu."""
        if self.menu is None:
            self.menu = QtGui.QMenu()

            # Style dialog
            datacursor = QtGui.QAction("Data cursor", self.menu)
            datacursor.triggered.connect(lambda: self.createNewDataCursor())
            self.menu.addAction(datacursor)
            self.menu.styleDialog = datacursor

            # Make sure plot becomes highlighted if right-clicked
            self.menu.aboutToShow.connect(lambda: self.setCurveSelected(True))
            self.menu.aboutToHide.connect(lambda: self.setCurveSelected(False))
        return self.menu

    def _fixDataCursor(self) -> None:
        """ Fixes the current datacursor in place or releases it. """
        try:
            dc = self.dataCursors[-1]
            dc.fix(not dc.fixed)
        except IndexError:
            pass

    def _displayDataCursor(self, ev: QtCore.QEvent) -> None:
        """ If the datacursor is not fixed in place: Displays or hides the datacursor when the mouse enters or leaves
        the plot and moves it to the point nearest to the cursor."""
        try:
            dc = self.dataCursors[-1]
        except IndexError:
            return
        if dc.fixed:
            return
        if ev.enter:
            dc.setVisible(True)
        if ev.exit:
            dc.setVisible(False)
        xdata, ydata = self.getData()
        x = ev.pos().x()
        idx = np.searchsorted(xdata, x, side='left')
        if idx == len(xdata):
            return
        if idx != 0 and abs(x - xdata[idx - 1]) < abs(x - xdata[idx]):
            idx -= 1
        dc.setIndex(idx)


class PlotDataItemWithDataCursor(pg.PlotDataItem):
    """ Subclasses PlotDataItem to have it use the subclassed PlotCurveItem that provides a datacursor. """
    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self.curve = PlotCurveItemWithDataCursor()
        self.curve.setParentItem(self)
        self.curve.sigClicked.connect(self.curveClicked)
        self.setData(*args, **kwargs)


@traced(logger, "addPlot", "__init__")
class CustomPlotItem(pg.PlotItem):
    """
    A default plot item for displaying timeseries data.
    Each plot is, by default, located in its own ViewBox and has its own AxisItem.
    Additional signals:
        sigRenamed : Emitted when the graph is renamed
    """
    sigRenamed = QtCore.Signal(str)

    vb_dict_Type = TypedDict('vb_dict_Type', {'vb': pg.ViewBox, 'ax': pg.AxisItem})
    x_Type = Union[float, int, np.integer, np.floating]  # Permitted X dataset types

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        self.super_init(*args, **kwargs)
        self.opts = {'zoom_step': 0.5, 'plot_z_spacing': 10}
        self.addCustomXaxis()
        self.datetimeAxis(True)
        self.vb.setRange(xRange=[time.time(), time.time() + 60])
        self.DE = kwargs.get('DE', None)
        self.setMenuEnabled(False, enableViewBoxMenu=None)
        self.menu = None
        self.disableAutoRange()
        # Keep track of all viewboxes and plots in this plotitem.
        self.vbs: List[CustomPlotItem.vb_dict_Type] = []
        # Push viewbox to the background and add custom menu
        self.vb.sigResized.connect(self.updateViews)
        self.vb.setZValue(-1E9)
        self.vb.menu = self.customViewBoxMenu(self)
        self.vb.setWheelEnabled(x=True, y=False)
        self.vb.setDragEnabled(x=True, y=False)
        self.setLabel('bottom', text="Time")
        self.hideAxis('left')
        # Private items
        self._withX = False

    def super_init(self, *args: Any, **kwargs: Any) -> None:
        """Initialize the superclass"""
        viewBox = kwargs.get('viewBox', None)
        if not viewBox:
            kwargs['viewBox'] = moddedViewBox()
        else:
            if not isinstance(viewBox, moddedViewBox):
                raise TypeError("viewBox is not of type moddedViewBox.")
        axisItems = kwargs.get('axisItems', {})
        for orientation, axis in axisItems.items():
            if not isinstance(axis, moddedAxisItem):
                raise TypeError(f"Axis \"{orientation}\" is not of type moddedAxisItem.")
        orientations = ['top', 'bottom', 'right', 'left']
        for o in orientations:
            if o not in axisItems.keys():
                axisItems[o] = moddedAxisItem(o)
        kwargs['axisItems'] = axisItems
        super().__init__(*args, **kwargs)

    def addCustomXaxis(self) -> None:
        """Removes the standard X axis and places a custom X axis in its place"""
        _oldAxis = self.axes['bottom']['item']
        newaxis = moddedAxisItem(orientation='bottom', parent=self)
        pos = self.axes['bottom']['pos']
        self.layout.removeItem(_oldAxis)
        self.layout.addItem(newaxis, *pos)
        newaxis.linkToView(self.getViewBox())
        self.axes['bottom']['item'] = newaxis
        newaxis.setZValue(-1000)
        _oldAxis.hide()

    def updateViews(self, _: pg.ViewBox = None) -> None:
        """Updates all viewboxes contained within this Default1DPlot when the 
        master viewbox sends sigResized signal."""
        for vb_obj in self.vbs:
            vb = vb_obj['vb']
            vb.setGeometry(self.vb.sceneBoundingRect())
            vb.linkedViewChanged(self.vb, vb.XAxis)

    def newAxis(self, name: str) -> 'CustomPlotItem.vb_dict_Type':
        """
        Adds a new viewbox to the CustomPlotItem. 
        Returns a dict with the new viewbox, its Y axis, and empty slots for the 
        plots and metadata.
        """
        vb = moddedViewBox()
        vb.setWheelEnabled(x=True, y=False)
        vb.setDragEnabled(x=True, y=False)
        vb.setMenuEnabled(False)
        vb.setGeometry(self.vb.sceneBoundingRect())
        c_count = self.layout.columnCount()
        # Create and add axis
        ax = moddedAxisItem('right', name=name, parent=self, axis_number=len(self.vbs))
        ax.setLabelPos("side")
        self.layout.addItem(ax, 2, c_count)
        ax.linkToView(vb)
        self.scene().addItem(vb)
        vb.setZValue(len(self.vbs) * self.opts['plot_z_spacing'])
        vb.state['limits']['xLimits'] = self.getViewBox().state['limits']['xLimits']
        vb.setXLink(self)
        vb.disableAutoRange()
        vb.enableAutoRange(axis='y', enable=True)
        ax.setLabel(ax.name)
        vb_obj: CustomPlotItem.vb_dict_Type = {'vb': vb, 'ax': ax}
        self.vbs.append(vb_obj)
        return vb_obj

    def removePlot(self, item: object) -> None:
        """Reimplementation of removePlot which also corrects the Z positions"""
        self.removeItem(item)
        self._fixPlotZs()

    def removeItem(self, item: pg.GraphicsObject) -> None:
        """Overrides the removeItem method in the parent class pg.PlotItem,
        providing added functionality in the form of automatic legend update 
        and the removal of auxilary axes and the plot's viewbox"""
        toRemove = None
        for i, obj in enumerate(self.vbs):
            if item in obj['vb'].addedItems:
                toRemove = obj
                break
        if toRemove is not None:
            self.legend.removePlotItem(item)
            vb = toRemove['vb']
            vb.removeItem(item)
            if len(vb.addedItems) == 0:
                # Remove axis from layout
                self.layout.removeItem(toRemove['ax'])
                # REmove viewbox from scene
                self.scene().removeItem(vb)
                # Remove axis from scene
                self.scene().removeItem(toRemove['ax'])
                del self.vbs[i]
                # Go over each remaining plot Y axis and fix their axis numbers
                for i, vb_obj in enumerate(self.vbs):
                    vb_obj['ax'].setAxisNumber(i)
        else:
            logger.error(f"Could not find the plot item {item} in the list of "
                         "plot view self.vbs.")

    def addPlot(self, plotItem: pg.PlotDataItem, axis: str) -> None:
        try:
            # Find the Viewbox that has the axis we want to place the plot on
            ax_names = {vb_obj['ax'].name: vb_obj for vb_obj in self.vbs}
            vb_obj = ax_names.get(axis, None)
            if vb_obj is None:
                vb_obj = self.newAxis(axis)  # Create viewbox with axis

            vb_obj['vb'].addItem(plotItem)  # Add plot
            plotItem.setParentAx(vb_obj['ax'])  # Link plot to axis
            # #Add to legend
            if self.legend is None:
                self.addLegend()  # type: ignore
            assert (self.legend is not None)  # For mypy
            self.legend.addItem(plotItem, plotItem.opts['name'])
            # self.legend.setParentItem(vb_obj['vb'])
        except:
            logger.exception("An exception occurred when adding a new plot to "
                             "the graph.")
            return None

    def datetimeAxis(self, enable: bool) -> None:
        """Enables or disables the datetimeaxis. Disabling not yet implemented."""
        if enable:
            if not isinstance(self.axes['bottom']['item'], DateAxisItem):
                axis = DateAxisItem(orientation='bottom', parent=self)
                axis.attachToPlotItem(self)
        else:
            # TODO: Implement.
            raise NotImplementedError()

    def nameSelf(self, name: str = "") -> None:
        """Names the CustomPlotItem.
        The viewbox is registered so that the CustomPlotItem can be found from
        other CustomPlotItem items, for example through the "link X axis" feature.
        If name is "", the CustomPlotItem is unregistered.
        
        Keyword arguments:
            name    : The name you wish to give the CustomPlotItem. Default blank
            """
        if self.vb.name != name:
            if name != "":
                self.vb.register(name)
            else:
                self.vb.unregister()
                self.vb.name = None
            self.sigRenamed.emit(self.vb.name)

    def zoom(self, step: float, pos: float = 0.5) -> None:
        """Zooms in or out on the X axis. Zoom amount is given by optional
        keyword argument 'step'. Positive values means outwards zoom, negative 
        value means inwards zoom. The zoom level becomes viewrange*10^step.
        
        Keyword arguments:
            step  : How much to zoom by. Zoom amount is viewrange*10^step.
            pos : Value between 0 and 1 indicating where along the X axis to zoom. Default: 0.5
        """
        if not (pos < 1 and pos > 0):
            raise ValueError("Argument 'pos' must be a value between 0 and "
                             f"1, not {pos}")
        xrange = self.getViewBox().state['viewRange'][0]
        length = xrange[1] - xrange[0]
        center = xrange[0] + pos * length
        zoom_length = length * 10 ** step
        self.setXRange(center - zoom_length * pos, center + zoom_length * (1 - pos))

    def viewAll(self) -> None:
        """Zooms out to view all data present in all plots"""
        minv = []
        maxv = []
        for vb_obj in self.vbs:
            """Disables autorange on x-axis"""
            vb_obj['vb'].state['xRangeMin'][0] = False
            vb_obj['vb'].state['xRangeMin'][1] = False
            linkedview = vb_obj['vb'].linkedView(0)
            if linkedview is not None:
                linkedview.state['xRangeMin'][0] = False
                linkedview.state['xRangeMin'][1] = False
            """Disables autorange on x-axis"""
        
            plots = vb_obj['vb'].addedItems
            for pl in plots:
                minv.append(pl.xMin)
                maxv.append(pl.xMax)
        if len(minv) != 0:
            max_x: np.number = max(maxv)
            min_x: np.number = min(minv)
            self.setXRange(min_x, max_x)

    from pyqtgraph.graphicsItems.ViewBox.ViewBoxMenu import ViewBoxMenu  # type: ignore
    class customViewBoxMenu(ViewBoxMenu):
        """This is a custom implementation of the standard ViewBoxMenu.
        Some elements have been removed since they do not make sense for 
        CustomPlotItem, which is a highly customized subclass of PlotItem.
        "View All" and "Mouse mode" is removed until they can be reimplemented 
        to work with Default1DPlot.
        """

        def __init__(self, parent: pg.PlotItem) -> None:
            super().__init__(parent.vb)
            self.parent = parent
            self.removeActions()
            self.addActions()

        def removeActions(self) -> None:
            toRemove = ["X Axis", "Y Axis", "Mouse Mode", "View All"]
            actions = self.actions()
            for a in actions:
                if a.text() in toRemove:
                    self.removeAction(a)

        def addActions(self) -> None:
            zoomIn = QtGui.QAction("Zoom in", self)
            zoomIn.triggered.connect(self.zoom_in)
            self.addAction(zoomIn)
            self.zoomIn = zoomIn

            zoomOut = QtGui.QAction("Zoom out", self)
            zoomOut.triggered.connect(self.zoom_out)
            self.addAction(zoomOut)
            self.zoomOut = zoomOut

            viewAll = QtGui.QAction("View all", self)
            viewAll.triggered.connect(self.parent.viewAll)
            self.addAction(viewAll)
            self.viewAll = viewAll

        def zoom_in(self) -> None:
            """Disables autorange on x-axis"""
            view = self.parent.getViewBox()
            view.state['xRangeMin'][0] = False
            view.state['xRangeMin'][1] = False
            linkedview = view.linkedView(0)
            if linkedview is not None:
                linkedview.state['xRangeMin'][0] = False
                linkedview.state['xRangeMin'][1] = False
            """Disables autorange on x-axis"""
        
            geo = self.parent.getViewBox().screenGeometry()  # Viewbox geometry
            menu_pos = self.localPos()
            self.parent.zoom(-1, pos=menu_pos.x() / geo.width())

        def zoom_out(self) -> None:
            """Disables autorange on x-axis"""
            view = self.parent.getViewBox()
            view.state['xRangeMin'][0] = False
            view.state['xRangeMin'][1] = False
            linkedview = view.linkedView(0)
            if linkedview is not None:
                linkedview.state['xRangeMin'][0] = False
                linkedview.state['xRangeMin'][1] = False
            """Disables autorange on x-axis"""
        
            geo = self.parent.getViewBox().screenGeometry()  # Viewbox geometry
            menu_pos = self.localPos()
            self.parent.zoom(1, pos=menu_pos.x() / geo.width())

        def localPos(self) -> QtCore.QPoint:
            """Returns the position of the QMenu in the coordinate system of the 
            main viewbox of the PlotItem which activated it.
            """
            vb = self.parent.getViewBox()
            menu_pos = vb.getViewWidget().mapFromGlobal(self.pos())  # Map menu pos to viewbox coords
            assert isinstance(menu_pos, QtCore.QPoint)  # To make mypy happy
            return menu_pos

    def viewBoxFromPlot(self, item: object) -> Optional['moddedViewBox']:
        for i, obj in enumerate(self.vbs):
            if item in obj['vb'].addedItems:
                return obj['vb']  # type: ignore #It thinks this is Any.
        return None

    def addLegend(self, size: int = None, offset: Tuple[int, int] = (-20, 1)) -> pg.LegendItem:
        """
        Create a new LegendItem and anchor it over the internal ViewBox.
        Plots will be automatically displayed in the legend if they
        are created with the 'name' argument.
        """
        self.legend = moddedLegendItem(size, offset)
        self.legend.setParentItem(self.vb)
        return self.legend


class AutoUpdatingPlot(PlotDataItemWithDataCursor):
    """
    AutoUpdatingPlot is a subclass of PlotDataItemWithDataCursor, and adds functionality for
    automatically fetching and displaying data collected by a PCTMHandler, by 
    connecting to its newData signal.
    
    The plot does not update every time a new datapoint is added to it, because
    this could quickly become very slow when you have many plots. An update
    limit, minUpdatePeriod, sets the minimum time it must wait between updates.
    The default is 250ms.
    
    Keyword arguments:
        handler     : The PCTMHandler this plot should connect to.
        source      : The data source this plot should display.
    """

    def __init__(self, handler: 'PCTMHandler', source: SampleSource, *args: Any, **kwargs: Any) -> None:
        self.source = source
        self.h = handler
        self.h.signals.newData.connect(self.updateDataFromHandler)
        self.minUpdatePeriod = 0.250  # Minimum time needed to wait (in seconds) before a plot update can be made
        self.lastUpdate = 0
        super().__init__(*args, **kwargs)
        #self.parent_ax: Optional[pg.AxisItem] = None
        self.parent_ax: Any = None
        
        self.xMin: float = 0.0
        self.xMax: float = 0.0

    def updateDataFromHandler(self, src: SampleSource) -> None:
        """Whenever self.h emits a newData signal, this class is called. If 
        the data source is the one this plot should display, and enough time 
        has passed since last update, it performs an update and displays the
        new data."""
        if src == self.source:
            now = time.time()
            if (now - self.lastUpdate) > self.minUpdatePeriod:
                self._dataRect = None
                self.xClean = self.yClean = None
                self.xDisp = None
                self.yDisp = None
                self.updateItems()  # This will call getData()
                self.informViewBoundsChanged()
                self.sigPlotChanged.emit(self)

    def getData(self) -> Tuple[Any, Any]: #np.ndarray:
        """This method is called on a plot update, and has been significantly 
        changed. Instead of returning the data in the PlotDataItems internal 
        data buffer, it returns the data in the rolling buffers of the 
        PCTMHandler it is subscribed to."""
        
        x = np.array(self.h.data[self.source]['time'])
        y = np.array(self.h.data[self.source]['value'])
        
        if len(x) != len(y):
            #print("X != Y")
            #print(type(self.h.data[self.source]['time']))
            x = np.array(self.h.data[self.source]['time'])
            y = np.array(self.h.data[self.source]['value'])
            
        """------------------------------Code copied from PlotDataItem pyqtgraph ver 0.12.4------------------------------"""
        view = self.getViewBox()
        if view is None:
            view_range = None
        else:
            view_range = view.viewRect() # this is always up-to-date
        if view_range is None:
            view_range = self.viewRect()

        ds = self.opts['downsample']
        if not isinstance(ds, int):
            ds = 1

        if self.opts['autoDownsample']:
            # this option presumes that x-values have uniform spacing
            if view_range is not None and len(x) > 1:
                dx = float(x[-1]-x[0]) / (len(x)-1)
                if dx != 0.0:
                    x0 = (view_range.left()-x[0]) / dx
                    x1 = (view_range.right()-x[0]) / dx
                    width = self.getViewBox().width()
                    if width != 0.0:
                        ds = int(max(1, int((x1-x0) / (width*self.opts['autoDownsampleFactor']))))
                    ## downsampling is expensive; delay until after clipping.
                    
        if len(x) > 1: #Wait for some data
            self.xMin = x[0]
            self.xMax = x[-1]
        
            if self.opts['clipToView']:
                #if view is None or self.parent_ax.linkedView().state['autoRange'][0]:
                if view is None or ((view_range.left() < x[0]) and (view_range.right() > x[-1])) or self.parent_ax.linkedView().state['xRangeMin'][1]: #Check if data is in view
                    pass # no ViewBox to clip to, or view will autoscale to data range.
                else:
                    # clip-to-view always presumes that x-values are in increasing order
                    if view_range is not None and len(x) > 1:
                        # find first in-view value (left edge) and first out-of-view value (right edge)
                        # since we want the curve to go to the edge of the screen, we need to preserve
                        # one down-sampled point on the left and one of the right, so we extend the interval
                        x0 = np.searchsorted(x, view_range.left()) - ds
                        #x0 = fn.clip_scalar(x0, 0, len(x)) # workaround
                        x0 = np.clip(x0, 0, len(x))

                        x1 = np.searchsorted(x, view_range.right()) + ds
                        #x1 = fn.clip_scalar(x1, x0, len(x))
                        x1 = np.clip(x1, 0, len(x))
                        x = x[x0:x1]
                        y = y[x0:x1]

        if ds > 1:
            if self.opts['downsampleMethod'] == 'subsample':
                x = x[::ds]
                y = y[::ds]
            elif self.opts['downsampleMethod'] == 'mean':
                n = len(x) // ds
                stx = ds//2 # start of x-values; try to select a somewhat centered point
                x = x[stx:stx+n*ds:ds] 
                y = y[:n*ds].reshape(n,ds).mean(axis=1)
            elif self.opts['downsampleMethod'] == 'peak':
                n = len(x) // ds
                x1 = np.empty((n,2))
                stx = ds//2 # start of x-values; try to select a somewhat centered point
                x1[:] = x[stx:stx+n*ds:ds,np.newaxis]
                x = x1.reshape(n*2)
                y1 = np.empty((n,2))
                y2 = y[:n*ds].reshape((n, ds))
                y1[:,0] = y2.max(axis=1)
                y1[:,1] = y2.min(axis=1)
                y = y1.reshape(n*2)
        """------------------------------Code copied from PlotDataItem pyqtgraph ver 0.12.4 end---------------------------"""
        
        #ret = (np.array(self.h.data[self.source])['time'], np.array(self.h.data[self.source])['value'])
        ret = (x, y)
        self.xData = x
        return ret

    def setParentAx(self, ax: pg.AxisItem) -> None:
        """Sets which Y axis this plot is linked to."""
        self.parent_ax = ax

    def setMinUpdatePeriod(self, v: float) -> None:
        """Sets the minimum allowed time between plot updates in seconds."""
        self.minUpdatePeriod = v


class moddedLegendItem(pg.LegendItem):
    """This is a subclass of the LegendItem class. The original pyqtgraph
    legend is pretty bad, so this class fixes that.
    
    You normally should not have to instantiate this class, it is handled 
    by the CustomPlotItem.
    """

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.layout.setVerticalSpacing(0)
        self.layout.setHorizontalSpacing(20)
        self.layout.setContentsMargins(3, 3, 3, 3)
        self.setMinimumHeight(0)
        self.style = {'background': lambda: pg.getConfigOption('background'),
                      'foreground': lambda: pg.getConfigOption('foreground')}

    def removePlotItem(self, plotItem: pg.GraphicsObject) -> None:
        """Removes a plotItem from the legend if its name matches the legend text."""
        try:
            name = plotItem.name()
            items = self.items.copy()
            for item in items:
                label = item[1]
                if label.text == name:
                    self.removeItem(label.text)
                    break
        except:
            logger.exception(f"Exception on removing plot by name {name} from legend.")

    def addItem(self, item: pg.LabelItem, name: str) -> None:
        label = moddedLabelItem(name)
        label.contentChanged.connect(self.updateSize)
        if hasattr(item, 'sigRename'):
            item.sigRename.connect(label.updateFromSignal)
        if isinstance(item, pg.graphicsItems.LegendItem.ItemSample):
            sample = item
        else:
            sample = pg.graphicsItems.LegendItem.ItemSample(item)
            sample.setFixedWidth(0)
        column = self.layout.columnCount()
        self.items.append((sample, label))
        self.layout.addItem(sample, 0, column)
        self.layout.addItem(label, 0, column + 1)
        self.updateSize()

    def updateSize(self) -> None:
        if self.size is not None:
            return
        height = 0
        width = 0
        for sample, label in self.items:
            ###This is NOT the correct way this should be done, but I couldn't get
            ###the legend to correctly scale in any other way.
            # Find column where label is located
            col = None
            for n in range(self.layout.columnCount()):
                if self.layout.itemAt(0, n) is label:
                    col = n
            # Remove label from layout
            if col is not None:
                self.layout.removeItem(label)
                label.updateMin()
                label.resizeEvent(None)
                label.updateGeometry()
                # Add the label back
                self.layout.addItem(label, 0, col)
            height = max(height, sample.height(), label.height())
            width += sample.width() + label.itemRect().width()
        self.setGeometry(0, 0, width, height + 6)

    def paint(self, p: Any, *args: Any) -> None:
        p.setPen(pg.mkPen(self.style['foreground']()))
        p.setBrush(pg.mkBrush(self.style['background']()))
        p.drawRect(self.boundingRect())

    def mouseDragEvent(self, ev: QtCore.QEvent) -> None:
        pass


class moddedLabelItem(pg.LabelItem):
    """Subclass of LabelItem that adds functionality for emitting a signal
    when its contents changes (for rescaling of the legend), and the setting
    of new text and updating its style from the global style configuration."""
    contentChanged = QtCore.pyqtSignal()

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

    def setGlobalDefaultStyle(self) -> None:
        self.opts['color'] = pg.getConfigOption('foreground')
        self.setText(self.text)

    def setText(self, str: str) -> None:
        if not hasattr(self, 'text') or self.text != str:
            super().setText(str)
            self.contentChanged.emit()


@traced(logger, "__init__", "setXLink", "setYLink")
class moddedViewBox(pg.ViewBox):
    """
    A modified version of the pyqtgraph ViewBox. Adds functionality which I feel
    is lacking.
    
    dragEnabled and wheelEnabled are like the pre-existing mouseEnabled, but 
    are specifically for mouse wheel and drag events. Both are overridden by 
    mouseEnabled. They are also ignored when the wheel and drag events comes from
    a moddedAxisItem.
    
    _all_views is a weak reference dictionary of all moddedViewBoxes in the app.
    Its keys are viewbox UUID's, values are the viewboxes themselves. This is 
    implemented because the original ViewBox class only keeps weakrefs to named
    views.
    """
    if TYPE_CHECKING:
        all_views_type = weakref.WeakValueDictionary[UUID, 'moddedViewBox']
    _all_views: 'all_views_type' = weakref.WeakValueDictionary({})

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        uuid = kwargs.pop('uuid', uuid4())
        super().__init__(*args, **kwargs)
        self.state.update({'dragEnabled': [True, True],
                           'wheelEnabled': [True, True],
                           'xRangeMin': [False, False, 28800]})
        self.setLayout(QtWidgets.QGraphicsLinearLayout())
        self.uuid: UUID = UUID(int=0)  # Placeholder. Real uuid is set next.
        self._set_uuid(uuid)

    def setWheelEnabled(self, x: bool = None, y: bool = None) -> None:
        """
        Set whether each axis is enabled for wheel interaction. *x*, *y* 
        arguments must be True or False. This allows the user to wheel scale 
        one axis of the view while leaving the other axis unchanged.
        This setting is overridden by mouseEnabled.
        """
        if x is not None:
            self.state['wheelEnabled'][0] = x
        if y is not None:
            self.state['wheelEnabled'][1] = y
        self.sigStateChanged.emit(self)

    def setDragEnabled(self, x: bool = None, y: bool = None) -> None:
        """
        Set whether each axis is enabled for drag interaction. *x*, *y* 
        arguments must be True or False. This allows the user to drag pan one 
        axis of the view while leaving the other axis unchanged.
        This setting is overridden by mouseEnabled.
        """
        if x is not None:
            self.state['dragEnabled'][0] = x
        if y is not None:
            self.state['dragEnabled'][1] = y
        self.sigStateChanged.emit(self)

    def wheelEvent(self, ev: QtCore.QEvent, axis: int = None, src: object = None) -> None:
        """Disables autorange on x-axis"""
        self.state['xRangeMin'][0] = False
        self.state['xRangeMin'][1] = False
        view = self.linkedView(0)
        if view is not None:
            view.state['xRangeMin'][0] = False
            view.state['xRangeMin'][1] = False
        """Disables autorange on x-axis"""
    
        if isinstance(src, moddedAxisItem):
            super().wheelEvent(ev, axis)
        else:
            me = copy.copy(self.state['mouseEnabled'])
            self.state['mouseEnabled'] = [a and b for a, b in zip(me, self.state['wheelEnabled'])]
            super().wheelEvent(ev, axis)
            self.state['mouseEnabled'] = me

    def scaleBy(self, s=None, center=None, x=None, y=None):  # type: ignore
        """Fixes a bug where axes not meant to be scaled still has a scale-call 
        made to it, which resets the autorange option for that axis."""
        s = [sc if sc != 1 else None for sc in s]
        super().scaleBy(center=center, x=s[0], y=s[1])

    def mouseDragEvent(self, ev: QtCore.QEvent, axis: int = None, src: object = None) -> None:
        """Disables autorange on x-axis"""
        self.state['xRangeMin'][0] = False
        self.state['xRangeMin'][1] = False
        view = self.linkedView(0)
        if view is not None:
            view.state['xRangeMin'][0] = False
            view.state['xRangeMin'][1] = False
        """Disables autorange on x-axis"""
        
        if isinstance(src, moddedAxisItem):
            super().mouseDragEvent(ev, axis)
        else:
            me = copy.copy(self.state['mouseEnabled'])
            self.state['mouseEnabled'] = [a and b for a, b in zip(me, self.state['dragEnabled'])]
            super().mouseDragEvent(ev, axis)
            self.state['mouseEnabled'] = me
            

    def setXLink(self, view: Union[basestring, pg.ViewBox, 'moddedViewBox', UUID]) -> None:
        """Link this view's X axis to another view. (see ViewBox.LinkView).
        Reimplented to allow ModdedViewBox UUID's as the argument."""
        v: Union[str, pg.ViewBox, 'moddedViewBox']  # For mypy
        if isinstance(view, UUID):
            v = self._all_views.get(view, None)
            if v is None:
                logger.error("Could not link X Axis to viewbox with UUID "
                             f"{view}. No such UUID in __all_views weakref dict.")
                return
        elif isinstance(view, basestring):
            if view == '':
                v = None
            else:
                v = ViewBox.NamedViews.get(view, view)  ## convert view name to ViewBox if possible
        elif hasattr(view, 'implements') and view.implements('ViewBoxWrapper'):
            v = view.getViewBox()
        elif isinstance(view, pg.ViewBox):
            v = view
        else:
            v = None
        # Try to prevent user from linking viewboxes if it is likely they have
        # different axis types. It is not possible to know this for sure.
        # But a hint is found in the lower x limit which is set to 0 when a
        # datetime axis is attached to a viewbox.
        is_self_likely_datetime = self.state['limits']["xLimits"][0] == 0
        warning_text = None
        link = True
        if isinstance(v, pg.ViewBox):
            if is_self_likely_datetime and v.state['limits']["xLimits"][0] != 0:
                warning_text = ("You are attempting to link a (likely) datetime "
                                "axis to a (likely) non-datetime axis. Are you sure "
                                "you want to proceed?")
            if not is_self_likely_datetime and v.state['limits']["xLimits"][0] == 0:
                warning_text = ("You are attempting to link a (likely) non-datetime "
                                "axis to a (likely) datetime axis. Are you sure "
                                "you want to proceed?")
            if warning_text:
                answer = dialogs.GenericDialog(severity="Warning", text=warning_text,
                                               buttons=[QtWidgets.QMessageBox.Cancel,
                                                        QtWidgets.QMessageBox.Yes])
                if answer != QtWidgets.QMessageBox.Yes:
                    link = False
        if link:
            self.linkView(self.XAxis, v)

    def setYLink(self, view: Union[str, pg.ViewBox, 'moddedViewBox', UUID]) -> None:
        """Link this view's Y axis to another view. (see ViewBox.LinkView).
        Reimplented to allow ModdedViewBox UUID's as the argument."""
        v: Union[str, pg.ViewBox, 'moddedViewBox']  # For mypy
        if isinstance(view, UUID):
            v = self._all_views.get(view, None)
            if v is None:
                logger.error("Could not link Y Axis to viewbox with UUID "
                             f"{view}. No such UUID in __all_views weakref dict.")
                return
        else:
            v = view
        self.linkView(self.YAxis, v)

    def close(self) -> None:
        super().close()
        self._all_views.pop(self.uuid)

    def _set_uuid(self, uuid: UUID) -> None:
        uuid_old = self.uuid
        self.uuid = uuid
        if uuid_old.int != 0:  # If not placeholder
            del self._all_views[uuid_old]
        self._all_views[self.uuid] = self
        
    def childrenBounds(self, frac: Any = None, orthoRange: Any = (None,None), items: Any = None) -> Any: #Change autoRange for x-axis
        range = super().childrenBounds(frac, orthoRange, items)
        if range[0] is not None:   
            if self.state['xRangeMin'][2] < 0 and not self.state['xRangeMin'][1] :
                range[0][0] = range[0][1] + self.state['xRangeMin'][2]
            #elif self.state['xRangeMin'][1]:
            elif self.state['xRangeMin'][2] == 0:
                return range
            else:
                range[0][0] = self.state['xRangeMin'][2]
        return range    
        
    """Modifed functions to stop autorange on x-axis randomly disabling"""
    def linkedXChanged(self) -> None:
        ## called when x range of linked view has changed 
        view = self.linkedView(0)
        self.linkedViewChanged(view, ViewBox.XAxis)
        if view is not None:
            self.enableAutoRange(ViewBox.XAxis, view.state['xRangeMin'][0])
            self.state['xRangeMin'][0] = view.state['xRangeMin'][0]
        else:
            self.enableAutoRange(ViewBox.XAxis, self.state['xRangeMin'][0])
            
    def linkedViewChanged(self, view: Any, axis: Any) -> None:
        if self.linksBlocked or view is None:
            return

        #print self.name, "ViewBox.linkedViewChanged", axis, view.viewRange()[axis]
        vr = view.viewRect()
        vg = view.screenGeometry()
        sg = self.screenGeometry()
        if vg is None or sg is None:
            return

        view.blockLink(True)
        try:
            if axis == ViewBox.XAxis:
                overlap = min(sg.right(), vg.right()) - max(sg.left(), vg.left())
                if overlap < min(vg.width()/3, sg.width()/3):  ## if less than 1/3 of views overlap,
                                                               ## then just replicate the view
                    x1 = vr.left()
                    x2 = vr.right()
                else:  ## views overlap; line them up
                    upp = float(vr.width()) / vg.width()
                    if self.xInverted():
                        x1 = vr.left()   + (sg.right()-vg.right()) * upp
                    else:
                        x1 = vr.left()   + (sg.x()-vg.x()) * upp
                    x2 = x1 + sg.width() * upp
                self.state['xRangeMin'][2] = view.state['xRangeMin'][2]
                self.enableAutoRange(ViewBox.XAxis, False)
                self.setXRange(x1, x2, padding=0)
            else:
                overlap = min(sg.bottom(), vg.bottom()) - max(sg.top(), vg.top())
                if overlap < min(vg.height()/3, sg.height()/3):  ## if less than 1/3 of views overlap,
                                                                 ## then just replicate the view
                    y1 = vr.top()
                    y2 = vr.bottom()
                else:  ## views overlap; line them up
                    upp = float(vr.height()) / vg.height()
                    if self.yInverted():
                        y2 = vr.bottom() + (sg.bottom()-vg.bottom()) * upp
                    else:
                        y2 = vr.bottom() + (sg.top()-vg.top()) * upp
                    y1 = y2 - sg.height() * upp
                self.enableAutoRange(ViewBox.YAxis, False)
                self.setYRange(y1, y2, padding=0)
        finally:
            view.blockLink(False)
          
            
@traced(logger, "__init__", "linkAxis", "unLinkAxis")
class moddedAxisItem(pg.AxisItem):
    """
    Subclass of the AxisItem that adds functionality:
        wheelEvent and mouseDragEvent sends src=self to linkedView
        Left click raises menu
        Can be linked to other axes
        Can be named
        Can be autoscaled
        Y axis can have label on top
    """

    def __init__(self, *args: Any, **kwargs: Any):
        self.parent = kwargs.get("parent", None)
        self.axis_number: Optional[int] = kwargs.pop('axis_number', None)
        self.name: Optional[str] = kwargs.pop("name", None)
        self.setDefaultName()
        super().__init__(*args, **kwargs)
        self.setZValue(-1000)
        self.clickable: bool = True
        self.menu: Optional[QtGui.QMenu] = None
        self.submenu: Optional[QtGui.QMenu] = None
        self.autoScaleGroup: Optional[QtGui.QActionGroup] = None
        self.labelpos = "side"

    def setDefaultName(self) -> None:
        """Sets the name of the axis in case it has not already been set.
        If self.axis_number is defined, the name is based on it. Else, the axis
        is given a UUID-like string. Name collisions are not a problem."""
        if not self.name:
            if self.axis_number:
                self.setName("Axis " + str(self.axis_number))
            else:
                self.setName(str(uuid4())[:4])

    def setName(self, name: str = None) -> None:
        """Sets the name of the axis. If keyword argument name is not given,
        the user is presented with a text input dialog where they may set the name."""
        if not name:
            name, okPressed = QtWidgets.QInputDialog.getText(None,
                                                             "Set axis name",
                                                             "",
                                                             QtWidgets.QLineEdit.Normal,
                                                             self.name)
            if not okPressed:
                name = self.name
        self.name = name

    # generateDrawSpecs is type ignored: It returns a complicated type, and it is
    # only used internally by the parent class itself.
    def generateDrawSpecs(self, p):  # type: ignore
        """
        Inherited from AxisItem.
        Adds functionality to not draw the entire length of the Y axis in 
        case the label has been placed on the top of it."""
        specs = list(super().generateDrawSpecs(p))
        if self.isY() and self.labelpos == "top":
            # Limit axis line length
            axisSpec = list(specs[0])
            axisSpec[1].setY(axisSpec[1].y() + 40)
            specs[0] = tuple(axisSpec)
            # Limit ticks
            tickSpec = list(specs[1])
            textSpec = list(specs[2])
            tmp_tickSpec = []
            tmp_textSpec = []
            for tick, txt in zip(tickSpec, textSpec):
                if tick[1].y() > 40:
                    tmp_tickSpec.append(tick)
                    tmp_textSpec.append(txt)
            specs[1] = tmp_tickSpec
            specs[2] = tmp_textSpec
        return tuple(specs)

    def setAxisNumber(self, number: int) -> None:
        """Sets the number of this axis. Used in the default name of the axis."""
        self.axis_number = number
        self.resizeEvent()

    def setLabelPos(self, pos: str) -> None:
        """Sets the label position to either "top" or "side" if the axis is an
        Y-axis."""
        if not self.isY():
            return
        if pos not in ["top", "side"]:
            raise ValueError("Argument 'pos' must be one of [top, side]")
        if pos == "top":
            self.label.rotate(-180)
            self.setLabel(**{'font-weight': 'bold'})
        if pos == "top":
            self.label.rotate(-90)
        self.labelpos = pos

    def _updateMaxTextSize(self, x):  # type: ignore #It is only used internally by the parent class
        """Fixes a bug in the original implementation of this method which
        prevented the axis from shrinking its width when the tick width shrunk."""
        if self.orientation in ['left', 'right']:
            mx = x
            if mx > self.textWidth or mx < self.textWidth - 10:
                self.textWidth = mx
                if self.style['autoExpandTextSpace'] is True:
                    self._updateWidth()
                    # return True  ## size has changed
        else:
            mx = x
            if mx > self.textHeight or mx < self.textHeight - 10:
                self.textHeight = mx
                if self.style['autoExpandTextSpace'] is True:
                    self._updateHeight()
                    # return True  ## size has changed

    def _updateWidth(self) -> None:
        """Mostly copy-pasted from AxisItem's original _updateWidth method but
        with some custom logic implemented to correctly scale the axis width 
        when the custom label position has been set to top"""
        if not self.isVisible():
            w = 0
        else:
            if self.fixedWidth is None:
                if not self.style['showValues']:
                    w = 0
                elif self.style['autoExpandTextSpace'] is True:
                    w = self.textWidth
                else:
                    w = self.style['tickTextWidth']
                w += self.style['tickTextOffset'][0] if self.style['showValues'] else 0
                w += max(0, self.style['tickLength'])
                try:
                    istop = self.labelpos == "top"
                except AttributeError:
                    istop = False
                if self.label.isVisible() and not istop:
                    w += self.label.boundingRect().height() * 0.8  ## bounding rect is usually an overestimate
            else:
                w = self.fixedWidth

        self.setMaximumWidth(w)
        self.setMinimumWidth(w)
        self.picture = None

    def resizeEvent(self, ev: QtCore.QEvent = None) -> None:
        """
        Adds a bit more functionality to the original resizeEvent to handle the
        case where the label is placed on the top of the axis.
        """
        try:
            if self.labelpos == "top":
                ax_num = self.axis_number if self.axis_number else 0
                nudge = 5 - (ax_num % 2) * 20
                br = self.label.boundingRect()
                p = QtCore.QPointF(0, 0)
                p.setY(-nudge)
                p.setX(int(self.size().width() / 2. - br.width() / 2.))
                self.label.setPos(p)
                self.picture = None
                return
        except AttributeError:
            pass
        super().resizeEvent(ev)
        
    """Convenience functions for deciding whether the axis is a y or x axis"""
    def isY(self) -> bool:
        return self.orientation in ['left', 'right']
        
    def isX(self) -> bool:
        return self.orientation in ['top', 'bottom']

    def wheelEvent(self, ev: QtCore.QEvent) -> None:
        """WheelEvents are ignored and passed on to underlying items if the events
        happens outside the "visible axis", i.e. the part of the axis with ticks,
        tick labels, axis name, etc.
        Otherwise we call the wheelEvent method of the linked view."""
        if (not self.inAxisBox(ev)) or (self.linkedView() is None):
            # Pass wheelevent on to underlying graphics items
            ev.ignore()
            return
        ax = int(self.isY())
        if isinstance(self.linkedView(), moddedViewBox):
            self.linkedView().wheelEvent(ev, axis=ax, src=self)
        else:
            self.linkedView().wheelEvent(ev, axis=ax)
        ev.accept()

    def mouseDragEvent(self, ev: QtCore.QEvent) -> None:
        """mouseDragEvent are ignored and passed on to underlying items if the events
        happens outside the "visible axis", i.e. the part of the axis with ticks,
        tick labels, axis name, etc.
        Otherwise we call the mouseDragEvent method of the linked view."""
       
        if not self.inAxisBox(ev) or self.linkedView() is None:
            ev.ignore()
            return

        ax = int(self.isY())
        if isinstance(self.linkedView(), moddedViewBox):
            self.linkedView().mouseDragEvent(ev, axis=ax, src=self)
        else:
            self.linkedView().mouseDragEvent(ev, axis=ax)
        ev.accept()

    def mouseClickEvent(self, ev: QtCore.QEvent) -> None:
        if not self.clickable or not self.inAxisBox(ev):
            return
        ev.accept()
        if ev.button() == QtCore.Qt.RightButton:
            self.raiseContextMenu(ev)

    def inAxisBox(self, ev: QtCore.QEvent) -> bool:
        """Check if mouse event happened within the actual axis and not somewhere
        else in the linked viewbox"""
        gridbackup = copy.copy(self.grid)  # type: ignore
        self.grid = False
        brect = self.boundingRect()
        c = brect.contains(ev.pos())
        self.grid = gridbackup
        return bool(c)

    def raiseContextMenu(self, ev: QtCore.QEvent) -> bool:
        """Displays the axis context menu"""
        menu = self.getContextMenus()
        menu.grid.setChecked(self.grid)  # Check grid option if grid is visible
        if self.isY():
            ax = 1
            # Check autoscale option if autoscale is enabled
            autoscale_en = self.linkedView().state['autoRange'][1]
            menu.autoscaleY.setChecked(autoscale_en)
            autoscale_en = self.linkedView().state['autoRange'][0]
            if not autoscale_en:
                menu.submenu.autoscaleX_all.setChecked(autoscale_en)
                menu.submenu.autoscaleX_now.setChecked(autoscale_en)
                menu.submenu.autoscaleX_5min.setChecked(autoscale_en)
                menu.submenu.autoscaleX_30min.setChecked(autoscale_en)
                menu.submenu.autoscaleX_1hour.setChecked(autoscale_en)
                menu.submenu.autoscaleX_6hour.setChecked(autoscale_en)
                menu.submenu.autoscaleX_12hour.setChecked(autoscale_en)

        if self.isX():
            ax = 0
        # Display unlink option if the linked viewbox of the axis is linked
        if self.linkedView().linkedView(ax):
            if self.menu:
                self.menu.unLinkAxis.setVisible(True)

        pos = ev.screenPos()
        menu.popup(QtCore.QPoint(pos.x(), pos.y()))
        return True

    def getContextMenus(self, event: QtCore.QEvent = None) -> QtGui.QMenu:
        """Creates the axis context menu if it does not exist.
        Then returns the axis context menu."""
        if self.menu is None:
            self.menu = QtGui.QMenu()

            # Name axis option
            nameAxis = QtGui.QAction("Name axis", self.menu)
            nameAxis.triggered.connect(lambda: self.setName())
            self.menu.addAction(nameAxis)
            self.menu.nameAxis = nameAxis

            # Link to other axes option
            linkAxis = QtGui.QAction("Link to other axis", self.menu)
            linkAxis.triggered.connect(self.linkAxis)
            self.menu.addAction(linkAxis)
            self.menu.linkAxis = linkAxis

            # Unlink from other axes option
            unLinkAxis = QtGui.QAction("Unlink", self.menu)
            unLinkAxis.triggered.connect(lambda: self.unLinkAxis())
            self.menu.addAction(unLinkAxis)
            self.menu.unLinkAxis = unLinkAxis
            self.menu.unLinkAxis.setVisible(False)

            # Show grid option
            grid = QtGui.QAction("Grid", self.menu)
            grid.setCheckable(True)
            grid.toggled.connect(self.toggleGrid)
            self.menu.addAction(grid)
            self.menu.grid = grid

            if self.isY():
                # If the axis is a Y axis we add an autoscale option to the menu
                autoscaleY = QtGui.QAction("AutoscaleY", self.menu)
                autoscaleY.setCheckable(True)
                autoscaleY.toggled.connect(self.autoscaleY)
                self.menu.addAction(autoscaleY)
                self.menu.autoscaleY = autoscaleY
                
                #Add submenu to autoscale
                self.submenu = QtGui.QMenu()
                self.submenu.setTitle("AutoscaleX")
                
                #Add different options for autoscaling x-axis
                self.autoScaleGroup = QtGui.QActionGroup(self)
                
                autoscaleX_all = QtGui.QAction("All data", self.submenu)
                autoscaleX_all.setCheckable(True)
                autoscaleX_all.toggled.connect(lambda enable: self.autoscaleX(enable, 0))
                self.autoScaleGroup.addAction(autoscaleX_all)
                self.submenu.addAction(autoscaleX_all)
                self.submenu.autoscaleX_all = autoscaleX_all
                
                autoscaleX_now = QtGui.QAction("Now to inf", self.submenu)
                autoscaleX_now.setCheckable(True)
                autoscaleX_now.toggled.connect(lambda enable: self.autoscaleX(enable, int(time.time() - 1)))
                self.autoScaleGroup.addAction(autoscaleX_now)
                self.submenu.addAction(autoscaleX_now)
                self.submenu.autoscaleX_now = autoscaleX_now
                
                autoscaleX_5min = QtGui.QAction("Last 5 Min", self.submenu)
                autoscaleX_5min.setCheckable(True)
                autoscaleX_5min.toggled.connect(lambda enable: self.autoscaleX(enable, -300))
                self.autoScaleGroup.addAction(autoscaleX_5min)
                self.submenu.addAction(autoscaleX_5min)
                self.submenu.autoscaleX_5min = autoscaleX_5min
                
                autoscaleX_30min = QtGui.QAction("Last 30 Min", self.submenu)
                autoscaleX_30min.setCheckable(True)
                autoscaleX_30min.toggled.connect(lambda enable: self.autoscaleX(enable, -1800))
                self.autoScaleGroup.addAction(autoscaleX_30min)
                self.submenu.addAction(autoscaleX_30min)
                self.submenu.autoscaleX_30min = autoscaleX_30min

                autoscaleX_1hour = QtGui.QAction("Last 1 Hour", self.submenu)
                autoscaleX_1hour.setCheckable(True)
                autoscaleX_1hour.toggled.connect(lambda enable: self.autoscaleX(enable, -3600))
                self.autoScaleGroup.addAction(autoscaleX_1hour)
                self.submenu.addAction(autoscaleX_1hour)
                self.submenu.autoscaleX_1hour = autoscaleX_1hour
                
                autoscaleX_6hour = QtGui.QAction("Last 6 Hour", self.submenu)
                autoscaleX_6hour.setCheckable(True)
                autoscaleX_6hour.toggled.connect(lambda enable: self.autoscaleX(enable, -21600))
                self.autoScaleGroup.addAction(autoscaleX_6hour)
                self.submenu.addAction(autoscaleX_6hour)
                self.submenu.autoscaleX_6hour = autoscaleX_6hour
                
                autoscaleX_12hour = QtGui.QAction("Last 12 Hour", self.submenu)
                autoscaleX_12hour.setCheckable(True)
                autoscaleX_12hour.toggled.connect(lambda enable: self.autoscaleX(enable, -43200))
                self.autoScaleGroup.addAction(autoscaleX_12hour)
                self.submenu.addAction(autoscaleX_12hour)
                self.submenu.autoscaleX_12hour = autoscaleX_12hour

                self.menu.addMenu(self.submenu)
                self.menu.submenu = self.submenu
              
        return self.menu

    def toggleGrid(self, enable: bool) -> None:
        """Enables or disables the axis grid"""
        if enable:
            self.setGrid(200)
        else:
            self.setGrid(False)

    def linkAxis(self, *args: List[Any], view: ViewBox = None) -> None:
        """Link the viewbox which this axis belong to to another viewbox.
        If keyword argument 'view' is not given, the user is presented with a
        dialog of named viewboxes to choose from.
        
        Keyword arguments:
            view : ViewBox to link to. Optional
        """
        parent_vb = self.linkedView()
        if not parent_vb:
            logger.error("Axis is not linked to a viewbox.")
            return None

        if view is None:
            if self.isY():
                return
            else:
                nv = list(ViewBox.NamedViews.keys())
                try:
                    parent_name = parent_vb.name
                except AttributeError:
                    parent_name = None
                except:
                    logger.exception("Exception occurred when attempting to retrieve the "
                                     "name of the parent graph")
                    return
                if parent_name:
                    # Remove own name from list of named views
                    if parent_name in nv:
                        nv.remove(parent_name)
                if len(nv) == 0:
                    # Inform user to name their graps if there are no other named views
                    msg = QtWidgets.QMessageBox()
                    msg.setIcon(QtWidgets.QMessageBox.Information)
                    msg.setText("There are no other named graphs.")
                    msg.setInformativeText("Name your graphs to link to their axes. "
                                           "Right click a graph to name it.")
                    msg.setWindowTitle("Information")
                    msg.exec()
                else:
                    # Present user with list of named views to choose from
                    ssd = dialogs.SimpleSelectorDialog(items=nv)
                    ssd.exec_()
                    selected = ssd.selected
                    if len(selected) > 0:
                        view = ViewBox.NamedViews[selected[0].text()]
        # If a viewbox has been selected / provided for linking, proceed
        if view:
            success = False
            if self.isY():
                try:
                    parent_vb.setYLink(view)
                    success = True
                except:
                    logger.exception("Error when linking Y axis")
            else:
                try:
                    parent_vb.setXLink(view)
                    success = True
                except:
                    logger.exception("Error when linking X axis")
            if success and self.menu:
                self.menu.unLinkAxis.setVisible(True)

    def unLinkAxis(self) -> None:
        """Unlinks the parent viewbox of this axis from the viewbox it is linked
        to."""
        parent_vb = self.linkedView()
        if not parent_vb:
            logger.error("Axis is not linked to a viewbox.")

        success = False
        if self.isY():
            try:
                parent_vb.setYLink(None)
                success = True
            except:
                logger.exception("Error when unlinking Y axis")
        else:
            try:
                parent_vb.setXLink(None)
                success = True
            except:
                logger.exception("Error when unlinking X axis")
        if success and self.menu:
            self.menu.unLinkAxis.setVisible(False)
            
    """Enable or disable autoscale of the axis"""
    def autoscaleY(self, enable: bool) -> None:
        self.linkedView().enableAutoRange(axis='y', enable=enable)
        
    def autoscaleX(self, enable: bool, xMin: int) -> None:
        if enable:
            if xMin == 0:
                self.linkedView().state['xRangeMin'][1] = True
            else:
                self.linkedView().state['xRangeMin'][1] = False
                self.linkedView().setRange(xRange=[time.time() - 1, time.time() + 1])
            self.linkedView().state['xRangeMin'][2] = xMin
        self.linkedView().state['xRangeMin'][0] = enable
        
        view = self.linkedView().linkedView(0)
        if view is not None:
            view.state['xRangeMin'][0] = enable
            view.state['xRangeMin'][1] = self.linkedView().state['xRangeMin'][1]
        self.linkedView().enableAutoRange(axis='x', enable=enable)

@traced(logger, "__init__", "attachToPlotItem")
class DateAxisItem(moddedAxisItem):
    """
    This is a subclass of moddedAxistItem that adds date-time awareness. 
    It is implemented as an AxisItem that interpretes positions as unix 
    timestamps.
    The labels and the tick positions are dynamically adjusted depending
    on the range.
    It provides an attachToPlotItem() method to add it to a given
    PlotItem
    """

    # Max width in pixels reserved for each label in axis
    _pxLabelWidth = 100

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self._oldAxis = None
        self.setGrid(200)
        self.timezone = datetime.now().astimezone().tzinfo

    def tickValues(self, minVal: float, maxVal: float, size: float
                   ) -> List[Tuple[float, List[float]]]:
        """
        Reimplemented from PlotItem to adjust to the range and to force
        the ticks at "round" positions in the context of time units instead of
        rounding in a decimal base
        """

        maxMajSteps = int(size / self._pxLabelWidth)
        if minVal < 0:
            logger.error(f"minVal {minVal} must be greater than 0.")
            minVal = 0
        if maxVal < 0:
            logger.error(f"maxVal {maxVal} must be greater than 0.")
            maxVal = 0
        if minVal > maxVal:
            logger.error(f"maxVal ({maxVal}) cannot be less than minVal ({minVal})")
            maxVal = minVal + 1

        try:
            dt1 = datetime.fromtimestamp(minVal)
            dt2 = datetime.fromtimestamp(maxVal)

            dx = maxVal - minVal
            majticks = []

            if dx > 63072001:  # 3600s*24*(365+366) = 2 years (count leap year)
                d = timedelta(days=366)
                for y in range(dt1.year + 1, dt2.year):
                    dt = datetime(year=y, month=1, day=1)
                    majticks.append(mktime(dt.timetuple()))

            elif dx > 5270400:  # 3600s*24*61 = 61 days
                d = timedelta(days=31)
                dt = dt1.replace(day=1, hour=0, minute=0,
                                 second=0, microsecond=0) + d
                while dt < dt2:
                    # make sure that we are on day 1 (even if always sum 31 days)
                    dt = dt.replace(day=1)
                    majticks.append(mktime(dt.timetuple()))
                    dt += d

            elif dx > 172800:  # 3600s24*2 = 2 days
                d = timedelta(days=1)
                dt = dt1.replace(hour=0, minute=0, second=0, microsecond=0) + d
                while dt < dt2:
                    majticks.append(mktime(dt.timetuple()))
                    dt += d

            elif dx > 7200:  # 3600s*2 = 2hours
                d = timedelta(hours=1)
                dt = dt1.replace(minute=0, second=0, microsecond=0) + d
                while dt < dt2:
                    majticks.append(mktime(dt.timetuple()))
                    dt += d

            elif dx > 1200:  # 60s*20 = 20 minutes
                d = timedelta(minutes=10)
                dt = dt1.replace(minute=(dt1.minute // 10) * 10,
                                 second=0, microsecond=0) + d
                while dt < dt2:
                    majticks.append(mktime(dt.timetuple()))
                    dt += d

            elif dx > 120:  # 60s*2 = 2 minutes
                d = timedelta(minutes=1)
                dt = dt1.replace(second=0, microsecond=0) + d
                while dt < dt2:
                    majticks.append(mktime(dt.timetuple()))
                    dt += d

            elif dx > 20:  # 20s
                d = timedelta(seconds=10)
                dt = dt1.replace(second=(dt1.second // 10) * 10, microsecond=0) + d
                while dt < dt2:
                    majticks.append(mktime(dt.timetuple()))
                    dt += d

            elif dx > 2:  # 2s
                d = timedelta(seconds=1)
                majticks = list(range(int(minVal), int(maxVal)))

            else:  # <2s , use standard implementation from parent
                # Typechecking is ignored due to pyqtgraph not being statically typed.
                return super().tickValues(minVal, maxVal, size)  # type: ignore

            L = len(majticks)
            if L > maxMajSteps:
                try:
                    majticks = majticks[::int(np.ceil(float(L) / maxMajSteps))]
                except ZeroDivisionError:
                    # An error occurs for no reason here.
                    pass

            return [(d.total_seconds(), majticks)]
        except:
            logger.exception("Exception occurred while calculating datetime "
                             "X-axis tick values.")
            return []

    def tickStrings(self, values: List[Union[float, int]],
                    scale: Union[float, int], spacing: Union[float, int]) -> List[str]:
        """Reimplemented from PlotItem to adjust to the range"""
        ret = []
        if not values:
            return []

        if spacing >= 31622400:  # 366 days
            fmt = "%Y"

        elif spacing >= 2678400:  # 31 days
            fmt = "%Y %b"

        elif spacing >= 86400:  # = 1 day
            fmt = "%Y %d/%b"

        elif spacing >= 3600:  # 1 h
            fmt = "%d/%b-%Hh"

        elif spacing >= 60:  # 1 m
            fmt = "%d/%b-%H:%M"

        elif spacing >= 1:  # 1s
            fmt = "%H:%M:%S"

        else:
            fmt = "%H:%M:%S.%f"
        for x in values:
            try:
                t = datetime.fromtimestamp(x, tz=self.timezone)
                split = t.strftime(fmt).split('.')
                if len(split) == 2:  # In this case there is microsecond info
                    ts = split[0]
                    microseconds = split[1]
                    # Figure out how many decimals to keep
                    n_decimals = -1 * math.floor(math.log(spacing, 10))
                    ts += f".{microseconds[:n_decimals]}"
                else:
                    ts = split[0]
                ret.append(ts)
            except ValueError:  # Windows can't handle dates before 1970
                ret.append('')
            except:
                logger.exception("Exception occurred while formatting datetime "
                                 "X-axis tick strings.")
        return ret

    def attachToPlotItem(self, plotItem: pg.PlotItem) -> None:
        """Add this axis to the given PlotItem.
        :param plotItem: (PlotItem)
        """
        self._oldAxis = plotItem.axes[self.orientation]['item']
        if self._oldAxis is not None:
            # Mypy incorrectly states this is unreachable
            plotItem.layout.removeItem(self._oldAxis)  # type: ignore
            self._oldAxis.hide()
        pos = plotItem.axes[self.orientation]['pos']
        self.setParentItem(plotItem)
        plotItem.layout.addItem(self, *pos)
        vb = plotItem.getViewBox()
        vb.setLimits(xMin=0)
        self.linkToView(vb)
        plotItem.axes[self.orientation]['item'] = self
