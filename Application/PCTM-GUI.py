# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 17:13:54 2020

@author: Magne Lauritzen

Entrypoint for the PCTM-GUI application.
"""

# main.py
from termcolor import colored 
import colorama #type: ignore #Needed for Window terminal colors
colorama.init()
import pyqtgraph as pg #type: ignore
from PyQt5.QtCore import pyqtRemoveInputHook #type: ignore
pyqtRemoveInputHook() #Prevents QT spamming terminal on user input
from src.pctm_app import PctmApp
from src import loggers

logger = loggers.get_logger(__name__)

def main()->None:
    try:
        qtapp = pg.mkQApp() #Create PyQT app
        with PctmApp(): #Instantiate PctmApp
            print(colored("PCTM-GUI initialization succesful.",'green', attrs=['bold']))
            qtapp.exec() #Execute QT event loop
    except:
        logger.exception("An exception occurred in the PCTM-GUI application.")

if __name__ == "__main__":
    main()