SET file=pctm.py
cd ..
mypy --disallow-untyped-defs --disallow-incomplete-defs^
 --warn-redundant-casts --warn-unused-ignores --warn-return-any^
 --warn-unreachable --pretty --config-file=typechecking\mypy.ini^
 src/%mod%
cd typechecking
pause