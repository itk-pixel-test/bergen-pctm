**Visit the [Mattermost channel](https://mattermost.web.cern.ch/pctm/channels/town-square) if you have questions**

**Check the [Wiki](https://gitlab.cern.ch/itk-pixel-test/bergen-pctm/-/wikis/home) for more details**

**Visit the [Mattermost channel](https://mattermost.web.cern.ch/pctm/channels/town-square) if you have questions**

# Bergen-PCTM

The Bergen Peltier Controller and Test Monitor (PCTM) is a system to be
used for ITK Pixel QA/QC testing. It consists of a PCB with an Arduino and
a accompanying GUI application. It exists because I was annoyed with the
lack of a unified solution for test monitoring, and because proper Peltier
controllers are prohibitively expensive.

The PCTM consists of a PCB and a Python GUI. The PCTM monitors several parameters
during testing, and can turn nearly any lab PSU into a cooling **and** heating Peltier PSU.


## Monitoring

The PCTM digitizes many of the parameters that matters during testing of pixel
modules, such as:
- Module NTC temperature
- Enclosure humidity
- Purge gas flow

The PTCTM is designed to sit between the sensors and the HW Interlock, and does
not affect the performance or operation of the HW interlock. In fact, it is meant
to receive the module temperature from the HW Interlock over I2C! The logged parameters
are plotted in realtime in the GUI and saved to an HDF5 file. Csv files begone!
It is expected that the PCTM will also be able to log directly to Influx-DB.

## Peltier controller

The PCTM can turn nearly any laboratory PSU into a Peltier PSU, given that it can be
controlled either through RS232 or with a ratiometric 5V input. The PCTM runs a
PID loop that calculates the correct current or voltage to provide to the Peltier
elements, and sets the PSU to match it.

Switching from cooling to heating requires inverting the PSU polarity, but it is
rare for lab PSUs to have this feature. To get around this problem,
the PCTM has a polarity inverting circuit consisting of a relay and some
carefully selected components to dampen the resulting inductance spikes when the
current switches direction.

## Install
Using the PCTM requires a bit of software to get started. First you'll need the
software for programming it, and then you'll need some software for
controlling it and seeing the data on the PC.

But first things first: Open a command line terminal and clone the repository
and checkout the PCTM V0.2 branch.
```
git clone https://gitlab.cern.ch/itk-pixel-test/bergen-pctm.git
cd bergen-pctm
git checkout PCTM_V0.2
```

To use the software interlocks. Check out the psu-remote repository:
```
cd Application/src/
git clone ssh://git@gitlab.cern.ch:7999/shellesu/psu_remote.git
```


### Arduino related software
You must first install the Arduino IDE if you haven't already done so : https://www.arduino.cc/en/software

Then, unzip and copy all the libraries from `bergen-pctm\Firmware\Libraries` to your local Arduino `libraries`
folder. This folder is found in your Sketchbook folder. You can check the location of your Sketchbook folder
in the Arduino IDE by going to `File->Preferences`.

Then, install all the libraries in `bergen-pctm\Firmware\REQUIREMENTS.txt` by using the Arduino IDE library
manager, which is found at `Sketch->Include Library->Manage Libraries...`. Make sure you install the correct
libraries by verifying the author against the REQUIREMENTS.txt file.

Then, install "Arduino megaAVR boards" from the Board Manager found at `Tools->Board->Boards Manager`.

Make sure that `Tools->Registers emulation` is set to `"None(ATMEGA4809)"`.

### Readout related software
This project uses Anaconda for dependency control. If you have not already installed Anaconda, do
so from : https://www.anaconda.com/products/individual

Then, open the Anaconda Prompt and create the `pctm` environment from environment.yml:
```
cd bergen-pctm\Application\INSTALL
conda env create -f environment.yml
```

If you are instead updating a pre-existing PCTM environment, run the following command:
```
conda activate pctm
conda env update --file environment.yml
```

You are now ready to go! Go to the project Wiki (book symbol in side bar) to learn how to use the PCTM.
