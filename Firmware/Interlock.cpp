#include "Interlock.h"

Interlock::Interlock(Sampler& sampler_, SerialTransfer& uart_txrx_, Peltiercontroller& pc_) : sampler(sampler_), uart_txrx(uart_txrx_), pc(pc_)
{
}

void Interlock::init()
{
  checkInterlockTimer = interlockTimer.every(500, _checkInterlockRoutine, this);
  _loadCalibrations();
}

void Interlock::setSettings(interlock_settings cmd, float value)
{
  switch(cmd)
  {
    case HIGH_LIMIT:
      setTemperatureLimits(value, _temperature_limit_low);
      break;

    case LOW_LIMIT:
      setTemperatureLimits(_temperature_limit_high, value);

    case DEWPOINT_LIMIT:
      setDewpointLimit(value);

    case COLDPLATE:
      setColdplateLimit(value);
  }
}

void Interlock::activateInterlock(bool enable)
{
  _interlockEnable = enable;
  _activated_high = false;
  _activated_low = false;
  _activated_dew = false;
  _activated_absent = false;
  _activated_coldplate = false;
}

bool Interlock::checkTemperatureLimits()
{
  float temperature_dewpoint_diff = sampler.coldstack.vacuumchuck_t.val - sampler.environment.humidity_1.dp.val;

  if (sampler.module_temperature.ntc_1.val > _temperature_limit_high)
  {
    if (!_activated_high)
    {
      _tx_setting(ACTIVATED_HIGH, 1);
      _activated_high = true;
    }
  }

  if (sampler.module_temperature.ntc_1.val < _temperature_limit_low)
  {
    if (!_activated_low)
    {
      _tx_setting(ACTIVATED_LOW, 1);
      _activated_low = true;
    }
  }

  if (temperature_dewpoint_diff <= _dewpoint_limit)
  {
    if (!_activated_dew)
    {
      _tx_setting(ACTIVATED_DEW, 1);
      _activated_dew = true;
    }
  }

  if (isnan(sampler.module_temperature.ntc_1.val) || isnan(sampler.environment.humidity_1.dp.val))
  {
    if (!_activated_absent)
    {
      _tx_setting(ACTIVATED_ABSENT, 1);
      _activated_absent = true;
    }
  }

  if (sampler.coldstack.coldplate_t.val > _coldplate_limit)
  {
    if (!_activated_coldplate)
    {
      _tx_setting(ACTIVATED_COLDPLATE, 1);
      _activated_coldplate = true;
    }
  }

  if (_activated_high || _activated_low ||_activated_dew || _activated_absent
      || _activated_coldplate)
    return true;
  else
    return false;

}

void Interlock::setTemperatureLimits(float high, float low)
{
  _temperature_limit_high = high;
  _temperature_limit_low = low;
  EEPROM.put(ILHIGH_EEPROM_ADDR, _temperature_limit_high);
  EEPROM.put(ILLOW_EEPROM_ADDR, _temperature_limit_low);
}

void Interlock::setDewpointLimit(float limit)
{
  _dewpoint_limit = limit;
  EEPROM.put(ILDEW_EEPROM_ADDR, _dewpoint_limit);
}

void Interlock::setColdplateLimit(float limit)
{
  _coldplate_limit = limit;
  EEPROM.put(ILCOLD_EEPROM_ADDR, _coldplate_limit);
}

void Interlock::checkInterlock()
{
  if (_interlockEnable)
  {
    if (checkTemperatureLimits())
      activateInterlockTemperature();
  }
}

void Interlock::activateInterlockTemperature()
{
  pc.disable();
}


void Interlock::_loadCalibrations()
{
  // Interlock settings
  float high_limit;
  float low_limit;
  float dew_limit;
  float coldplate_limit;

  EEPROM.get(ILHIGH_EEPROM_ADDR, high_limit);
  EEPROM.get(ILLOW_EEPROM_ADDR, low_limit);
  EEPROM.get(ILDEW_EEPROM_ADDR, dew_limit);
  EEPROM.get(ILCOLD_EEPROM_ADDR, coldplate_limit);

  if (isnan(high_limit)) {
    high_limit = 100.0; // Replace unitinialized value with default
  }
  if (isnan(low_limit)) {
    low_limit = -100.0; // Replace unitinialized value with default
  }
  if (isnan(dew_limit)) {
    dew_limit = 0.0; // Replace unitinialized value with default
  }
  if (isnan(coldplate_limit)) {
    coldplate_limit = 50.0; // Replace unitinialized value with default
  }

  setTemperatureLimits(high_limit, low_limit);
  setDewpointLimit(dew_limit);
  setColdplateLimit(coldplate_limit);
}

void Interlock::tx_settings()
{
  _tx_setting(HIGH_LIMIT, _temperature_limit_high);
  _tx_setting(LOW_LIMIT, _temperature_limit_low);
  _tx_setting(DEWPOINT_LIMIT, _dewpoint_limit);
  _tx_setting(COLDPLATE, _coldplate_limit);
  _tx_setting(ENABLE, (int)_interlockEnable);
}

void Interlock::_tx_setting(interlock_settings r, float val)
{
  uint16_t sendSize = 0;
  sendSize = uart_txrx.txObj(r, sendSize);
  sendSize = uart_txrx.txObj(val, sendSize);
  uart_txrx.sendData(sendSize, INTERLOCK_INFO);
}

//Timed routines
bool Interlock::_checkInterlockRoutine(Interlock& interlock) {
  interlock.checkInterlock();
  return true;
}
