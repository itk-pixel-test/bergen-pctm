/*
 SegmentDisplay.h
 Sets the symbol displayed by the segmented display. 
 */

#ifndef SegmentDisplay_h
#define SegmentDisplay_h
#include "constants.h"
#include <Adafruit_MCP23017.h>

class SegmentDisplay
{
  public:
    //Methods
    SegmentDisplay(Adafruit_MCP23017& mcp_);
    void setSymbol(uint8_t symbol);
    void clear();
    void init();
  
  private:
    //Private Methods
    void _write(uint8_t b);
    byte _decodeByte(uint8_t b);
    //Variables and objects
    Adafruit_MCP23017& mcp;
};
#endif
