#include "Peltiercontroller.h"

Peltiercontroller::Peltiercontroller(Sampler& sampler_, SerialTransfer& uart_txrx_, Adafruit_MCP23017& mcp_) : sampler(sampler_), uart_txrx(uart_txrx_), mcp(mcp_)
{
  TECPID = new PID(&sampler.module_temperature.ntc_1_dval, &_TEC_Output, &_TEC_Setpoint, _Kp, _Ki, _Kd, _PON, REVERSE);
  //TECPID->SetOutputLimits(-10, 10);

  setPSUsetpoint(0.0);
  PSUPID = new PID(&sampler.coldstack.peltier_i_dval, &_PSU_Output, &_PSU_Setpoint, _PSU_Kp, _PSU_Ki, _PSU_Kd, DIRECT);
  PSUPID->SetOutputLimits(-255, 255);
}

void Peltiercontroller::setSettings(pid_settings cmd, float value)
{
  switch (cmd)
  {
    case KP_TEC:
      setKp(value, PID_TEC);
      break;
    case KI_TEC:
      setKi(value, PID_TEC);
      break;
    case KD_TEC:
      setKd(value, PID_TEC);
      break;
    case PON_TEC:
      value = ((int)value == 1) ? 0 : 1; //Proportional on measurement if _PON = 0
      setPON((int)value, PID_TEC);
      break;
    case SETPOINT:
      controlTECsetpoint(cmd, value);
      break;
    case RAMP:
      controlTECsetpoint(cmd, value);
      break;
    case SETRAMP:
      setTECsetpointRamp(value);
      break;
    case ILIMIT:
      setILimit(value);
      break;
    case VLIMIT:
      setVLimit(value);
      break;
    case CC:
      setMode(cmd, value);
      break;
    case CV:
      setMode(cmd, value);
      break;
    case PID_TEC:
      setMode(cmd, value);
      break;
    case PSU_FREQ:
      setPID_freq(value, PID_PSU);
      break;
    case TEC_FREQ:
      setPID_freq(value, PID_TEC);
      break;
    case KP_PSU:
      setKp(value, PID_PSU);
      break;
    case KI_PSU:
      setKi(value, PID_PSU);
      break;
    case KD_PSU:
      setKd(value, PID_PSU);
      break;
    case CCTRL:
      setMode(cmd, value);
      break;
    case PID_PSU:
      setMode(cmd, value);
      break;
    case PSU_MAX:
      setPSUmax(value);
      break;
    case COMM_MODE:
      // TODO: Switch between Ratiometric (val: 0) and RS232 (val: 1) PSU control mode by setting _psu_comm.
      break;
  }
}

void Peltiercontroller::analogCompute()
{
  if (TECPID->Compute())
  {
    setPSUsetpoint(_TEC_Output);
  }
}

void Peltiercontroller::currentCompute()
{
  if (PSUPID->Compute())
  {
    setPSURatiometricOutput((int)_PSU_Output);
  }
}

void Peltiercontroller::controlTECsetpoint(pid_settings cmd, float value)
{
  switch (cmd)
  {
    case SETPOINT:
      pidtimer.cancel(routines.ramp);
      setTECsetpoint(value);
      break;

    case RAMP:
      _TEC_Setpoint_start = sampler.module_temperature.ntc_1_dval;
      _TEC_Setpoint_end = value;
      _TEC_Setpoint_difference = _TEC_Setpoint_end - _TEC_Setpoint_start;
      _TEC_total_steps = (((_TEC_Setpoint_difference) / _TEC_ramp) * 60 * 1000) / 500;
      _TEC_steps = (_TEC_Setpoint_difference) / abs(_TEC_total_steps);
      setTECsetpoint(_TEC_Setpoint_start);
      pidtimer.cancel(routines.ramp);
      routines.ramp = pidtimer.every(500, _rampRoutine, this);
      break;
  }
}

void Peltiercontroller::rampCompute()
{
  if (_enabled == true)
  {
    double setpoint = _TEC_Setpoint + _TEC_steps;

    if (_TEC_Setpoint_difference <= 0)
    {
      setpoint = (setpoint <= _TEC_Setpoint_end) ? _TEC_Setpoint_end : setpoint;
    }
    else
    {
      setpoint = (setpoint >= _TEC_Setpoint_end) ? _TEC_Setpoint_end : setpoint;
    }

    setTECsetpoint(setpoint);
  }
}

void Peltiercontroller::setTECsetpointRamp(double ramp)
{
  _TEC_ramp = ramp;
}

void Peltiercontroller::setTECsetpoint(double setpoint)
{
  _TEC_Setpoint = setpoint;
  sampler.pid_info.TEC_setpoint = _TEC_Setpoint;
}

void Peltiercontroller::setPSUsetpoint(double setpoint)
{
  _PSU_Setpoint = setpoint;
  sampler.pid_info.PSU_setpoint = _PSU_Setpoint;
}

void Peltiercontroller::setPSUAbsoluteOutput(double value)
// Sets the PSU output value. Unit: Amperes
{
  if (_psu_comm == 0)
  {
    if (_psu_max == 0) {
      return; // Cannot set an absolute output with ratiometric control if the PSU max current has not been given
    }
    setPSURatiometricOutput((int)(255 * value / _psu_max));
  } else {
    // Here we must call a function that sets the PSU output via RS232.
  }
}

void Peltiercontroller::setPSURatiometricOutput(int value)
// Sets the ratiometric control output value. Unit: PWM counts (-255 to +255)
{
  value = min(max(value, -255), 255); // Enforce valid value
  if (value < 0)
  {
    mcp.digitalWrite(POLARITY, HIGH);
  }

  else if (value >= 0)
  {
    mcp.digitalWrite(POLARITY, LOW);
  }

  analogWrite(PSU_RATIOMETRIC, abs(value));
  sampler.pid_info.PSU_control_pwm = value;
  sampler.pid_info.PSU_control = (float)(value) * _psu_max / 255;
}

void Peltiercontroller::setPSUmax(double psu_max)
{
  _psu_max = psu_max;
  EEPROM.put(PSUMAX_EEPROM_ADDR, psu_max);
  EEPROM.put(PSUMAX_FLAG_EEPROM_ADDR, 0);
}

void Peltiercontroller::disable()
// Disable the PID loop. The PCTM boots to disabled mode to prevent accidents.
{
  delete PSUPID;
  setPSUsetpoint(0.0);
  _PSU_Output = 0.0;
  PSUPID = new PID(&sampler.coldstack.peltier_i_dval, &_PSU_Output, &_PSU_Setpoint, _PSU_Kp, _PSU_Ki, _PSU_Kd, DIRECT);
  PSUPID->SetOutputLimits(-255, 255);
  PSUPID->SetMode(MANUAL);
  setPSUAbsoluteOutput(0);

  //Reset PID loop values
  delete TECPID;
  _TEC_Output = 0.0;
  TECPID = new PID(&sampler.module_temperature.ntc_1_dval, &_TEC_Output, &_TEC_Setpoint, _Kp, _Ki, _Kd, _PON, REVERSE);
  TECPID->SetMode(MANUAL);
  _loadCalibrations();
  

  _enabled = false;
}

void Peltiercontroller::enable()
// Enable the PID loop.
{
  _applyPreferredSettings();
  _enabled = true;
}

bool Peltiercontroller::checkEnabled()
{
  return _enabled;
}

void Peltiercontroller::constantMode(boolean state)
// Set it to constant current mode
{
  if (state)
  {
    _preferredSettings.TECPID = MANUAL;
    _preferredSettings.PSUsetpoint = _PSU_Setpoint;
  }
  else
  {
    _preferredSettings.TECPID = AUTOMATIC;
  }

  if (_enabled)
  {
    _applyPreferredSettings();
  }

}

void Peltiercontroller::setMode(pid_settings cmd, float value)
{
  switch (cmd)
  {
    case CC:
      _preferredSettings.TECPID = MANUAL;
      _preferredSettings.PSUsetpoint = value;
      break;

    case CV:
      /* // The PCTM cannot yet measure the peltier voltage

      */
      break;

    case CCTRL:
      _preferredSettings.PSUPID = MANUAL;
      _preferredSettings.PSUabsout = value;
      break;

    case PID_TEC:
      _preferredSettings.TECPID = AUTOMATIC;
      break;

    case PID_PSU:
      _preferredSettings.PSUPID = AUTOMATIC;
      break;
  }

  if (_enabled)
  {
    _applyPreferredSettings();
  }
}

void Peltiercontroller::_applyPreferredSettings()
{
  TECPID->SetMode(_preferredSettings.TECPID);
  if (_preferredSettings.TECPID == MANUAL) {
    setPSUsetpoint(_preferredSettings.PSUsetpoint);
  }
  PSUPID->SetMode(_preferredSettings.PSUPID);
  if (_preferredSettings.PSUPID == MANUAL) {
    setPSUAbsoluteOutput(_preferredSettings.PSUabsout);
  }

}

void Peltiercontroller::setPID_freq(double pid_freq, pid_settings which_pid)
{
  int sampletime;
  switch (which_pid)
  {
    case PID_TEC:
      _TEC_freq = (pid_freq == 0 or pid_freq < 1) ? 1 : pid_freq;
      EEPROM.put(TECFREQ_EEPROM_ADDR, _TEC_freq);
      sampletime = (1.0 / _TEC_freq) * 1000.0;
      TECPID->SetSampleTime(sampletime);
      break;

    case PID_PSU:
      _PSU_freq = (pid_freq == 0 or pid_freq < 1) ? 1 : pid_freq;
      EEPROM.put(PSUFREQ_EEPROM_ADDR, _PSU_freq);
      sampletime = (1.0 / _PSU_freq) * 1000.0;
      PSUPID->SetSampleTime(sampletime);
      break;
  }
}

void Peltiercontroller::setILimit(double i_limit)
{
  _i_limit = i_limit;
  EEPROM.put(TECIMAX_EEPROM_ADDR, _i_limit);
  TECPID->SetOutputLimits(-_i_limit, _i_limit);
}

void Peltiercontroller::setVLimit(double v_limit)
{
  _v_limit = v_limit;
}

void Peltiercontroller::setKp(double new_Kp, pid_settings which_pid)
{
  switch (which_pid)
  {
    case PID_TEC:
      _Kp = new_Kp;
      EEPROM.put(TECKP_EEPROM_ADDR, _Kp);
      TECPID->SetTunings(_Kp, _Ki, _Kd);
      break;
    case PID_PSU:
      _PSU_Kp = new_Kp;
      EEPROM.put(PSUKP_EEPROM_ADDR, _PSU_Kp);
      PSUPID->SetTunings(_PSU_Kp, _PSU_Ki, _PSU_Kd);
      break;
  }
}

void Peltiercontroller::setKi(double new_Ki, pid_settings which_pid)
{
  switch (which_pid)
  {
    case PID_TEC:
      _Ki = new_Ki;
      EEPROM.put(TECKI_EEPROM_ADDR, _Ki);
      TECPID->SetTunings(_Kp, _Ki, _Kd);
      break;
    case PID_PSU:
      _PSU_Ki = new_Ki;
      EEPROM.put(PSUKI_EEPROM_ADDR, _PSU_Ki);
      PSUPID->SetTunings(_PSU_Kp, _PSU_Ki, _PSU_Kd);
      break;
  }
}

void Peltiercontroller::setKd(double new_Kd, pid_settings which_pid)
{
  switch (which_pid)
  {
    case PID_TEC:
      _Kd = new_Kd;
      EEPROM.put(TECKD_EEPROM_ADDR, _Kd);
      TECPID->SetTunings(_Kp, _Ki, _Kd);
      break;
    case PID_PSU:
      _PSU_Kd = new_Kd;
      EEPROM.put(PSUKD_EEPROM_ADDR, _PSU_Kd);
      PSUPID->SetTunings(_PSU_Kp, _PSU_Ki, _PSU_Kd);
      break;
  }
}

void Peltiercontroller::setPON(int PON, pid_settings which_pid)
{
  switch (which_pid)
  {
    case PID_TEC:
      _PON = PON;
      EEPROM.put(TECPON_EEPROM_ADDR, _PON);
      TECPID->SetTunings(_Kp, _Ki, _Kd, _PON);
      break;
//    case PID_PSU:
//      _PSU_Kd = new_Kd;
//      EEPROM.put(PSUKD_EEPROM_ADDR, _PSU_Kd);
//      PSUPID->SetTunings(_PSU_Kp, _PSU_Ki, _PSU_Kd);
//      break;
  }
}

void Peltiercontroller::init()
{
  mcp.pinMode(POLARITY, OUTPUT);
  pinMode(PSU_RATIOMETRIC, OUTPUT);
  _loadCalibrations();
  disable();
  routines.psu = pidtimer.every(10, _PSUcontroller, this);
  routines.tec = pidtimer.every(10, _PCcontroller, this);
}

void Peltiercontroller::tx_settings()
{
  _tx_setting(KP_TEC, _Kp);
  _tx_setting(KI_TEC, _Ki);
  _tx_setting(KD_TEC, _Kd);
  _tx_setting(VLIMIT, _v_limit);
  _tx_setting(ILIMIT, _i_limit);
  _tx_setting(SETPOINT, _TEC_Setpoint);
  _tx_setting(PSU_FREQ, _PSU_freq);
  _tx_setting(TEC_FREQ, _TEC_freq);
  _tx_setting(KP_PSU, _PSU_Kp);
  _tx_setting(KI_PSU, _PSU_Ki);
  _tx_setting(KD_PSU, _PSU_Kd);
  _tx_setting(PSU_MAX, _psu_max);
  _tx_setting(COMM_MODE, _psu_comm);
  _tx_setting(PON_TEC, (_PON == 1) ? 0 : 1); //Proportional on measurement if _PON = 0

  // TODO: How can we return the mode (constant current, constant voltage, or PID) of the TEC PID and the mode mode (constant control or PID) of the PSU PID?
  if (_preferredSettings.TECPID == MANUAL)
    _tx_setting(TEC_MANUAL, _preferredSettings.PSUsetpoint);
  else
    _tx_setting(TEC_AUTO, 0);
    
  if (_preferredSettings.PSUPID == MANUAL)
    _tx_setting(PSU_MANUAL, _preferredSettings.PSUabsout);
  else
    _tx_setting(PSU_AUTO, 0); 
}

void Peltiercontroller::_loadCalibrations()
{
  // Load maximum PSU current
  uint8_t psumax_flag;
  EEPROM.get(PSUMAX_FLAG_EEPROM_ADDR, psumax_flag);
  if (psumax_flag == 0xFF)
  {
    _psu_max = 0.0;
  } else {
    EEPROM.get(PSUMAX_EEPROM_ADDR, _psu_max);
  }
  // Load PSU control mode (Ratiometric or RS232)
  uint8_t psucomm;
  EEPROM.get(PSUCOMM_EEPROM_ADDR, psucomm);
  if (psucomm == 0xFF)
  {
    psucomm = 0;
  }
  _psu_comm = psucomm;

  // Load TEC PID constants
  float kp;
  float ki;
  float kd;
  int pon;
  EEPROM.get(TECKP_EEPROM_ADDR, kp);
  EEPROM.get(TECKI_EEPROM_ADDR, ki);
  EEPROM.get(TECKD_EEPROM_ADDR, kd);
  EEPROM.get(TECPON_EEPROM_ADDR, pon);
  
  if (isnan(kp)) {
    kp = 2.77; // Replace unitinialized value with default
  }
  if (isnan(ki)) {
    ki = 21.31; // Replace unitinialized value with default
  }
  if (isnan(kd)) {
    kd = 0.0; // Replace unitinialized value with default
  }
  if (isnan(pon) or (pon < 0)) {
    pon = 1; // Replace unitinialized value with default
  }
  
  setKp(kp, PID_TEC);
  setKi(ki, PID_TEC);
  setKd(kd, PID_TEC);
  setPON(pon, PID_TEC);

  // Load PSU PID constants
  EEPROM.get(PSUKP_EEPROM_ADDR, kp);
  EEPROM.get(PSUKI_EEPROM_ADDR, ki);
  EEPROM.get(PSUKD_EEPROM_ADDR, kd);
  if (isnan(kp)) {
    kp = 2.4; // Replace unitinialized value with default
  }
  if (isnan(ki)) {
    ki = 10.91; // Replace unitinialized value with default
  }
  if (isnan(kd)) {
    kd = 0.0; // Replace unitinialized value with default
  }
  setKp(kp, PID_PSU);
  setKi(ki, PID_PSU);
  setKd(kd, PID_PSU);

  // Load max voltage and current
  float i_limit;
  EEPROM.get(TECIMAX_EEPROM_ADDR, i_limit);
  if (isnan(i_limit)) {
    i_limit = 10; // Replace unitinialized value with default
  }
  setILimit(i_limit);

  // Load PSU and TEC PID sample frequency
  float psu_freq;
  float tec_freq;
  EEPROM.get(PSUFREQ_EEPROM_ADDR, psu_freq);
  EEPROM.get(TECFREQ_EEPROM_ADDR, tec_freq);
  if (isnan(psu_freq)) {
    psu_freq = 5; // Replace unitinialized value with default
  }
  if (isnan(tec_freq)) {
    tec_freq = 5; // Replace unitinialized value with default
  }
  setPID_freq(psu_freq, PID_PSU);
  setPID_freq(tec_freq, PID_TEC);
}

void Peltiercontroller::_tx_setting(pid_settings r, float val)
{
  uint16_t sendSize = 0;
  sendSize = uart_txrx.txObj(r, sendSize);
  sendSize = uart_txrx.txObj(val, sendSize);
  uart_txrx.sendData(sendSize, PID_INFO);
}

//Timed routines
bool Peltiercontroller::_PSUcontroller(Peltiercontroller& pc) {
  pc.currentCompute();
  return true;
}
bool Peltiercontroller::_PCcontroller(Peltiercontroller& pc) {
  pc.analogCompute();
  return true;
}
bool Peltiercontroller:: _rampRoutine(Peltiercontroller& pc) {
  pc.rampCompute();
  return true;
}
