/*
 Peltiercontroller.h
 A class that controls the peltier element
 */

#ifndef Peltiercontroller_h
#define Peltiercontroller_h

#include <PID_v1.h>
#include "Sampler.h"
#include "constants.h"
#include <arduino-timer.h>
#include <Adafruit_MCP23017.h>

class Peltiercontroller
{
  public:
    //Methods
    Peltiercontroller(Sampler& sampler_, SerialTransfer& uart_txrx_, Adafruit_MCP23017& mcp_);
    void analogCompute();
    void currentCompute();
    void setKp(double new_Kp, pid_settings which_pid);
    void setKi(double new_Ki, pid_settings which_pid);
    void setKd(double new_Kd, pid_settings which_pid);
    void setPON(int PON, pid_settings which_pid);
    void setTunings();
    void setTECsetpoint(double setpoint);
    void setPSUsetpoint(double setpoint);
    void setPSUAbsoluteOutput(double value);
    void setPSURatiometricOutput(int value);
    void setPID_freq(double pid_freq, pid_settings which_pid);
    void disable();
    void enable();
    void constantMode(boolean state);
    void init();
    void setSettings(pid_settings cmd, float value);
    void setVLimit(double new_V_limit);
    void setILimit(double new_I_limit);
    void setPSUmax(double psu_max);
    void tx_settings();
    void setMode(pid_settings cmd, float value);
    void controlTECsetpoint(pid_settings cmd, float value);
    void rampCompute();
    void setTECsetpointRamp(double ramp);
    bool checkEnabled();

    //Variables
    Timer<> pidtimer;
  
  private:
    //Private Methods
    //Variables and objects
    SerialTransfer& uart_txrx;
    Sampler& sampler;
    Adafruit_MCP23017& mcp;
    PID* TECPID;
    PID* PSUPID;
    void _loadCalibrations();
    void _tx_setting(pid_settings r, float val);
    void _applyPreferredSettings();
    double _TEC_Output;
    double _TEC_Setpoint;
    double _TEC_Setpoint_start;
    double _TEC_Setpoint_end;
    double _TEC_Setpoint_difference;
    double _TEC_total_steps;
    double _TEC_steps;
    double _Kp;
    double _Ki;
    double _Kd;
    int _PON;
    double _PSU_Output;
    double _PSU_Setpoint;
    double _PSU_Kp;
    double _PSU_Ki;
    double _PSU_Kd;
    double _PSU_freq;
    double _TEC_freq;
    float _v_limit; 
    float _i_limit;
    double _current;
    double _voltage;
    double _psu_max;
    double _TEC_ramp;
    uint8_t _psu_comm; //0: ratiometric, 1: RS232
    bool _enabled;
    _preferred_PID_settings _preferredSettings;
    pid_routines routines;

    //Static routine methods
    static bool _PSUcontroller(Peltiercontroller& pc);
    static bool _PCcontroller(Peltiercontroller& pc);
    static bool _rampRoutine(Peltiercontroller& pc);
};
#endif
