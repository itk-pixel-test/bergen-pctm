/*
  Pctm.h
  The class that representes the PCTM itself.

  Author: Magne Lauritzen
*/

#ifndef Pctm_h
#define Pctm_h

#include "Display.h"
#include "SegmentDisplay.h"
#include "Sampler.h"
#include "Peltiercontroller.h"
#include "constants.h"
#include <EEPROM.h>
#include <arduino-timer.h>
#include <Adafruit_MCP23017.h>
#include "Interlock.h"

class PCTM
{
  public:
    PCTM(SerialTransfer& uart_txrx_, Display& lcd_, SegmentDisplay& id_disp_, Sampler& sampler_, Peltiercontroller& pc_, Interlock& interlock_, Adafruit_MCP23017& mcp_);
    void init();
    void strprint(char str[]);
    void _rx();
    void _calcUptime();
    //Objects
    Timer<> generaltimer;
    //Variables
    uptime_struct uptime;
    volatile bool interlock_connection;
    volatile unsigned long before_time;
    volatile unsigned long delay_time;

  private:
    //Methods
    void _do_request(rx_struct request);
    void _set_systemsetting(syssetting setting, float val);
    void _get_systemsetting(syssetting setting);
    void _tx_setting(syssetting r, float val);
    void _doSync();
    void _ack(uint8_t pack_id, rx_struct rx_pack);
    void _setID(uint8_t id);
    void _clearNonvolatileMemory();
    uint8_t _getID();
    unsigned long _time();
    void _interlockCheck();
    //Objects
    general_routines routines;
    SerialTransfer& uart_txrx;
    Display& lcd; 
    SegmentDisplay& id_disp;
    Sampler& sampler;
    Peltiercontroller& pc;
    Adafruit_MCP23017& mcp;
    Interlock& interlock;
    //Timed routines
    static bool _monitorUptime(PCTM* p);
    static bool _routineRx(PCTM& p);
    static bool _routineLcdUpd(PCTM& p);
    //Private variables
    int _count;
};

#endif
