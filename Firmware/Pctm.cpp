#include "Pctm.h"

PCTM::PCTM(SerialTransfer& uart_txrx_, Display& lcd_, SegmentDisplay& id_disp_, Sampler& sampler_, Peltiercontroller& pc_, Interlock& interlock_, Adafruit_MCP23017& mcp_) : 
  uart_txrx(uart_txrx_), lcd(lcd_), id_disp(id_disp_), sampler(sampler_), pc(pc_), interlock(interlock_), mcp(mcp_)
{
}

void PCTM::init(){
  pinMode(8, OUTPUT);
  digitalWrite(8, HIGH);
  mcp.begin();
  uptime.millis_ = millis();
  uptime.overflows = 0;
  _interlockCheck();
  lcd.init();
  id_disp.init();
  sampler.init();
  pc.init();
  interlock.init();
  id_disp.setSymbol(_getID());
  routines.uptime = generaltimer.every(1000, _monitorUptime, this);
  routines.rx = generaltimer.every(5, _routineRx, this);
  routines.lcdupd = generaltimer.every(200, _routineLcdUpd, this);
  lcd.start_row1 = 0;
  lcd.start_row2 = 0;
  _count = 0;
}


void PCTM::strprint(char str[])
{
  char sendstr[250] = "";
  strcpy(sendstr, str); //If we try to send str[], it only sends the pointer for some reason...
  uint16_t sendSize = 0;
  sendSize = uart_txrx.txObj(sendstr, sendSize);
  uart_txrx.sendData(sendSize, TEXT);
}

void PCTM::_set_systemsetting(syssetting setting, float val)
{
  switch (setting)
  {
    case (ID):
      _setID((uint8_t)val);
      break;
  }
}

void PCTM::_get_systemsetting(syssetting setting)
{
  switch (setting)
  {
    case (ID):
      _tx_setting(ID, _getID());
      break;
  }
}

void PCTM::_do_request(rx_struct request)
{
  uint16_t sendSize = 0;
  switch (request.cmd)
  {
    case (req_UPTIME):
      sendSize = uart_txrx.txObj(UPTIME, sendSize);
      sendSize = uart_txrx.txObj(uptime, sendSize);
      uart_txrx.sendData(sendSize, SYS_INFO);
      break;
    case (req_SYNC):
      _doSync();
      break;
    case (req_SAMPLEFREQS):
      sampler.tx_samplefreqs();
      break;
    case (req_PIDS):
      pc.tx_settings();
      break;
    case (req_ICALIBRATE):
      sampler.calibrateIsensor();
      break;
    case (req_PIDDISABLE):
      pc.disable();
      break;
    case (req_PIDENABLE):
      pc.enable();
      break;
    case (req_RESET):
      _clearNonvolatileMemory();
      break;
    case (req_CONSTANTENABLE):
      pc.constantMode(true);
      break;
    case (req_CONSTANTDISABLE):
      pc.constantMode(false);
      break;
    case (req_ACTIVATEINTERLOCK):
      interlock.activateInterlock(true);
      break;
    case (req_DEACTIVATEINTERLOCK):
      interlock.activateInterlock(false);
      break;
    case (req_INTERLOCK):
      interlock.tx_settings();
      break;
    case (req_ID):
      _get_systemsetting(ID);
  }
}
void PCTM::_rx()
{   
  if(uart_txrx.available())
  {
    uint8_t pack_id = uart_txrx.currentPacketID();
    rx_struct rx_pack;
//    char str[250] = "";
//    sprintf(str, "Got command with packID %d", pack_id);
//    strprint(str);
    switch (pack_id)
    {
      case (REQUESTS):
        rx_struct_nodata rx_obj;
        uart_txrx.rxObj(rx_obj, 0);
        rx_pack.cmd = rx_obj.cmd;
        _ack(pack_id, rx_pack);
        _do_request(rx_pack);
        break;
      case (PELTIER):
        uart_txrx.rxObj(rx_pack, 0);
        _ack(pack_id, rx_pack);
        pc.setSettings(rx_pack.cmd, rx_pack.val);
        break;
      case (SAMPLEFREQ):
        uart_txrx.rxObj(rx_pack, 0);
        _ack(pack_id, rx_pack);
        sampler.setSamplePeriod(rx_pack.cmd, (int)rx_pack.val);
        break;
      case (SYS_SETTINGS):
        uart_txrx.rxObj(rx_pack, 0);
        _ack(pack_id, rx_pack);
        _set_systemsetting(rx_pack.cmd, rx_pack.val);
        break;
      case (INTERLOCK):
        uart_txrx.rxObj(rx_pack, 0);
        _ack(pack_id, rx_pack);
        interlock.setSettings(rx_pack.cmd, rx_pack.val); 
    }
  }
}

void PCTM::_ack(uint8_t pack_id, rx_struct rx_pack)
{
  //Acknowledge the reception of a data packet by returning it.
  ack_struct ack_s;
  ack_s.ID = pack_id;
  ack_s.rx_pack = rx_pack;
  uint16_t sendSize = 0;
  sendSize = uart_txrx.txObj(ack_s, sendSize);
  uart_txrx.sendData(sendSize, ACK);
}

void PCTM::_calcUptime()
{
  unsigned long m = millis();
  if (m < uptime.millis_){
    uptime.overflows += 1;
  }
  uptime.millis_ = m;
}

void PCTM::_doSync()
{
  /*Syncing must be performed in this blocking function because there is no way to
  know how long a sync command stays in the input buffer before being read.*/
  //Tell the PC that the PCTM is ready to synchronize
  uint16_t sendSize = 0;
  sendSize = uart_txrx.txObj((unsigned char)SYNC_READY, sendSize);
  uart_txrx.sendData(sendSize, (unsigned char)SYS_INFO);
  //Blocking wait for the sync command, ignoring all other commands
  bool synchronized = false;
  while(not synchronized){
    if(uart_txrx.available())
    {
      uint8_t pack_id = uart_txrx.currentPacketID();
      switch (pack_id)
      {
        case (REQUESTS):
          rx_struct_nodata rx_obj;
          uart_txrx.rxObj(rx_obj, 0);
          if (rx_obj.cmd == do_SYNC){
            sampler.setSync(millis());
            synchronized = true;
          }
          break;
      }
    }
  }
}

void PCTM::_tx_setting(syssetting r, float val)
{
  uint16_t sendSize = 0;
  sendSize = uart_txrx.txObj(r, sendSize);
  sendSize = uart_txrx.txObj(val, sendSize);
  uart_txrx.sendData(sendSize, SYS_SETTING_INFO);
}

void PCTM::_setID(uint8_t id)
{
  EEPROM.put(ID_EEPROM_ADDR, id);
  id_disp.setSymbol(id);
}

uint8_t PCTM::_getID()
{
  uint8_t id;
  EEPROM.get(ID_EEPROM_ADDR, id);
  return id;
}

void PCTM::_clearNonvolatileMemory()
{
  for (int i = 0 ; i < EEPROM.length() ; i++) {
    EEPROM.update(i, 0xFF);
  }
}

void PCTM::_interlockCheck()
{
  CPUINT.LVL1VEC = 0b00011000; //Select PORTC (I2C_SNIFFER_SCL) to level 1 vector priority
  mcp.pinMode(INTERLOCK_CONNECTION, INPUT);
  pinMode(I2C_SNIFFER_SDA, INPUT);
  if (mcp.digitalRead(INTERLOCK_CONNECTION) == HIGH)
  {
    interlock_connection = true;
    sampler.interlock_connection = true;

    PORTA.PIN0CTRL = 0b00000010; //Interrupt enabled with sense on rising edge on pin D2 (I2C_SNIFFER_START)
  }
  else
  {
    interlock_connection = false;
    sampler.interlock_connection = false;
  }
  mcp.setupInterrupts(false, false, LOW);
  mcp.setupInterruptPin(INTERLOCK_CONNECTION, CHANGE);
  before_time = millis();
  PORTA.PIN1CTRL = 0b00000011; //Interrupt enabled with sense on falling edge on pin D7 (EXPANDER_INTB)
}

    
//Timed routines
bool PCTM::_monitorUptime(PCTM* p){p->_calcUptime();return true;}
bool PCTM::_routineRx(PCTM& p) {p._rx(); return true;}
bool PCTM::_routineLcdUpd(PCTM& p) {p.lcd.update(); p._count +=1; if(p._count == 3){p.lcd.start_row1+=1; p.lcd.start_row2+=1; p._count = 0;} return true;}
