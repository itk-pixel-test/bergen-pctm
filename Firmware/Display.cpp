#include "Display.h"

Display::Display(Sampler& sampler_, Adafruit_MCP23017& mcp_) : sampler(sampler_), mcp(mcp_)
{
  lcd = new DogLcd(MOSI, SCK, DISPLAY_RS, DISPLAY_SELECT, mcp);
}

void Display::update()
{
  //Updates the display. Should be called once in a while, e.g. every 100ms.
  _BuildMainInfoString();
  _BuildSecondaryInfoString();
  lcd->setCursor(0, 0);
  lcd->print(_main_info);
  lcd->setCursor(0, 1);
  lcd->print(_scroll_info_row1);
  lcd->setCursor(0, 2);
  lcd->print(_scroll_info_row2);
}

void Display::_BuildMainInfoString()
{
  //Builds the string that presents the most important information
  char buf_T1[7];
  _TempToStr(buf_T1, sampler.module_temperature.ntc_1.val, 1);
  char buf_DP[7];
  _TempToStr(buf_DP, sampler.environment.humidity_1.dp.val, 0);
  sprintf(_buf_main_info, "T:%6s DP:%4s", buf_T1, buf_DP);
  strncpy(_main_info, _buf_main_info, 16);
}

void Display::_BuildSecondaryInfoString()
{
  //char buf_T2[7];
  //_TempToStr(buf_T2, sampler.module_temperature.ntc_2.val, 1);
  //char buf_T3[7];
  //_TempToStr(buf_T3, sampler.module_temperature.ntc_3.val, 1);
  char buf_H1[7];
  _HumToStr(buf_H1, sampler.environment.humidity_1.rel.val);
  char buf_H2[7];
  _HumToStr(buf_H2, sampler.environment.humidity_2.rel.val);
  char buf_GS[7];
  _GSToStr(buf_GS, sampler.environment.gas_flow.val, 1);
  char buf_CP[7];
  _TempToStr(buf_CP, sampler.coldstack.coldplate_t.val, 1);
  char buf_VC[7];
  _TempToStr(buf_VC, sampler.coldstack.vacuumchuck_t.val, 1);
  char buf_TEC[7];
  _TECToStr(buf_TEC, sampler.coldstack.peltier_i.val, 1);
  char buf_DP2[7];
  _TempToStr(buf_DP2, sampler.environment.humidity_2.dp.val, 0);
  char buf_VA[8];
  dtostrf(sampler.adc0_value, 4, 1, buf_VA); // Convert the pressure value to a string with 2 decimal places

  sprintf(_Secondary_info_row1, "H1:%4s H2:%4s DP2:%4s ", buf_H1, buf_H2, buf_DP2);
  sprintf(buf_VA, "%skPa", buf_VA); // Append "kPa" to the formatted pressure value
  sprintf(_Secondary_info_row2, "GS:%5s CP:%6s VC:%6s TEC:%6s VA:%4s ", buf_GS, buf_CP, buf_VC, buf_TEC, buf_VA);

  char buf_scroll_1[17] = {};
  _BuildScrollInfo(buf_scroll_1, _Secondary_info_row1, 1);
  sprintf(_scroll_info_row1, "%s", buf_scroll_1);

  char buf_scroll_2[17] = {};
  _BuildScrollInfo(buf_scroll_2, _Secondary_info_row2, 2);
  sprintf(_scroll_info_row2, "%s", buf_scroll_2);
}

void Display::_BuildScrollInfo(char *buf, char *info, int row)
{
  int start;
  switch (row)
  {
    case 1:
      if (start_row1 >= strlen(info))
      {
        start_row1 = 0;
      }
      start = start_row1;
      break;

    case 2:
      if (start_row2 >= strlen(info))
      {
        start_row2 = 0;
      }
      start = start_row2;
      break;
  }

  strncat(buf, info + start, 16);

  if (start + 16 >= strlen(info))
  {
    strncat(buf, info, start + 16 - strlen(info));
  }
}

char * Display::_TempToStr(char *buf, float T, int precision)
{
  char str_temp[100];
  dtostrf(T, 3, precision, str_temp);
  if (strlen(str_temp) > 5)
  {
    sprintf(buf,"%sC","?");
  }
  else
  {
    sprintf(buf,"%sC",str_temp);
  }
}

char * Display::_HumToStr(char *buf, float H)
{
  char str_hum[100];
  dtostrf(H, 3, 0, str_hum);
  if (strlen(str_hum) > 3)
  {
    sprintf(buf,"%s%%","?");
  }
  else
  {
    sprintf(buf,"%s%%",str_hum);  
  }
}

char * Display::_GSToStr(char *buf, float G, int precision)
{
  char str_gs[100];
  dtostrf(G, 3, precision, str_gs);  
  if (strlen(str_gs) > 4)
  {
    sprintf(buf,"%s","?");
  }
  else
  {
    sprintf(buf,"%s",str_gs);
  }  
}

char * Display::_TECToStr(char *buf, float I, int precision)
{
  char str_tec[100];
  dtostrf(I, 3, precision, str_tec);  
  if (strlen(str_tec) > 5)
  {
    sprintf(buf,"%sA","?");
  }
  else
  {
    sprintf(buf,"%sA",str_tec);
  }  
}

void Display::init()
{
  //Init the display
  lcd->begin(DOG_LCD_M163, 0x1D);
  lcd->clear();
  lcd->print("Display");
  lcd->setCursor(0, 1);
  lcd->print("initialized");
  lcd->noCursor();
  lcd->clear();
}
