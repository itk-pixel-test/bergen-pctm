#include <arduino-timer.h>
#include <PID_v1.h>

struct sample //Struct that defines all sampled values
{
  float val; //The sampled value
  float s; //Time in seconds at sampletime
  uint16_t counts; //Raw ADC counts, if applicable
};

struct humidity_struct //Struct that holds sampled humidity values from one humidity sensor
{
  sample rel;
  sample dp;
  sample t;
};

struct pid_info_struct // Struct that holds PID loop info that is provided to the Sampler from the Peltiercontroller
{
  float TEC_setpoint;
  float PSU_setpoint;
  float PSU_control;
  int PSU_control_pwm;
};

struct pid_routines // Struct that holds PID routines
{
  uintptr_t tec;
  uintptr_t psu;
  uintptr_t ramp;
};

struct sample_routines //Struct that holds the timed sampling routine tasks
{
  uintptr_t ntc_1;
  uintptr_t ntc_2;
  uintptr_t ntc_3;
  uintptr_t hum_1;
  uintptr_t hum_2;
  uintptr_t gas;
  uintptr_t coldplate;
  uintptr_t vchuck;
  uintptr_t vchuck_read;
  uintptr_t peltier;
  uintptr_t pid;
  uintptr_t vacuum_pressure;
};

struct sample_freqs //Struct that holds the sampling frequencies.
{
  float ntc_1 = 0;
  float ntc_2 = 0;
  float ntc_3 = 0;
  float hum_1 = 0;
  float hum_2 = 0;
  float gas = 0;
  float coldplate = 0;
  float vchuck = 0;
  float vchuck_read = 0;
  float peltier = 0;
  float pid = 0;
  float vacuum_pressure = 0;
};

struct _preferred_PID_settings // struct that holds the preferred PID loop settings
{
  int TECPID = AUTOMATIC;
  int PSUPID = AUTOMATIC;
  float PSUsetpoint = 0.0;
  float PSUabsout = 0;
};

struct general_routines //Struct that holds the general timed routines
{
  uintptr_t rx;
  uintptr_t lcdupd;
  uintptr_t uptime;
};

struct rx_struct
{
  uint16_t cmd;
  float val;
};

struct ack_struct
{
  uint8_t ID;
  rx_struct rx_pack;
};

struct rx_struct_nodata
{
  uint16_t cmd;
};

struct uptime_struct
{
    unsigned long overflows; 
    unsigned long millis_;
};
