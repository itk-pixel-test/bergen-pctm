#include "SegmentDisplay.h"

SegmentDisplay::SegmentDisplay(Adafruit_MCP23017& mcp_) : mcp(mcp_)
{
  pinMode(MOSI, OUTPUT);
  pinMode(SCK, OUTPUT);
}

void SegmentDisplay::init()
{
  mcp.pinMode(ID_DISPLAY, OUTPUT);
}

void SegmentDisplay::setSymbol(uint8_t symbol)
{
  _write(_decodeByte(symbol));
}

byte SegmentDisplay::_decodeByte(uint8_t b)
{
  switch (b)
  {
    case 0x00:
      return 0x3F;
    case 0x01:
      return 0x06;
    case 0x02:
      return 0x5B;
    case 0x03:
      return 0x4F;
    case 0x04:
      return 0x66;
    case 0x05:
      return 0x6D;
    case 0x06:
      return 0x7D;
    case 0x07:
      return 0x07;
    case 0x08:
      return 0x7F;
    case 0x09:
      return 0x6F;
    case 0x0A:
      return 0xF7;
    case 0x0B:
      return 0xFC;
    case 0x0C:
      return 0xB9;
    case 0x0D:
      return 0xDE;
    case 0x0E:
      return 0xF9;
    case 0x0F:
      return 0xF1;
  }
  return 0x00; //In case the byte is not recognized, the display won't light up.
}

void SegmentDisplay::_write(uint8_t b)
{
  digitalWrite(SCK,LOW);
  for(int i=7;i>=0;i--) {
    if(bitRead(b,i)) {
      digitalWrite(MOSI,HIGH);
    }
    else {
      digitalWrite(MOSI,LOW);
    }
    digitalWrite(SCK,HIGH);
    digitalWrite(SCK,LOW);
  }
  mcp.digitalWrite(ID_DISPLAY, HIGH);
  mcp.digitalWrite(ID_DISPLAY, LOW);
}
