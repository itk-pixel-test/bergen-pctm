/*
 Sampler.h
 A class that handles the sampling of monitored values.
 The sampled values are immediately sent over USB.
 */

#ifndef Sampler_h
#define Sampler_h

#include "Arduino.h"
#include "types.h"
#include "enums.h"
#include "SerialTransfer.h"
#include "constants.h"
#include "SHTSensor.h"
#include "Adafruit_MAX31865.h"
#include <arduino-timer.h>
#include <Wire.h>
#include <EEPROM.h>
#include <Adafruit_MCP23017.h>
#include <DFRobot_ADS1115.h>   //ADD Library for ADC



struct module_t_struct //Struct that holds sampled module temperatures
{
  sample ntc_1;
  double ntc_1_dval; //Precast double version of the ntc_1 value for use with the PID controller
  sample ntc_2;
  sample ntc_3;
};
struct environment_struct //Struct that holds sampled humidity and gas flow values
{
  sample gas_flow;
  humidity_struct humidity_1;
  humidity_struct humidity_2;
};
struct coldstack_struct //Struct that holds sampled values related to the cooling unit coldstack
{
  sample coldplate_t;
  sample vacuumchuck_t;
  sample peltier_i;
  double peltier_i_dval;
  sample peltier_v; //Not supported as of v0.1, but will be added in v0.2
};

class Sampler
{
  public:
    //Methods
    Sampler(SerialTransfer& uart_txrx_, Adafruit_MCP23017& mcp_);
    void sampleModuleT(sample_source NTC);
    void sampleHumidity(int sensor);
    void sampleGasFlow();
    void sampleColdplateT();
    void sampleVacuumchuckT();
    void samplePeltier();
    void sampleColdstack();
    void samplePid();
    void sampleADS1115(); //ADD Pressure
    void init();
    void setSamplePeriod(sample_routine routine, int period);
    void setSync(unsigned long synctime);
    void calibrateIsensor();
    void tx_samplefreqs();
    unsigned long _time();
		
    //Variables
    module_t_struct module_temperature;
    environment_struct environment;
    coldstack_struct coldstack;
    pid_info_struct pid_info;
    sample_routines routines;
    sample_freqs freqs;
    Timer<> sampletimer;
    
    float adc0_value;
    unsigned long sync = 0;
    volatile bool interlock_connection;
    volatile bool i2c_ready;
    volatile uint8_t sda_buffer;
    volatile uint64_t i2c_buffer;
    volatile unsigned long i2c_sniffer_time;
  
  private:
    SerialTransfer& uart_txrx;
    Adafruit_MCP23017& mcp;
    DFRobot_ADS1115 ads;  //ADDED PRESSUE
    void _emit(sample_source src, sample val);
    void _tx_samplefreq(sample_routine r, float f);
    sample _readModuleTemperature(int module_num);
    sample _readColdPlateTemperature();
    sample _readGasFlow();
    sample _readPeltierCurrent();
    void _i2c_select(int channel);
    
    //Static routine sampling methods
    static bool _routineSampleNtc1(Sampler& s);
    static bool _routineSampleNtc2(Sampler& s);
    static bool _routineSampleNtc3(Sampler& s);
    static bool _routineSampleHum1(Sampler& s);
    static bool _routineSampleHum2(Sampler& s);
    static bool _routineSampleGasflow(Sampler& s);
    static bool _routineSampleColdplate(Sampler& s);
    static bool _routineSampleVChuck(Sampler& s);
    static bool _routineReadVChuck(Sampler& s);
    static bool _routineSamplePeltier(Sampler& s);
    static bool _routineSamplePid(Sampler& s);
    static bool _routineSampleVacuumPressure(Sampler& s);

    void _loadCalibrations();
    void _setDefaultSamplePeriods();
    void _readVacuumChuckTemperature();
    void _writeVacuumChuckTemperature();
    humidity_struct _readHumidity(int sensor);
    humidity_struct _readHumidity_sniffer();
    
    //Variables and objects
    float VC_sample_time;
    uintptr_t _old_routine;
    bool _resampleNextVCT = false;
    Adafruit_MAX31865* thermo;
    SHTSensor* sht;
    double _current_zero;

    
};
#endif
