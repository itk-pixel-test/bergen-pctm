#include "Sampler.h"

Sampler::Sampler(SerialTransfer& uart_txrx_, Adafruit_MCP23017& mcp_) : uart_txrx(uart_txrx_), mcp(mcp_), ads(&Wire) // ads(0x49)
{
  thermo = new Adafruit_MAX31865(CHUCK_PT100_SELECT, MOSI, MISO, SCK, mcp); //Must use software SPI. SPI.begin() conflicts with DOGLCD.
  sht = new SHTSensor(SHTSensor::SHT3X);
}

void Sampler::init()
{
  //Wire.begin();
  _loadCalibrations();
  sht->init();
  sht->setAccuracy(SHTSensor::SHT_ACCURACY_HIGH);
  thermo->begin(MAX31865_4WIRE);
  mcp.pinMode(CHUCK_PT100_READY, INPUT);
  pinMode(PELTIER_CURRENT, INPUT);
  routines.vchuck_read = sampletimer.every(2, _routineReadVChuck, this);
  _setDefaultSamplePeriods();

  // Initialize ADS1115 reading vacuum pressure from the pressure sensor
  ads.setAddr_ADS1115(ADS1115_IIC_ADDRESS0); // Set address
  ads.setGain(eGAIN_TWOTHIRDS); // 2/3x gain
  ads.setMode(eMODE_SINGLE); // single-shot mode
  ads.setRate(eRATE_128); // 128SPS (default)
  ads.setOSMode(eOSMODE_SINGLE); // Set to start a single-conversion
  Serial.println("Initializing ADS1115...");
  ads.init();
  Serial.println("ADS1115 initialization complete");
}


void Sampler::setSync(unsigned long synctime)
{
  _resampleNextVCT = true;
  sync = synctime;
}

void Sampler::setSamplePeriod(sample_routine routine, int period)
{
  float F = 0;
  float p = period;
  if (period!=0){F = 1000.0/p;}
  else{F = 0.0;}
  switch (routine)
  {
    case sr_NTC_1:
      freqs.ntc_1 = F;
      _old_routine = routines.ntc_1;
      sampletimer.cancel(_old_routine);
      if (period!=0){routines.ntc_1 = sampletimer.every(period, _routineSampleNtc1, this);}
      break;
    case sr_NTC_2:
      freqs.ntc_2 = F;
      _old_routine = routines.ntc_2;
      sampletimer.cancel(_old_routine);
      if (period!=0){routines.ntc_2 = sampletimer.every(period, _routineSampleNtc2, this);}
      break;
    case sr_NTC_3:
      freqs.ntc_3 = F;
      _old_routine = routines.ntc_3;
      sampletimer.cancel(_old_routine);
      if (period!=0){routines.ntc_3 = sampletimer.every(period, _routineSampleNtc3, this);}
      break;
    case sr_HUM_1:
      freqs.hum_1 = F;
      _old_routine = routines.hum_1;
      sampletimer.cancel(_old_routine);
      if (period!=0){routines.hum_1 = sampletimer.every(period, _routineSampleHum1, this);}
      break;
    case sr_HUM_2:
      freqs.hum_2 = F;
      _old_routine = routines.hum_2;
      sampletimer.cancel(_old_routine);
      if (period!=0){routines.hum_2 = sampletimer.every(period, _routineSampleHum2, this);}
      break;
    case sr_GAS:
      freqs.gas = F;
      _old_routine = routines.gas;
      sampletimer.cancel(_old_routine);
      if (period!=0){routines.gas = sampletimer.every(period, _routineSampleGasflow, this);}
      break;
    case sr_CP:
      freqs.coldplate = F;
      _old_routine = routines.coldplate;
      sampletimer.cancel(_old_routine);
      if (period!=0){routines.coldplate = sampletimer.every(period, _routineSampleColdplate, this);}
      break;
    case sr_VC:
      freqs.vchuck = F;
      _old_routine = routines.vchuck;
      sampletimer.cancel(_old_routine);
      if (period!=0){routines.vchuck = sampletimer.every(period, _routineSampleVChuck, this);}
      break;
    case sr_TEC:
      freqs.peltier = F;
      _old_routine = routines.peltier;
      sampletimer.cancel(_old_routine);
      if (period!=0){routines.peltier = sampletimer.every(period, _routineSamplePeltier, this);}
      break;
    case sr_PID:
      freqs.pid = F;
      _old_routine = routines.pid;
      sampletimer.cancel(_old_routine);
      if (period!=0){routines.pid = sampletimer.every(period, _routineSamplePid, this);}
      break;
    case sr_VACUUM_PRESSURE:
      freqs.vacuum_pressure = F;
      _old_routine = routines.vacuum_pressure;
      sampletimer.cancel(_old_routine);
      if (period!=0){routines.vacuum_pressure = sampletimer.every(period, _routineSampleVacuumPressure, this);}
      break;
  }
}

void Sampler::sampleModuleT(sample_source NTC)
{
  float t;
  switch (NTC)
  {
    case ss_NTC_1:
      module_temperature.ntc_1 = _readModuleTemperature(NTC);
      module_temperature.ntc_1_dval = module_temperature.ntc_1.val;
      _emit(NTC, module_temperature.ntc_1);
      break;
    case ss_NTC_2:
      module_temperature.ntc_2 = _readModuleTemperature(NTC);
      _emit(NTC, module_temperature.ntc_2);
      break;
    case ss_NTC_3:
      module_temperature.ntc_3 = _readModuleTemperature(NTC);
      _emit(NTC, module_temperature.ntc_3);
      break;
  }
}

void Sampler::sampleADS1115() {
     _i2c_select(3);
    if (ads.checkADS1115()) {
        int16_t adc0;
        adc0 = ads.readVoltage(0);

        // Convert voltage [m]) to pressure [kPa]
        int16_t voltage = abs(adc0); // Voltage in mV
        adc0_value = 0.04213 * voltage - 25.48; // Converting formula

        // Emit the vacuum pressure to the PCTM software
        sample adc0_sample;
        adc0_sample.s = _time();
        adc0_sample.val = adc0_value;
        _emit(ss_VACUUM_PRESSURE, adc0_sample);
    } else {
        Serial.println("ADS1115 Disconnected!");
    }
}

void Sampler::sampleHumidity(int sensor)
{
  switch (sensor)
    {
    case 0:

      // Print the values of interlock_connection and i2c_ready for debugging
      //Serial.print("interlock_connection: ");
      //Serial.println(interlock_connection);
      //Serial.print("i2c_ready: ");
      //Serial.println(i2c_ready);


      if (interlock_connection == false)
        {
          environment.humidity_1 = _readHumidity(0);
        }
      if (interlock_connection == true && i2c_ready == true)
        {
        environment.humidity_1 = _readHumidity_sniffer();
        PORTA.PIN0CTRL = 0b00000010 & (interlock_connection << 1); //Interrupt re-enabled (I2C_SNIFFER_START) only if interlock_connection is true
        }

      //environment.humidity_1 = _readHumidity(0);
        
      _emit(ss_DP_1, environment.humidity_1.dp);
      _emit(ss_RH_1, environment.humidity_1.rel);
      _emit(ss_ENVT_1, environment.humidity_1.t);

      //Printing statsment for debugging humidity 
      //Serial.print("Sensor 0 - Dew Point: ");
      //Serial.print(environment.humidity_1.dp.val);  // Print dew point (dp.val)
      //Serial.print(", Relative Humidity: ");
      //Serial.print(environment.humidity_1.rel.val); // Print relative humidity (rel.val)
      //Serial.print(", Temperature: ");
      //Serial.println(environment.humidity_1.t.val); // Print temperature (t.val)
      break;
    case 1:
      environment.humidity_2 = _readHumidity(1);
      _emit(ss_DP_2, environment.humidity_2.dp);
      _emit(ss_RH_2, environment.humidity_2.rel);
      _emit(ss_ENVT_2, environment.humidity_2.t);
      break;
    }
}


void Sampler::sampleGasFlow()
{
  environment.gas_flow = _readGasFlow();
  _emit(ss_GAS, environment.gas_flow);
}

void Sampler::sampleColdplateT()
{
  coldstack.coldplate_t = _readColdPlateTemperature();
  _emit(ss_CP, coldstack.coldplate_t);
}

void Sampler::sampleVacuumchuckT()
{
  _writeVacuumChuckTemperature();
}

void Sampler::samplePeltier()
{
  coldstack.peltier_i = _readPeltierCurrent();
  coldstack.peltier_i_dval = coldstack.peltier_i.val;
  _emit(ss_TEC_I, coldstack.peltier_i);
  //coldstack.peltier_v = PLACEHOLDER_PELTIER_V; Add in V0.2
  //_emit(ss_TEC_V, coldstack.peltier_v);
}

void Sampler::sampleColdstack()
{
  sampleColdplateT();
  sampleVacuumchuckT();
  samplePeltier();
}

void Sampler::samplePid()
{
  // Simply emits the PID values and setpoints which have been set by the Peltiercontroller
  sample pid_sample;
  pid_sample.s = _time();
  pid_sample.val = pid_info.TEC_setpoint;
  _emit(ss_TEC_SP, pid_sample);
  pid_sample.val = pid_info.PSU_setpoint;
  _emit(ss_PSU_SP, pid_sample);
  pid_sample.val = pid_info.PSU_control;
  pid_sample.counts = pid_info.PSU_control_pwm;
  _emit(ss_PSU_CTRL, pid_sample);
}

void Sampler::calibrateIsensor()
{
  // Sample the current sensor continuously for one second (or up to 256 samples, whichever comes first)
  // and set the zero-point to its average
  double reads[256];
  double average = 0;
  sample s;
  int counter = 0;

  // Set existing calibration factor to zero for the purpose of the calibration
  _current_zero = 0;

  // Sample current sensor
  unsigned long starttime = millis();
  unsigned long endtime = starttime;
  while ((endtime - starttime) < 1000)
  {
    s = _readPeltierCurrent();
    reads[counter] = s.val;
    counter = counter + 1;
    if (counter == 256)
    {
      break;
    }
    endtime = millis();
  }

  // Add all reads and divide by number of reads to get average
  for (int i = 0; i < counter ; i++ )
  {
    average = average + reads[i];
  }
  average = average/counter;
  // Write average value (calibration) to EEPROM and set the flag indicating the calibration has been set
  EEPROM.put(ICAL_EEPROM_ADDR, average);
  EEPROM.put(ICAL_FLAG_EEPROM_ADDR, 0);
  // Update volatile calibration value
  _current_zero = average;
}

void Sampler::tx_samplefreqs()
{
  //Transmit back to the PC all the sample frequencies in use
  _tx_samplefreq(sr_NTC_1, freqs.ntc_1);
  _tx_samplefreq(sr_NTC_2, freqs.ntc_2);
  _tx_samplefreq(sr_NTC_3, freqs.ntc_3);
  _tx_samplefreq(sr_HUM_1, freqs.hum_1);
  _tx_samplefreq(sr_HUM_2, freqs.hum_2);
  _tx_samplefreq(sr_GAS, freqs.gas);
  _tx_samplefreq(sr_CP, freqs.coldplate);
  _tx_samplefreq(sr_VC, freqs.vchuck);
  _tx_samplefreq(sr_TEC, freqs.peltier);
  _tx_samplefreq(sr_PID, freqs.pid);
  _tx_samplefreq(sr_VACUUM_PRESSURE, freqs.vacuum_pressure);
}

void Sampler::_tx_samplefreq(sample_routine r, float f)
{
  uint16_t sendSize = 0;
  sendSize = uart_txrx.txObj(r, sendSize);
  sendSize = uart_txrx.txObj(f, sendSize);
  uart_txrx.sendData(sendSize, SAMPLE_FREQ);
}

void Sampler::_emit(sample_source src, sample smpl)
{
  /* The _emit method sends a sampled value 'val' on the USB
   *  port. It sends the sample source enum 'src', which
   *  the receiver will use to know how to unpack and interpret
   *  the sampled value, together with the sample.
   */
  uint16_t sendSize = 0;
  sendSize = uart_txrx.txObj((unsigned char)src, sendSize);
  sendSize = uart_txrx.txObj(smpl, sendSize);
  uart_txrx.sendData(sendSize, SAMPLE_DATA);
}

void Sampler::_loadCalibrations()
{
  uint8_t ical_flag;
  EEPROM.get(ICAL_FLAG_EEPROM_ADDR, ical_flag);
  if (ical_flag == 0xFF)
  {
    _current_zero = 0.0;
  }else{
    EEPROM.get(ICAL_EEPROM_ADDR, _current_zero);
  }
}

void Sampler::_setDefaultSamplePeriods()
{
  /* Applies default sampling periods to all sampling routines. Called in init()*/
  setSamplePeriod(sr_NTC_1, 333);
  setSamplePeriod(sr_NTC_2, 333);
  setSamplePeriod(sr_NTC_3, 333);
  setSamplePeriod(sr_HUM_1, 500);
  setSamplePeriod(sr_HUM_2, 500);
  setSamplePeriod(sr_GAS, 500);
  setSamplePeriod(sr_CP, 200);
  setSamplePeriod(sr_VC, 200);
  setSamplePeriod(sr_TEC, 200);
  setSamplePeriod(sr_PID, 400);
  setSamplePeriod(sr_VACUUM_PRESSURE, 200);
}

void Sampler::_writeVacuumChuckTemperature()
{
	/*
	Instructs the MAX31865 to digitize the vacuum chuck temperature. This
	process takes ~55ms. As soon as the conversion process is finished, it is
	retrieved by the routine _readVacuumChuckTemperature().
	*/
  thermo->writeRTD();
  VC_sample_time = _time();
}

void Sampler::_readVacuumChuckTemperature()
{
  /*
	Reads a vacuum chuck temperature sample from the MAX31865 if there is one
	available. A sampling takes ~55ms and is requested with _writeVacuumChuckTemperature.
	This method is called repeatedly at a nominal frequency of 500Hz and will
	immediately get the sample if one is available.
	*/

  if (mcp.digitalRead(CHUCK_PT100_READY) == LOW)
  {

    double RREF = 400.0; //The value of the Rref resistor. Needs to be at least 4 * (RTD res at 0C)
    double RNOMINAL =  100.0; //The 'nominal' 0-degrees-C resistance of the sensor. 100.0 for PT100, 1000.0 for PT1000

    uint16_t rtd_value = thermo->readRTD();
    //double VC_res = (rtd_value x RREF) / (pow(2.0, 15.0)); // RRTD = (ADC Code x RREF)/2^15. ADC code : 15-bit (2^15 or  32768) result
    if (_resampleNextVCT)
    {
      _writeVacuumChuckTemperature();
      _resampleNextVCT=false;
      return;
    }
    //double VC_temp = (rtd_value / 32.0) - 256.0; //Temperature approximation, good for -100 to 100
    double VC_temp = thermo->temperature(RNOMINAL, RREF, rtd_value); //Temperature calc from https://www.analog.com/media/en/technical-documentation/application-notes/AN709_0.pdf.

    coldstack.vacuumchuck_t.val = VC_temp;
    coldstack.vacuumchuck_t.s = VC_sample_time;
    coldstack.vacuumchuck_t.counts = rtd_value;

    _emit(ss_VC, coldstack.vacuumchuck_t);
  }
}

sample Sampler::_readModuleTemperature(int module_num)
{
  int ntc_value;
  double ntc_ratio;
  sample temp_struct;

  switch(module_num)
  {
    case ss_NTC_1:
      _i2c_select(2);
      Wire.requestFrom(0x48, 2);
      if(Wire.available())
      {
        int ntc_msb = Wire.read();
        ntc_msb = ntc_msb << 8;
        ntc_value = Wire.read() | ntc_msb;
        ntc_ratio = ntc_value / (4096 * 1.7147);
      }
      else
      {
        ntc_value = NAN;
        ntc_ratio = NAN;
      }
      break;
    case ss_NTC_2:
      ntc_value = analogRead(NTC_2);
      ntc_ratio = ntc_value / 1023.0;
      break;
    case ss_NTC_3:
      ntc_value = analogRead(NTC_3);
      ntc_ratio = ntc_value / 1023.0;
      break;
  }

  double ntc_res = (ntc_ratio * 84.5) / (1 - ntc_ratio * (1.0 + (84.5 / 132.0)));

  double ntc_res_log = log10(ntc_res);
  double ntc_temp = -1.46779 * pow(ntc_res_log, 3.0) + 14.5655 * pow(ntc_res_log, 2.0) - 86.2842 * ntc_res_log + 371.338;
  double ntc_temp_cels = ntc_temp - 273.15;

  float sample_time  = _time();

  temp_struct.val = ntc_temp_cels;
  temp_struct.s = sample_time;
  temp_struct.counts = ntc_value;

  return temp_struct;
}

sample Sampler::_readColdPlateTemperature()
{
  sample temp_struct;

  double ntc_value = analogRead(NTC_CP);
  double ntc_ratio = ntc_value / 1023.0;
  double ntc_res = 1000*(ntc_ratio * 24.3) / (1 - ntc_ratio * (1.0 + (24.3 / 132.0)));

  //the following lines are the temperature calculation before we switched to a NXRT15XH103FA1B040 thermistor for the CP
  //double ntc_res_log = log10(ntc_res);
  //double ntc_temp_cels = -2.2 * pow(ntc_res_log, 3.0) + 11 * pow(ntc_res_log, 2.0) - 60 * ntc_res_log + 25;

  const float R0 = 10000.0;    // Nominal resistance at T0
  const float BETA = 3380.0;   // Beta constant of the thermistor
  const float T0 = 298.15;     // Nominal temperature in Kelvin (25°C)

  // Apply the Beta formula and convert temperature to Celcius
  double ntc_temp_cels = 1.0 / (1.0 / T0 + log(ntc_res / R0) / BETA) - 273.15; 
  
  float sample_time = _time();

  temp_struct.val = ntc_temp_cels;
  temp_struct.s = sample_time;
  temp_struct.counts = ntc_value;

  return temp_struct;
}

sample Sampler::_readGasFlow()
{
  sample gasflow_struct;
  double gasflow_max = 2.0;

  double gasflow_value = analogRead(FLOW);
  double gasflow_ratio = gasflow_value / 1023.0;

  double gasflow = gasflow_ratio * gasflow_max;

  float sample_time = _time();

  gasflow_struct.val = gasflow;
  gasflow_struct.s = sample_time;
  gasflow_struct.counts = gasflow_value;

  return gasflow_struct;
}

humidity_struct Sampler::_readHumidity(int sensor)
{
  _i2c_select(sensor);
  sample relhum_struct;
  sample dew_struct;
  sample temp_struct;
  humidity_struct humidity_struct;

  if (sht->readSample())
  {
    double temp = sht->getTemperature();
    double relhum = sht->getHumidity();

    if (relhum < 0.01)
      relhum = 0.01;

    double H = (log10(relhum) - 2.0) / 0.4343 + (17.62 * temp) / (243.12 + temp); // SHTxx Dew-point Calculation from http://irtfweb.ifa.hawaii.edu/~tcs3/tcs3/Misc/Dewpoint_Calculation_Humidity_Sensor_E.pdf
    double dewPoint = (243.12 * H) / (17.62 - H);

    float sample_time = _time();

    relhum_struct.val = relhum;
    relhum_struct.s = sample_time;

    temp_struct.val = temp;
    temp_struct.s = sample_time;

    dew_struct.val = dewPoint;
    dew_struct.s = sample_time;

    humidity_struct.rel = relhum_struct;
    humidity_struct.dp = dew_struct;
    humidity_struct.t = temp_struct;

    return humidity_struct;
  }
  else
  {
    float sample_time = _time();

    relhum_struct.val = NAN;
    relhum_struct.s = sample_time;

    temp_struct.val = NAN;
    temp_struct.s = sample_time;

    dew_struct.val = NAN;
    dew_struct.s = sample_time;

    humidity_struct.rel = relhum_struct;
    humidity_struct.dp = dew_struct;
    humidity_struct.t = temp_struct;

    return humidity_struct;
  }
}

humidity_struct Sampler::_readHumidity_sniffer()
{
  sample relhum_struct;
  sample dew_struct;
  sample temp_struct;
  humidity_struct humidity_struct;

  if (i2c_buffer == 0)
  {
    float sample_time = float(i2c_sniffer_time) - float(sync);

    relhum_struct.val = NAN;
    relhum_struct.s = sample_time;

    temp_struct.val = NAN;
    temp_struct.s = sample_time;

    dew_struct.val = NAN;
    dew_struct.s = sample_time;

    humidity_struct.rel = relhum_struct;
    humidity_struct.dp = dew_struct;
    humidity_struct.t = temp_struct;

    return humidity_struct;
  }

  uint16_t humidity_MSB = (i2c_buffer >> 20) & 0b11111111;
  uint16_t humidity_LSB = (i2c_buffer >> 11) & 0b11111111;
  uint16_t humidity_value = (humidity_MSB << 8) | humidity_LSB;
  double relhum = (humidity_value / (65536.0 - 1.0)) * 100.0;

  uint16_t temperature_MSB = (i2c_buffer >> 47) & 0b11111111;
  uint16_t temperature_LSB = (i2c_buffer >> 38) & 0b11111111;
  uint16_t temperature_value = (temperature_MSB << 8) | temperature_LSB;
  double temperature = -45.0 + (temperature_value / (65536.0 - 1.0)) * 175.0;

  if (relhum < 0.01)
    relhum = 0.01;

  double H = (log10(relhum) - 2.0) / 0.4343 + (17.62 * temperature) / (243.12 + temperature); // SHTxx Dew-point Calculation from http://irtfweb.ifa.hawaii.edu/~tcs3/tcs3/Misc/Dewpoint_Calculation_Humidity_Sensor_E.pdf
  double dewPoint = (243.12 * H) / (17.62 - H);

  float sample_time = float(i2c_sniffer_time) - float(sync);

  relhum_struct.val = relhum;
  relhum_struct.s = sample_time;

  temp_struct.val = temperature;
  temp_struct.s = sample_time;

  dew_struct.val = dewPoint;
  dew_struct.s = sample_time;

  humidity_struct.rel = relhum_struct;
  humidity_struct.dp = dew_struct;
  humidity_struct.t = temp_struct;

  return humidity_struct;
}

sample Sampler::_readPeltierCurrent()
{
  sample peltier_struct;

  double peltier_value = analogRead(PELTIER_CURRENT);
  double peltier_ratio = peltier_value / 1023.0;
  double peltier_current = ( 3.3 * peltier_ratio / 0.055 ) - _current_zero;

  float sample_time = _time();

  peltier_struct.val = peltier_current;
  peltier_struct.s = sample_time;
  peltier_struct.counts = peltier_value;

  return peltier_struct;

}

unsigned long Sampler::_time()
{
  //It would be nice to return a double here, but apparently on arduinos all doubles are actually floats.
  //Floats only have sub-10ms accuracy up to 36 hours.
  unsigned long t = millis() - sync; //Milliseconds since last PC sync
  return t;
}

void Sampler::_i2c_select(int channel)
{
  uint8_t sendByte;
  switch (channel)
  {
    case 0:
      sendByte = 0b00000100;
      break;
    case 1:
      sendByte = 0b00000101;
      break;
    case 2:
      sendByte = 0b00000110;
      break;
    case 3:
      sendByte = 0b00000111;
      break;
    default:
      Serial.println("Invalid I2C channel selected!");
      return;
  }


  Serial.print("Selecting I2C channel: ");
  Serial.println(channel);

  Wire.beginTransmission(0x70);
  Wire.write(sendByte);
  Wire.endTransmission();
}


//Timed sample routines
bool Sampler::_routineSampleNtc1(Sampler& s) {s.sampleModuleT(ss_NTC_1); return true;}
bool Sampler::_routineSampleNtc2(Sampler& s) {s.sampleModuleT(ss_NTC_2); return true;}
bool Sampler::_routineSampleNtc3(Sampler& s) {s.sampleModuleT(ss_NTC_3); return true;}
bool Sampler::_routineSampleHum1(Sampler& s) {s.sampleHumidity(0); return true;}
bool Sampler::_routineSampleHum2(Sampler& s) {s.sampleHumidity(1); return true;}
bool Sampler::_routineSampleGasflow(Sampler& s) {s.sampleGasFlow(); return true;}
bool Sampler::_routineSampleColdplate(Sampler& s) {s.sampleColdplateT(); return true;}
bool Sampler::_routineSampleVChuck(Sampler& s) {s.sampleVacuumchuckT(); return true;}
bool Sampler::_routineReadVChuck(Sampler& s) {s._readVacuumChuckTemperature(); return true;}
bool Sampler::_routineSamplePeltier(Sampler& s) {s.samplePeltier(); return true;}
bool Sampler::_routineSamplePid(Sampler& s) {s.samplePid(); return true;}
bool Sampler::_routineSampleVacuumPressure(Sampler& s) {s.sampleADS1115(); return true;}
