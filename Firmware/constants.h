#ifndef constants_h
#define constants_h
#include <Arduino.h>

//Analog pin definitions
const int NTC_2 = A1;
const int NTC_3 = A2;
const int FLOW = A0;
const int I2C_SDA = A4;
const int I2C_SCL = A5;
const int NTC_CP = A3;
const int PELTIER_CURRENT = A6;
const int PSU_RATIOMETRIC = 9;

//Digital pin definitions
const int I2C_SNIFFER_START = 2;
const int I2C_SNIFFER_STOP = 3;
const int I2C_SNIFFER_SCL = 4;
const int I2C_SNIFFER_SDA = 5;
const int EXPANDER_RESET = 8;
const int EXPANDER_INTA = 6;
const int EXPANDER_INTB = 7;
const int PSU_RS232_TX = 1;
const int PSU_RS232_RX = 0;

//MCP23017 Extender definitions
const int POLARITY = 8;
const int INTERLOCK_CONNECTION = 9;
const int DISPLAY_RS = 10;
const int DISPLAY_SELECT = 11;
const int ID_DISPLAY = 12;
const int CHUCK_PT100_SELECT = 13;
const int CHUCK_PT100_READY = 14;

//Nonvolatile variables locations
const int ID_EEPROM_ADDR = 0; // Holds a uint8
const int ICAL_FLAG_EEPROM_ADDR = 1; // Holds a uint8
const int ICAL_EEPROM_ADDR = 2; // Holds a double
const int PSUMAX_FLAG_EEPROM_ADDR = 6; // Holds a uint8
const int PSUMAX_EEPROM_ADDR = 7; // Holds a double
const int PSUCOMM_EEPROM_ADDR = 11; // Holds a uint8
const int PSUKP_EEPROM_ADDR = 12; // Holds a float
const int PSUKI_EEPROM_ADDR = 16; // Holds a float
const int PSUKD_EEPROM_ADDR = 20; // Holds a float
const int TECKP_EEPROM_ADDR = 24; // Holds a float
const int TECKI_EEPROM_ADDR = 28; // Holds a float
const int TECKD_EEPROM_ADDR = 32; // Holds a float
const int TECIMAX_EEPROM_ADDR = 36; // Holds a float
const int PSUFREQ_EEPROM_ADDR = 40; // Holds a float
const int TECFREQ_EEPROM_ADDR = 44; // Holds a float
const int TECPON_EEPROM_ADDR = 48;
const int ILHIGH_EEPROM_ADDR = 52;
const int ILLOW_EEPROM_ADDR = 56;
const int ILDEW_EEPROM_ADDR = 60;
const int ILCOLD_EEPROM_ADDR = 64;

#endif
