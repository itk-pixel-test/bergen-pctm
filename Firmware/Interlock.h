/*
 Peltiercontroller.h
 A class that controls the peltier element
 */

#ifndef Interlock_h
#define Interlock_h

#include "Sampler.h"
#include "constants.h"
#include <arduino-timer.h>
#include "Peltiercontroller.h"

class Interlock
{
  public:
    //Methods
    bool checkTemperatureLimits();
    void setTemperatureLimits(float high, float low);
    void setColdplateLimit(float limit);
    void setDewpointLimit(float limit);
    void checkInterlock();
    void activateInterlockTemperature();
    void init();
    void setSettings(interlock_settings cmd, float value);
    void activateInterlock(bool enable);
    void tx_settings();

    //Variables
    Timer<> interlockTimer;
    Interlock(Sampler& sampler_, SerialTransfer& uart_txrx_, Peltiercontroller& pc_);

  private:
    //Methods
    void _tx_setting(interlock_settings r, float val);
    void _loadCalibrations();

    //Variables and objects
    SerialTransfer& uart_txrx;
    Sampler& sampler;
    Peltiercontroller& pc;

    float _temperature_limit_high;
    float _temperature_limit_low;
    float _dewpoint_limit;
    float _coldplate_limit;

    //Flags
    bool _interlockEnable = false;
    bool _activated_high = false;
    bool _activated_low = false;
    bool _activated_dew = false;
    bool _activated_absent = false;
    bool _activated_coldplate = false;

    //Routines
    uintptr_t checkInterlockTimer;

    //Timers
    static bool _checkInterlockRoutine(Interlock& interlock);
};
#endif
