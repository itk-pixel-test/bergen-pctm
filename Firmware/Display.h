/*
  Display.h
  A class that handles the displaying of internal variables
  on the PCTM DogLCD display. Varibles include module temperature,
  flow rate, peltier current, and more.

  Author: Magne Lauritzen
*/

#ifndef Display_h
#define Display_h

#include "Arduino.h"
#include "DogLcd.h"
#include "Sampler.h"
#include "constants.h"
#include <Adafruit_MCP23017.h>

class Display
{
  public:
    Display(Sampler& sampler_, Adafruit_MCP23017& mcp_);
    void update();
    void init();
    int start_row1;
    int start_row2;
  private:
    //constants
    const static int LCDWIDTH = 16;
    DogLcd* lcd;
    Sampler& sampler;
    Adafruit_MCP23017& mcp;
    //Internal temperature parameters
    float _modntc1;
    float _modntc2;
    float _modntc3;
    float _cpntc;
    //Internal humidity parameters
    float _dewpoint;
    //Display strings
    char _main_info[LCDWIDTH + 1];
    char _buf_main_info[100];
    char _Secondary_info_row1[100];
    char _Secondary_info_row2[100];
    char _scroll_info_row1[LCDWIDTH + 1];
    char _scroll_info_row2[LCDWIDTH + 1];
    
    //Internal functions
    char *_TempToStr(char *buf, float T, int precision);
    char *_HumToStr(char *buf, float H);
    char *_GSToStr(char *buf, float H, int precision);
    char *_TECToStr(char *buf, float I, int precision);
    void _BuildMainInfoString();
    void _BuildSecondaryInfoString();
    void _BuildScrollInfo(char *buf, char *info, int row);
};

#endif
