/*
 InterrruptServiceRoutines.h
 Definitions of Interrupt Service Routines for hardware interrupts
 */

#ifndef InterruptServiceRoutines_h
#define InterruptServiceRoutines_h

#include <Arduino.h>
#include "Pctm.h"

extern PCTM pctm;
extern Sampler s;

ISR (PORTA_PORT_vect)
/*
 ISR for INTERLOCK_CONNECTION and I2C_SNIFFER_START interrupts
 INTERLOCK_CONNECTION and I2C_SNIFFER_START pins share the same PORT
 */
{
  /*
   HW interlock interrupt
   */
  if ((VPORTA.INTFLAGS & 0b00000010) > 0)
  {
    VPORTA.INTFLAGS = VPORTA.INTFLAGS | 0b00000010; //Clear flag on EXPANDER_INTB
    pctm.delay_time = millis() - pctm.before_time;
    if (pctm.delay_time < 100)
    {
      return;
    }
    pctm.interlock_connection = !pctm.interlock_connection;
    s.interlock_connection = !s.interlock_connection;
    
    if (pctm.interlock_connection == true)
    {
      PORTA.PIN0CTRL = 0b00000010; //Interrupt enabled with sense on rising edge on pin D2 (I2C_SNIFFER_START)
    }
    else
    {
      VPORTF.INTFLAGS = VPORTF.INTFLAGS | 0b00100000; //Clear flag on I2C_SNIFFER_STOP
      VPORTA.INTFLAGS = VPORTA.INTFLAGS | 0b00000001; //Clear flag on I2C_SNIFFER_START
      VPORTC.INTFLAGS = VPORTC.INTFLAGS | 0b01000000; //Clear flag on I2C_SNIFFER_SCL
      PORTA.PIN0CTRL = 0b00000000; //Disable interrupt on pin D2 (I2C_SNIFFER_START)
      PORTC.PIN6CTRL = 0b00000000; //Disable interrupt on pin D4 (I2C_SNIFFER_SCL)
      PORTF.PIN5CTRL = 0b00000000; //Disable interrupt on pin D3 (I2C_SNIFFER_STOP)
    }
    pctm.before_time = millis();
  }

  /*
   I2C start signal interrupt
   */
  if (VPORTA.INTFLAGS & 0b00000001)
  {
    VPORTA.INTFLAGS = VPORTA.INTFLAGS | 0b00000001; //Clear flag on I2C_SNIFFER_START

    /*
    Check for NACK
    */
    if ((s.i2c_buffer & 0b11) == 0b11)
    {
      VPORTF.INTFLAGS = VPORTF.INTFLAGS | 0b00100000; //Clear flag on I2C_SNIFFER_STOP
      VPORTA.INTFLAGS = VPORTA.INTFLAGS | 0b00000001; //Clear flag on I2C_SNIFFER_START
      VPORTC.INTFLAGS = VPORTC.INTFLAGS | 0b01000000; //Clear flag on I2C_SNIFFER_SCL

      PORTA.PIN0CTRL = 0b00000000; //Disable interrupt on pin D2 (I2C_SNIFFER_START)
      PORTC.PIN6CTRL = 0b00000000; //Disable interrupt on pin D4 (I2C_SNIFFER_SCL)
      PORTF.PIN5CTRL = 0b00000000; //Disable interrupt on pin D3 (I2C_SNIFFER_STOP)

      s.i2c_buffer = 0;
      s.i2c_ready = true;
      s.i2c_sniffer_time = millis();
      return;
    }
    
    PORTC.PIN6CTRL = 0b00000010; ////Interrupt enabled with sense on rising edge on pin D4 (I2C_SNIFFER_SCL)
    PORTF.PIN5CTRL = 0b00000010; ////Interrupt enabled with sense on rising edge on pin D3 (I2C_SNIFFER_STOP)
    s.i2c_buffer = 0;
    s.i2c_ready = false;
  }
}

ISR (PORTC_PORT_vect)
/*
 ISR for I2C_SNIFFER_SCL
 */
{
  s.sda_buffer = (VPORTB.IN & 0b00000100) >> 2; //DigitalRead on pin D5 (I2C_SNIFFER_SDA)
  s.i2c_buffer = (s.i2c_buffer << 1) | s.sda_buffer;
  VPORTC.INTFLAGS = VPORTC.INTFLAGS | 0b01000000; //Clear flag on I2C_SNIFFER_SCL
}

ISR (PORTF_PORT_vect)
/*
 ISR for I2C_SNIFFER_STOP
 */
{
  VPORTF.INTFLAGS = VPORTF.INTFLAGS | 0b00100000; //Clear flag on I2C_SNIFFER_STOP
  VPORTA.INTFLAGS = VPORTA.INTFLAGS | 0b00000001; //Clear flag on I2C_SNIFFER_START
  VPORTC.INTFLAGS = VPORTC.INTFLAGS | 0b01000000; //Clear flag on I2C_SNIFFER_SCL
  s.i2c_ready = true;
  s.i2c_sniffer_time = millis();
  PORTA.PIN0CTRL = 0b00000000; //Disable interrupt on pin D2 (I2C_SNIFFER_START)
  PORTC.PIN6CTRL = 0b00000000; //Disable interrupt on pin D4 (I2C_SNIFFER_SCL)
  PORTF.PIN5CTRL = 0b00000000; //Disable interrupt on pin D3 (I2C_SNIFFER_STOP)
}

#endif
