#include "SerialTransfer.h"
#include "constants.h"
#include "Display.h"
#include "SegmentDisplay.h"
#include "Sampler.h"
#include "Pctm.h"
#include "Peltiercontroller.h"
#include <arduino-timer.h>
#include <Adafruit_MCP23017.h>
#include <SPI.h>
#include "InterruptServiceRoutines.h"
#include "Interlock.h"
#include <Wire.h>

Adafruit_MCP23017 mcp;
SerialTransfer uart_txrx;
Sampler s = Sampler(uart_txrx, mcp);
Display d = Display(s, mcp);
SegmentDisplay sd = SegmentDisplay(mcp);
Peltiercontroller p = Peltiercontroller(s, uart_txrx, mcp);
Interlock i = Interlock(s, uart_txrx, p);
PCTM pctm = PCTM(uart_txrx, d, sd, s, p, i, mcp);


void setup() {
  Serial.begin(115200);
  Wire.begin(); // Initialize I2C bus
  uart_txrx.begin(Serial);
  pctm.strprint("PCTM booting");
  pctm.init();
  pctm.strprint("PCTM ready.");
  s.init(); // call to initiate ADS1115
}

void loop() {
  s.sampletimer.tick();
  pctm.generaltimer.tick();
  p.pidtimer.tick();
  i.interlockTimer.tick();
  s.sampleADS1115();  // Add call to sample ADS1115
  delay(10);
}